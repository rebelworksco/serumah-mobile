import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

import AppRouter from './src/routes';

export default class App extends Component {
  render() {
    return (
      <AppRouter/>
    );
  }
}
