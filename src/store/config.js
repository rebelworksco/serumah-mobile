import React from 'react';

import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { persistStore, persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import AsyncStorage from '@react-native-community/async-storage';
import thunk from 'redux-thunk';

import UserReducer from './User/user.reducer';
import autoMergeLevel2Immutable from './autoMergeLevel2Immutable';

const rootReducer = combineReducers({
  user: UserReducer,
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2Immutable,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const asyncStore = createStore(
  persistedReducer,
  compose(
    applyMiddleware(
      thunk,
    ),
  ),
);

const persistor = persistStore(asyncStore);
// persistor.purge();

const ReduxStore = props => {
  return (
    <Provider store={ asyncStore }>
      <PersistGate loading={ null } persistor={ persistor }>
        { props.children }
      </PersistGate>
    </Provider>
  );
};

export { ReduxStore, asyncStore };
