import React from 'react';
import { Platform } from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';

const Icon = ({ name, ...props }) => (
  <Ionicon
    name={ Platform.OS === 'ios' ? `ios-${name}` : `md-${name}` }
    { ...props }
  />
);

export default Icon;
