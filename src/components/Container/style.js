import {
  Colors,
  GlobalStyles,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  outerContainer: {
    flex: 1,
    // backgroundColor: Colors.white.default,
  },

  bg: (height = 315) => {
    return {
      backgroundColor: Colors.blue.main,
      height: RatioHelper.normalize(height),
      width: RatioHelper.screenWidth,
      top: 0,
      left: 0,
      position: 'absolute',
    };
  },

  bgImage: GlobalStyles.fullScreen,

  bgOverlay: [
    GlobalStyles.fullScreen,
    {
      backgroundColor: 'rgba(6, 32, 83, 0.7)',
    },
  ],

  container: (noPaddingTop, noPaddingBottom, noPaddingHorizontal) => {
    let paddingTop = 20;
    let paddingBottom = 30;
    let paddingHorizontal = 20;

    if (noPaddingTop) {
      paddingTop = 0;
    }

    if (noPaddingBottom) {
      paddingBottom = 0;
    }

    if (noPaddingHorizontal) {
      paddingHorizontal = 0;
    }

    return {
      flex: 1,
      paddingTop: paddingTop,
      paddingBottom: paddingBottom,
      paddingHorizontal: paddingHorizontal,
      // backgroundColor: Colors.white.default,
    };
  },

  diamondLeft: GlobalStyles.diamond(0 - RatioHelper.screenHeight / 5, 0 + RatioHelper.screenWidth / 2),
  diamondCenter: GlobalStyles.diamond(0 - RatioHelper.screenHeight / 4, 0),
  diamondRight: GlobalStyles.diamond(0 - RatioHelper.screenHeight / 3, 0 - RatioHelper.screenWidth / 2),
  diamondTop: GlobalStyles.diamond(),
  diamondBottom: GlobalStyles.diamond(RatioHelper.screenHeight * 3 / 5, RatioHelper.screenHeight / 4),

  load: {
    height: RatioHelper.screenHeight,
    width: RatioHelper.screenWidth,
    backgroundColor: Colors.black.opacityHalf,
  },
};
