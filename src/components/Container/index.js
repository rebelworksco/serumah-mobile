import React from 'react';
import {
  View,
  SafeAreaView,
  ImageBackground,
} from 'react-native';

import {
  RatioHelper,
} from 'helpers';
// import NetInfo from '@react-native-community/netinfo';

import Header from '../Header';
// import Modal from '../Modal';
// import BottomNotif from '../BottomNotif';
// import ErrorNotif from '../ErrorNotif';
import Styles from './style';

const Container = props => {
  // const [modalIsOpened, setModalIsOpened] = useState(false);

  // useEffect(() => {
  //   NetInfo.fetch().then(connectionInfo => {
  //     (connectionInfo.type === 'none' || !connectionInfo.isConnected) ? setModalIsOpened(true) : setModalIsOpened(false);
  //   });
  // }, []);

  const renderContent = () => {
    return (
      <View style={ [Styles.container(props.noPaddingTop, props.noPaddingBottom, props.noPaddingHorizontal), props.style] } >
        { props.children }
      </View>
    );
  };

  const renderHeader = () => {
    if (props.header) {
      return (
        <Header { ...props.header }/>
      );
    }

    return null;
  };

  const renderBg = () => {
    switch (props.bg) {
      case 'default': return (
        <View style={ Styles.bg(100) }  />
      );
      case 'custom': return (
        <View style={ Styles.bg(props.bgHeight) } >

          <View style={ Styles.diamondLeft } />
          <View style={ Styles.diamondCenter } />
          <View style={ Styles.diamondRight } />

        </View>
      );
      case 'full' : return (
        <View style={ Styles.bg(RatioHelper.screenHeight) } >
          {
            props.bgImage ? (
              <>
                <ImageBackground source={ props.bgImage } style={ Styles.bgImage } />
                <View style={ Styles.bgOverlay } />
              </>
            ) : (
              <>
                <View style={ Styles.diamondTop } />
                <View style={ Styles.diamondBottom } />
              </>
            )
          }
        </View>
      );
    }
  };

  // const renderLoading = () => {
  //   return <View style={ Styles.load }>
  //     <Ionicons name={ 'ios-sync' } />
  //   </View>;
  // };
  // const renderBottomNotif = () => {
  //   if (props.bottomNotif) {
  //     return (
  //       <BottomNotif { ... props.bottomNotif }/>
  //     );
  //   }

  //   return null;
  // };

  // const renderErrorNotif = () => {
  //   if (props.errorNotif) {
  //     return (
  //       <ErrorNotif { ...props.errorNotif }/>
  //     );
  //   }

  //   return null;
  // };

  // const renderModalNoConnection = () => {
  //   if (modalIsOpened) {
  //     return (
  //       <Modal
  //         type={ 'info' }
  //         height={ 320 }
  //         style={ { padding: 30 } }
  //         modalIsOpened={ modalIsOpened }
  //         onPressClose={ () => setModalIsOpened(false) }
  //         buttonText={ 'Retry' }
  //         image={ Images.ic_no_internet }
  //         title={ 'Tidak ada koneksi Internet' }
  //         desc={ 'Silahkan periksa koneksi internetmu dan coba lagi.' }
  //         onPress={ () => setModalIsOpened(false) }
  //       />
  //     );
  //   }

  //   return null;
  // };

  return (
    <View style={ Styles.outerContainer }>
      {/* { renderLoading() } */}
      <SafeAreaView style={ Styles.outerContainer }>
        { renderBg() }

        { renderHeader() }

        { renderContent() }

      </SafeAreaView>
{/*
        { renderBottomNotif() }

        { renderErrorNotif() }

        { renderModalNoConnection() } */}

    </View>
  );
};



export default Container;
