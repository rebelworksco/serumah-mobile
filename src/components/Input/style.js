import {
  RatioHelper,
} from 'helpers';

import {
  Platform,
} from 'react-native';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    marginBottom: 20,
    // borderWidth: 1,
    flexDirection: 'row',

  },

  animatedPlaceholder: {
    container: translate => {
      return {
        flex: -1,
        height: 20,
        position: 'absolute',
        top: 13,
        transform: [
          { translateY: translate },
        ],
        paddingLeft: 20,
        // borderWidth: 1,
      };
    },

    text: (size, color, fontWeight, placeholderColor) => {
      return [
        GlobalStyles.textRegular(),
        {
          fontSize: size,
          color: color.interpolate({
            inputRange: [0, 1],
            outputRange: placeholderColor ? [Colors.black.opacityLow, placeholderColor] : [Colors.black.opacityLow, Colors.blue.main],
          }),
          fontWeight: fontWeight,
        },
      ];
    },
  },

  input: {
    container: (state, value, editable = true, isDefault = false) => {
      return {
        flex: 1,
        height: 50,
        paddingLeft: isDefault ? 0 : 20,
        paddingTop: isDefault ? Platform.OS === 'ios' ? 0 : 10 : Platform.OS === 'ios' ? 10 : 20,
        width: RatioHelper.screenWidth - 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1.5,
        borderRadius: 5,
        borderColor: (state && editable) ? Colors.blue.main : Colors.black.opacityLow,
        backgroundColor: Colors.white.default,
        // borderWidth: 1,
      };
    },

    text: [
      GlobalStyles.textRegular(RatioHelper.normalize(14)),
    ],

    textDefault: [
      GlobalStyles.textBold(RatioHelper.normalize(20)),
      {
        textAlign: 'center',
      },
    ],

    icon: {
      flex: -1,
      position: 'absolute',
      right: 15,
      paddingTop: 10,
    },
  },
};
