import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  linkContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  text: (textColor = Colors.blue.main, fontSize = RatioHelper.normalize(15)) => [
    GlobalStyles.textBold(),
    {
      fontSize: fontSize,
      color: textColor,
    },
  ],

  icon: {
    marginRight: 5,
  },
};
