import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import { Colors } from 'consts';

import {
  Icon,
} from 'components';

import Styles from './style';

const Link = props => {

  return (
    <TouchableOpacity
      disabled={ props.disabled }
      onPress={ props.onPress }
      style={ [Styles.linkContainer, props.style] }
    >
      {
        props.icon && <Icon style={ Styles.icon } name={ props.icon } color={ props.color ? props.color : Colors.blue.main } size={ 20 } />
      }
      <Text style={ Styles.text(props.color, props.fontSize) }>{ props.text }</Text>
    </TouchableOpacity>
  );
};

export default Link;
