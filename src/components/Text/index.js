import React from 'react';

import {
  Text,
} from 'react-native';

import {
  Colors,
} from 'consts';

import Styles from './styles';

export default props => {
  let { children, type, fontSize, color, mb, bold, fade, align } = props;

  switch (color) {
    case 'primary':
      color = Colors.blue.main;
      break;
    case 'secondary':
      color = Colors.grey.default;
      break;
    case 'white':
      color = Colors.white.default;
      break;
  }

  switch (type) {
    case 'title':
      fontSize = 24;
      color = color ? color : Colors.blue.main;
      mb = mb ? mb : 30;
      bold = true;
      break;
    case 'semiTitle':
      fontSize = 20;
      bold = true;
      break;
  }

  return (
    <Text style={ [Styles.text(fontSize, color, mb, bold, fade, align), props.style] }>
      { children }
    </Text>
  );
}
;
