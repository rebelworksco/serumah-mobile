import {
  Colors,
} from 'consts';

export default {
  text: (fontSize = 14, color = Colors.black.default, mb = 20, bold, fade, align) => {
    return {
      fontSize: fontSize,
      color: color,
      marginBottom: mb,
      fontWeight: bold ? 'bold' : null,
      opacity: fade ? 0.5 : null,
      textAlign: align ? align : null,
    };
  },
};
