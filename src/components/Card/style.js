import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: [
    GlobalStyles.shadow,
    {
      flex: -1,
      padding: 15,
      flexDirection: 'row',
      backgroundColor: Colors.white.default,
      borderRadius: 8,
      borderWidth: 0,
    },
  ],
};
