import React from 'react';
import {
  View,
} from 'react-native';

import Styles from './style';

const Card = props => {
  return (
    <View style={ [Styles.container, props.style] }>
      { props.children }
    </View>
  );
};

export default Card;
