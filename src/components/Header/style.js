import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    flex: -1,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: 'transparent',
    // borderWidth: 1,
    zIndex: 10,
  },

  left: {
    container: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: 'transparent',
      // borderWidth: 1,
    },

    innerContainer: {
      flex: -1,
      height: 40,
      minWidth: 50,
      paddingLeft: 8,
      justifyContent: 'center',
      // borderWidth: 1,
    },

    text: {
      textAlign: 'left',
    },
  },

  right: {
    container: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
      alignItems: 'center',
      backgroundColor: 'transparent',
      // borderWidth: 1,
    },

    innerContainer: {
      flex: -1,
      height: 40,
      paddingTop: 2,
      justifyContent: 'center',
      minWidth: 50,
      paddingRight: 8,
      // borderWidth: 1,
    },

    innerContainer2: {
      flex: -1,
      height: 40,
      paddingTop: 2,
      justifyContent: 'center',
      // borderWidth: 1,
    },

    icon: {
      textAlign: 'right',
    },

    text: {
      flex: -1,
      paddingBottom: 7,
      // borderWidth: 1,
    },

    notif: {
      container: {
        width: 18,
        height: 18,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.green.preset1,
        borderRadius: 50,
        position: 'absolute',
        top: 0,
        right: 3,
      },

      text: GlobalStyles.textBold(RatioHelper.normalize(12), Colors.white.default, 'center'),
    },

    img: {
      width: RatioHelper.normalize(24),
      height: RatioHelper.normalize(24),
      resizeMode: 'contain',
      alignSelf: 'flex-end',
      top: 0,
    },
  },

  middle: {
    container: (width = 150) => {
      return {
        flex: -1,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        width,
        // borderWidth: 1,
      };
    },

    image: {
      width: 18,
      height: 20,
    },

    text: [
      GlobalStyles.textBold(RatioHelper.normalize(18), Colors.white.default),
    ],
  },
};
