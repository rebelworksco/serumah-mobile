import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwsome from 'react-native-vector-icons/FontAwesome5';

import {
  Colors,
  Images,
} from 'consts';
import Styles from './style';

const Header = props => {
  const renderLeftButton = () => {
    if (props.leftButton) {
      return <Ionicons style={ Styles.left.icon } name={ props.leftButton } color={ props.leftButtonColor ? props.leftButtonColor : Colors.grey.preset1 } size={ props.leftButtonSize ? props.leftButtonSize : 35 }/>;
    }

    return null;
  };

  const renderMiddleContent = () => {
    if (props.middleImage) {
      return <Image style={ Styles.middle.image } source={ Images.logo_jelas_small }/>;
    }

    if (props.middleText) {
      return <Text style={ [Styles.middle.text, { color: props.middleTextColor }] } >{ props.middleText }</Text>;
    }

    return null;
  };

  const renderRightButton = () => {
    if (props.rightButton) {
      if (props.rightBtnFontAwsome) {
        return <FontAwsome solid={ props.isSolid } style={ Styles.right.icon } name={ props.rightButton } color={ props.rightButtonColor ? props.rightButtonColor : Colors.grey.preset1 } size={ props.rightButtonSize ? props.rightButtonSize : 35 }/>;
      }
      return <Ionicons style={ Styles.right.icon } name={ props.rightButton } color={ props.rightButtonColor ? props.rightButtonColor : Colors.grey.preset1 } size={ props.rightButtonSize ? props.rightButtonSize : 35 }/>;
    }

    if (props.rightImg) {
      return <Image source={ props.rightImg } style={ Styles.right.img } />;
    }

    if (props.rightText) {
      return <Text style={ [Styles.right.text, props.rightTextStyle] }>{ props.rightText }</Text>;
    }

    return null;
  };

  const renderRightButton2 = () => {
    if (props.rightButton2) {
      if (props.rightBtn2FontAwsome) {
        return <FontAwsome solid={ props.isSolid2 } style={ Styles.right.icon } name={ props.rightButton2 } color={ props.rightButton2Color ? props.rightButton2Color : Colors.grey.preset1 } size={ props.rightButton2Size ? props.rightButton2Size : 35 }/>;
      }
      return <Ionicons style={ Styles.right.icon } name={ props.rightButton2 } color={ props.rightButton2Color ? props.rightButton2Color : Colors.grey.light } size={ props.rightButton2Size ? props.rightButton2Size : 35 }/>;
    }

    if (props.rightText2) {
      return <Text style={ [Styles.right.text, props.rightTextStyle2] }>{ props.rightText2 }</Text>;
    }

    return null;
  };

  const renderNotification = () => {
    if (props.notifLength) {
      return (
        <View style={ Styles.right.notif.container }>
          <Text style={ Styles.right.notif.text }>{ props.notifLength }</Text>
        </View>
      );
    }

    return null;
  };

  return (
    <View style={ Styles.container }>
      <View style={ Styles.left.container }>
        <TouchableOpacity
          style={ [Styles.left.innerContainer, props.leftButtonContainerStyle ? props.leftButtonContainerStyle : {}] }
          onPress={ props.onPressLeft }
        >
          { renderLeftButton() }
        </TouchableOpacity>
      </View>

      <View style={ Styles.middle.container(props.middleWidth) }>
        { renderMiddleContent() }
      </View>

      <View style={ Styles.right.container }>
        <TouchableOpacity
          style={ [Styles.right.innerContainer, props.rightButtonContainerStyle ? props.rightButtonContainerStyle : {}] }
          onPress={ props.onPressRight }
        >
          { renderRightButton() }
        </TouchableOpacity>

        <TouchableOpacity
          style={ [Styles.right.innerContainer2, props.rightButton2ContainerStyle ? props.rightButton2ContainerStyle : {}] }
          onPress={ props.onPressRight2 }
        >
          { renderRightButton2() }
          { renderNotification() }
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Header;
