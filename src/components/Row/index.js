import React from 'react';

import {
  View,
} from 'react-native';

import Styles from './style';

const Row = props => (
  <View style={ [Styles.row(props.justify, props.align), props.style] }>
    { props.children }
  </View>
);

export default Row;
