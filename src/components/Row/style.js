export default {
  row: (justifyContent, alignItems) => {
    return {
      flexDirection: 'row',
      justifyContent: justifyContent,
      alignItems: alignItems,
    };
  },
};
