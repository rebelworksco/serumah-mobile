import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
} from 'consts';

export default {
  container: {
    flexDirection: 'row',
  },

  item: {
    flex: 1,
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },

  label: active => {
    return {
      color: active ? Colors.blue.main : Colors.grey.text,
      fontSize: RatioHelper.normalize(13),
      fontWeight: active ? '600' : '400',
    };
  },

  image: (height = 24) =>  {
    return {
      width: RatioHelper.normalize(24),
      height: RatioHelper.normalize(height),
      marginBottom: RatioHelper.normalize(10),
      resizeMode: 'contain',
    };
  },

};
