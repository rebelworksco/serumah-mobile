import React, { useState, useEffect } from 'react';
import { Text, TouchableOpacity, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Images } from 'consts';
import BottomCard from '../BottomCard';
import Styles from './style';

const TabBar = props => {

  const [routes, setRoutes] = useState([
    { label: 'Absensi', active: Images.ic_calendar_blue, inactive: Images.ic_calendar, key: 'attendance' },
    { label: 'pekerja', active: Images.ic_employee_blue, inactive: Images.ic_employee_grey, key: 'worker' },
    { label: 'Pinjaman', active: Images.ic_loan_blue, inactive: Images.ic_loan_grey, key: 'loan' },
    { label: 'Profile', active: Images.ic_user_blue, inactive: Images.ic_user_grey, key: 'profile' },
  ]);

  const [activeRoute, setActive] = useState('attendance');

  useEffect(() => {
    setActive(Actions.currentScene.slice(1, Actions.currentScene.length));
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  const onPress = route => {
    Actions[route]();
    setActive(route);
  };

  return (
    <BottomCard
      style={ Styles.container }
    >
      {
        routes.map(x => {
          let active = activeRoute === x.key;
          return (
            <TouchableOpacity key={ x.label } style={ Styles.item } onPress={ () => onPress(x.key) }>
              <Image source={ active ? x.active : x.inactive } style={ Styles.image(x.height) } />
              <Text style={ Styles.label(active) }>{x.label}</Text>
            </TouchableOpacity>
          );
        })
      }
    </BottomCard>
  );
};

export default TabBar;

