import React from 'react';

import {
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';

import Styles from './style';

const Radio = props => {
  return (
    <View style={ Styles.optionContainer(props.disabled) } >

      <TouchableOpacity disabled={ props.disabled } style={ Styles.button(props.selected) } onPress={ props.onPress } />
      
      <TouchableOpacity disabled={ props.disabled } style={ Styles.labelContainer(props.card, props.selected) } onPress={ props.onPress }>
        <Text style={ Styles.label }>{ props.label }</Text>
        {
          props.card &&
          (<Image style={ Styles.image } source={ props.image } />)
        }
      </TouchableOpacity>

    </View>
  );
};

export default Radio;
