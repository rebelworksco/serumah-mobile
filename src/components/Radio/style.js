import {
  RatioHelper,
} from 'helpers';

import {
  GlobalStyles,
  Colors,
} from 'consts';

export default {
  optionContainer: (disabled = false) => {
    return {
      flexDirection: 'row',
      marginBottom: 15,
      alignItems: 'center',
      opacity: disabled ? 0.5 : 1,
    };
  },

  button: (selected = false) => {
    return {
      height: 20,
      width: 20,
      borderRadius: 20,
      marginRight: 10,
      borderWidth: selected ? 6 : 1,
      borderColor: selected ? Colors.blue.main : Colors.grey.light,
    };
  },

  labelContainer: (card = false, selected = false) => {
    if (card) {
      return {
        borderWidth: selected ? 2 : 1,
        borderColor: selected ? Colors.blue.main : Colors.grey.light,
        width: RatioHelper.screenWidth - 40 - 30,
        padding: 20 - (selected ? 2 : 1),
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
      };
    }
  },
  
  label: GlobalStyles.textRegular(undefined, Colors.grey.default),

  image: {
    height: 17,
    resizeMode: 'contain',
    width: 60,
  },
};
