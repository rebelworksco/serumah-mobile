import React, { useState, useEffect, useRef, useReducer } from 'react';
import { View, Text, Animated } from 'react-native';

import { RatioHelper  } from 'helpers';
import { Button } from 'components';
import Styles from './style';

const BottomCard = props => {
  const { height, btnText, onPressBtn, children, type, title, style, bg, isOpen } = props;

  const [color, setColor] = useState(new Animated.Value(0));
  const [modalIsOpened, setModalIsOpened] = useState(false);
  const [bounceVal, setBounceVal] = useState(new Animated.Value(height));
  const timeHandler = useRef(0);

  useEffect(() => {
    if (isOpen) {
      setModalIsOpened(true);

      // animate opacity
      Animated.timing(color, {
        toValue: 1,
        duration: 200,
      }).start();
         
      // animated bottom sheet content
      Animated.timing(bounceVal, {
        duration: 300,
        toValue: 0,
     }).start();

    } else if (!isOpen) {

      timeHandler.current = setTimeout(() => {
        setModalIsOpened(false);
      }, 200);

      Animated.timing(color, {
        toValue: 0,
        duration: 200,
      }).start();

      Animated.timing(bounceVal, {
        duration: 300,
        toValue: height,
     }).start();
    }

    return () => {
      clearTimeout(timeHandler.current);
      timeHandler.current = 0;
    };
  }, [isOpen]); // eslint-disable-line react-hooks/exhaustive-deps


  const renderMoneyType = () => {
    return (
      <View style={ Styles.bottomCard(height) }>

        <View style={ Styles.btnCardUpper }>
          <Text style={ Styles.titleLeft }>{ title[0] }</Text>
          <Text style={ Styles.titleRight }>{ title[1] }</Text>
        </View>
        <View style={ Styles.btnCardBottom } >
          <Button
            text={ btnText }
            onPress={ onPressBtn }
            width={ RatioHelper.screenWidth - 50 } />
        </View>
        
      </View>
    );
  };

  const renderDefault = () => {
    return (
      <View style={ [Styles.bottomCard(height), style] }>
        {
         children
        }
      </View>
    );
  };

  const renderCustom = () => {
    if (modalIsOpened) {
      return (
        <Animated.View style={ Styles.wholePage(bg, color, height) }>
          <Animated.View style={ [Styles.bottomCard(height), style, { transform: [{ translateY: bounceVal }] }] }>
            {
              children
            }
          </Animated.View>
        </Animated.View>
      );
    }
    return null;
  };

  switch (type) {
    case 'money': return renderMoneyType();
    case 'custom' : return renderCustom();
    default: return renderDefault();
  }
};

export default BottomCard;
