import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

const container = {
  borderWidth: 1,
  borderColor: Colors.white.default,
  borderTopLeftRadius: 21,
  borderTopRightRadius: 21,
  ...GlobalStyles.shadow,
};

export default {
  bottomTab: {
      height: RatioHelper.normalize(106),
      flexDirection: 'row',
      ...container,
  },

  bottomCard: height => {
    return {
      ...container,
      height: height ? RatioHelper.normalize(height) : null,
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      width: RatioHelper.screenWidth,
      bottom: 0,
      position: 'absolute',
      padding: 20,
      paddingBottom: 60,
    };
  },

  btnCardUpper: {
    flexDirection: 'row',
    flex: 2,
    width: RatioHelper.screenWidth - 50,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },

  btnCardBottom: {
    flexDirection: 'row',
    flex: 3,
    width: RatioHelper.screenWidth,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },

  titleLeft: {
    ...GlobalStyles.textSemiBold(18),
  },

  titleRight: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.main, 'right'),
  },

  item: {
    flex: 1,
    justifyContent: 'center',
    textAlign: 'center',
    borderRadius: 10,
  },

  wholePage: (bg, color, height) => {
    return {
      width: RatioHelper.screenWidth,
      height: bg ? RatioHelper.screenHeight : height,
      backgroundColor: color.interpolate({
        inputRange: [0, 1],
        outputRange: ['transparent', bg ? Colors.black.opacity : 'transparent'],
      }),
      position: 'absolute',
      bottom: 0,
    };
  },
};
