import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
} from 'consts';

export default {
  container: (height = 180) => {
    return {
      flexDirection: 'row',
      padding: 10,
      height,
    };
  },

  svg: {
    stroke: Colors.blue.main,
    // fill: Colors.blue.veryTransparent,
    strokeWidth: 1,
    fill: 'url(#gradient)',

  },
  
  xAxis: {
    width: RatioHelper.screenWidth * 0.90,
    marginLeft: -15,
    // borderWidth: 1,
  },

  xSvg: {
    strokeWidth: 0.5,
    fontSize: RatioHelper.normalize(10),
    fill: Colors.grey.light,
  },

  yAxis: {
    marginRight: 5,
    // borderRightWidth: 1,
    // borderColor: Colors.grey.light,
  },
  
  yContentInset: {
    top: 5,
    bottom: 25,
  },
  
  ySvg: {
    fontSize: RatioHelper.normalize(10),
    strokeWidth: 0.5,
    fill: Colors.grey.light,
  },

  lineChart: {
    flex: 1,
    marginBottom: 10,
  },
  
  lineChartContent: type => {
    let os = RatioHelper.devicePlatform;
    let left = 1;
    let right = 1;

    if (os === 'android' && type === 'Daily') {
      left = -RatioHelper.screenWidth * 0.135;
      right = -RatioHelper.screenWidth * 0.135;
    } else if (os === 'android' && type === 'Monthly') {
      left = -RatioHelper.screenWidth * 0.075;
      right = -RatioHelper.screenWidth * 0.075;
    }

    return {
      top: 5,
      bottom: 1,
      left: left,
      right: right,
    };
  },
};
