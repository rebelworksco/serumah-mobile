import React from 'react';
import {
  View,
  Image,
  Platform,
} from 'react-native';
import { LineChart, Grid, XAxis, YAxis, Path } from 'react-native-svg-charts';
import { Defs, LinearGradient, Stop } from 'react-native-svg';

import {
  Images,
  Colors,
} from 'consts';

import Styles from './style';

const Chart = props => {
  const monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const dataChart = [2, 4, 4, 6, 5, 5, 5.5, 6, 6, 7, 6, 7];
  const yAxisData = [0, 2, 4, 5, 6, 8];

  const renderXLabel = (value, index) => {
    const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    if (index === month.indexOf(month[ index ])) {
      index = month[ index ];
      return index;
    }
  };

  const Gradient = () => (
    <Defs key={ 'gradient' }>
      <LinearGradient id={ 'gradient' } x1={ '0%' } y1={ '0%' } x2={ '0%' } y2={ '100%' }>
          <Stop offset={ '0%' } stopColor={ Colors.blue.main } stopOpacity={ 0.09 }/>
          <Stop offset={ '100%' } stopColor={ Colors.blue.main } stopOpacity={ 0.03 }/>
      </LinearGradient>
    </Defs>
);

  return (
    <View style={ Styles.container(props.height) }>
      <YAxis
        data={ dataChart }
        numberOfTicks={ 6 }
        formatLabel={ value => `${value}jt` }
        style={ Styles.yAxis }
        svg={ Styles.ySvg }
      />
      <View style={ { flex: 1 } } >
        <LineChart
          data={ dataChart }
          svg={ Styles.svg }
          yMax={ 8 }
          yMin={ 0 }
          style={ Styles.lineChart }
          contentInset={ Styles.lineChartContent('Monthly') }
          animate={ true }
          animationDuration={ 300 }
        >
          <Grid />
          <Gradient/>
        </LineChart>

        <XAxis
          style={ Styles.xAxis }
          data={ dataChart }
          formatLabel={ renderXLabel }
          svg={ Styles.xSvg }
        />
      </View>

    </View>

  );
};

export default Chart;
