import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: (containerColor = Colors.blue.main, altCSS = false, height = 45, width = RatioHelper.screenWidth - 40, disabled = false) => {
    return {
      flex: -1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: width,
      height: height,
      backgroundColor: altCSS ? Colors.white.default : containerColor,
      borderRadius: 8,
      borderWidth: altCSS ? 1 : 0,
      borderColor: altCSS ? Colors.blue.main : null,
      opacity: disabled ? 0.2 : 1,
    };
  },

  text: (textColor = Colors.white.default, altCSS = false, fontSize = RatioHelper.normalize(15)) => [
    GlobalStyles.textBold(),
    {
      fontSize: fontSize,
      color: altCSS ? Colors.blue.main : textColor,
    },
  ],

  rightIcon: {
    right: 10,
    position: 'absolute',
  },
  icon: {
    marginRight: 5,
  },
};
