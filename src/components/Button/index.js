import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import lodash from 'lodash';
import FontAwsome from 'react-native-vector-icons/FontAwesome5';

import { Colors } from 'consts';

import {
  Icon,
} from 'components';

import Styles from './style';

const Button = props => {
  const debouncedOnPress = lodash.debounce(
    () => props.onPress ? props.onPress() : null,
    2000,
    {
      leading: true,
      trailing: false,
    }
  );

  return (
    <TouchableOpacity
      disabled={ props.disabled }
      style={ [Styles.container(props.containerColor, props.alt, props.height, props.width, props.disabled), props.style] }
      onPress={ debouncedOnPress }
      activeOpacity={ props.activeOpacity ? props.activeOpacity : 0.2 }
    >
      {
        props.icon && <Icon style={ Styles.icon } name={ props.icon } color={ props.alt ? Colors.blue.main : Colors.white.default } size={ 20 } />
      }
      <Text style={ Styles.text(props.textColor, props.alt, props.fontSize) }>{ props.text }</Text>
      {
        props.iconRight && <FontAwsome style={ Styles.rightIcon } solid={ !!props.solid } name={ props.iconRight } color={ props.alt ? Colors.blue.main : Colors.white.default } size={ 20 } />
      }
    </TouchableOpacity>
  );
};

export default Button;
