import React, { useState, useEffect, useRef } from 'react';
import {
  Text,
  TouchableOpacity,
  Animated,
  Image,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Colors,
} from 'consts';
import {
  RatioHelper,
} from 'helpers';
import {
  Link,
} from 'components';

import Button from '../Button';
import Styles from './style';

const Modal = props => {
  const [color] = useState(new Animated.Value(0));
  const [focus, setFocus] = useState(false);
  const [height] = useState(new Animated.Value(80));
  const [sheetOpacity] = useState(new Animated.Value(0));
  const [modalIsOpened, setModalIsOpened] = useState(false);
  const [showContent, setShowContent] = useState(false);

  const timeHandler = useRef(0);

  useEffect(() => {
    if (props.modalIsOpened) {
      setModalIsOpened(true);
      setShowContent(true);
      Animated.timing(color, {
        toValue: 1,
        duration: 200,
      }).start();

      Animated.spring(height, {
        toValue: props.height,
        duration: 200,
      }).start();

      Animated.timing(sheetOpacity, {
        toValue: 1,
        duration: 200,
      }).start();

    } else if (!props.modalIsOpened) {
      setShowContent(false);

      timeHandler.current = setTimeout(() => {
        setModalIsOpened(false);
      }, 200);

      Animated.timing(color, {
        toValue: 0,
        duration: 200,
      }).start();

      Animated.spring(height, {
        toValue: 80,
        duration: 200,
      }).start();

      Animated.timing(sheetOpacity, {
        toValue: 0,
        duration: 200,
      }).start();
    }

    return () => {
      clearTimeout(timeHandler.current);
      timeHandler.current = 0;
    };
  }, [props.modalIsOpened, focus]); // eslint-disable-line react-hooks/exhaustive-deps

  const renderDescription = () => {
    let res = props.desc ? props.desc.split(props.boldDesc).reduce((prev, current, i) => {
      if (!i) {
        return [current];
      }

      return prev.concat(<Text key={ i } style={ { fontWeight: 'bold' } } >{ props.boldDesc }</Text>, current);
    }, []) : null;
    
    return <Text style={ Styles.sheetDefaultDescription }>{ res }</Text>;
  };

  const renderContent = () => {
    return showContent ? (
      <Animated.View style={ [Styles.sheetDefaultContainer(props.height, sheetOpacity), props.style] }>
        <TouchableOpacity style={ Styles.iconCloseContainer } onPress={ props.onPressClose } activeOpacity={ 1 }>
          <Ionicons style={ Styles.iconClose } name={ 'md-close' } color={ Colors.grey.border } size={ 25 } />
        </TouchableOpacity>

        {
         props.type === 'info' ? renderDefault() : props.children
        }
       
      </Animated.View>
    ) : (<Animated.View style={ [Styles.sheetDefaultContainer(props.height, sheetOpacity), props.style] } />);
  };

  const renderDefault = () => {
    return (
      <>
        <Image style={ Styles.image } source={ props.image }/>
        <Text style={ Styles.sheetDefaultTitle }>{ props.title }</Text>
        { renderDescription() }

        <Button
          text={ props.buttonText }
          alt={ props.buttonAlt }
          icon={ props.buttonIcon }
          onPress={ props.onPress }
          width={ RatioHelper.screenWidth - 100 }
        />

        {
          props.button2Text &&
          props.button2Type === 'link' ?
          <Link
            text={ props.button2Text }
            onPress={ props.onPress2 }
            color={ props.button2Color }
            style={ { marginTop: 20 } }
          /> :
          <Button
            text={ props.button2Text }
            onPress={ props.onPress2 }
            width={ RatioHelper.screenWidth - 100 }
            style={ { marginTop: 20 } }
          />
        }
      </>
    );
  };

  if (modalIsOpened) {
    return (
      <Animated.View style={ Styles.container(color) } >

        <TouchableOpacity style={ [Styles.touchableContainer, props.sheetTitle ? { justifyContent: 'flex-end' } : {}] } onPress={ () => { props.onPressClose(); setFocus(false); } } activeOpacity={ 1 }>

          { renderContent() }

        </TouchableOpacity>

      </Animated.View>
    );
  }

  return null;
};

export default Modal;
