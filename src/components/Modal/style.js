import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: color => {
    return {
      flex: 1,
      height: RatioHelper.screenHeight,
      width: RatioHelper.screenWidth,
      position: 'absolute',
      backgroundColor: color.interpolate({
        inputRange: [0, 1],
        outputRange: ['transparent', Colors.black.opacity],
      }),
    };
  },

  touchableContainer: {
    flex: 1,
    height: RatioHelper.screenHeight,
    width: RatioHelper.screenWidth,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },

  sheetDefaultContainer: (height, opacity) => {
    return {
      flex: -1,
      justifyContent: 'center',
      alignItems: 'center',
      width: RatioHelper.screenWidth - 60,
      height: height,
      padding: 20,
      paddingBottom: 30,
      borderRadius: 10,
      backgroundColor: Colors.white.default,
      opacity: opacity,
    };
  },

  image: {
    width: RatioHelper.normalize(100),
    height: RatioHelper.normalize(100),
    resizeMode: 'contain',
    marginBottom: 20,
  },

  sheetDefaultTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(18), Colors.blue.main, 'center'),
    {
      marginBottom: 20,
    },
  ],

  sheetDefaultDescription: [
    GlobalStyles.textRegular(),
    {
      textAlign: 'center',
      color: Colors.blue.darkText,
      lineHeight: 24,
      marginBottom: 20,
    },
  ],

  iconCloseContainer: {
    flex: -1,
    width: 35,
    alignSelf: 'flex-end',
  },

  iconClose: {
    textAlign: 'center',
  },
};
