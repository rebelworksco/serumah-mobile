import React from 'react';
import {
  View,
  Image,
} from 'react-native';

import {
  Images,
} from 'consts';

import Styles from './style';

const Loader = props => {
  if (props.content) {
    return (
      <View style={ [Styles.contentContainer, props.style] }>
        <Image style={ [Styles.imageGif, props.imageStyle] } source={ Images.loader }/>
      </View>
    );
  }

  return (
    <View style={ Styles.gifContainer }>
      <Image style={ Styles.imageGif } source={ Images.loader2 }/>
    </View>
  );
};

export default Loader;
