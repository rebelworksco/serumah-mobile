import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
} from 'consts';

export default {
  contentContainer: {
    flex: -1,
    height: RatioHelper.screenHeight / 1.3,
    width: RatioHelper.screenWidth - 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.white.default,
    // borderWidth: 1,
  },

  gifContainer: {
    flex: -1,
    height: RatioHelper.screenHeight,
    width: RatioHelper.screenWidth,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    backgroundColor: Colors.black.default,
    opacity: 0.7,
  },

  imageGif: {
    width: 50,
    height: 50,
  },
};
