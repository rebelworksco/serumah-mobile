import React, { useEffect } from 'react';
import { Image } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { Images } from 'consts';

import {
  Container,
} from 'components';

import Styles from './style';

const Splash = () => {
  useEffect(() => {
    setTimeout(() => {
      Actions.login();
    }, 1000);
  }, []);

  return (
    <Container
      bg='full'
      style={ Styles.container }
    >

      <Image style={ Styles.logo } source={ Images.logo_seruma } />

    </Container>
  );
};

export default Splash;
