import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

const submissionCardWidth = RatioHelper.screenWidth - 80;

export default {
  container: {
    paddingHorizontal: 0,
    paddingTop: 0,
    paddingBottom: 0,
  },

  tabsContainer: {
    flexDirection: 'row',
  },

  tabContainer: (active = false) => {
    return {
      width: RatioHelper.screenWidth / 2,
      alignItems: 'center',
      height: 50,
      justifyContent: 'center',
      borderBottomWidth: active ? 2 : 0.5,
      borderBottomColor: active ? Colors.blue.main : Colors.grey.light,
    };
  },

  tabText: (active = false) => {
    return [
      active ?
      GlobalStyles.textBold(RatioHelper.normalize(14), Colors.blue.main) :
      GlobalStyles.textRegular(undefined, Colors.grey.default),
    ];
  },

  submissionCardsContainer: {
    paddingTop: 40,
    paddingLeft: 20,
    paddingBottom: 20,
    marginTop: 20,
  },

  submissionCard: {
    marginRight: 20,
    width: submissionCardWidth,
    flexDirection: 'column',
    padding: 0,
    height: 500,
    borderRadius: 20,
  },

  profileImage: {
    height: 80,
    width: 80,
    borderRadius: 60,
    position: 'absolute',
    left: submissionCardWidth / 2 - 40,
    top: -40,
    borderWidth: 6,
    borderColor: Colors.white.default,
  },

  submissionProfileContainer: {
    paddingTop: 50,
    alignItems: 'center',
  },

  submissionProfileTopLayer: {
    width: submissionCardWidth,
    height: 50,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    position: 'absolute',
    zIndex: -1,
    backgroundColor: Colors.blue.main,
  },

  submissionProfileBottomLayer: {
    width: submissionCardWidth / 2,
    height: 130,
    borderBottomRightRadius: submissionCardWidth / 2,
    borderBottomLeftRadius: submissionCardWidth / 2,
    left: submissionCardWidth / 4,
    top: 20,
    transform: [
      { scaleX: 2 },
    ],
    position: 'absolute',
    zIndex: -1,
    backgroundColor: Colors.blue.main,
  },

  submissionProfileText: [
    GlobalStyles.textRegular(undefined, Colors.white.default),
  ],

  submissionProfileTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(18), Colors.white.default),
    {
      margin: 10,
    },
  ],

  submissionContentContainer: {
    marginTop: 20,
    padding: 20,
    alignItems: 'center',
  },

  submissionContentTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(18)),
  ],

  submissionContentHighlight: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginVertical: 10,
    },
  ],

  submissionInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  submissionInfoTitleContainer: {
    width: submissionCardWidth / 2 - 40,
    alignItems: 'center',
  },

  submissionInfoTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(14)),
    {
      marginBottom: 5,
    },
  ],

  submissionInfoTitleDivider: {
    width: 20,
  },

  submissionInfoHighlightContainer: {
    backgroundColor: Colors.yellow.main,
    width: submissionCardWidth / 2 - 40,
    height: 30,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },

  submissionInfoHighlight: [
    GlobalStyles.textBold(RatioHelper.normalize(14), Colors.blue.main),
  ],

  submissionInfoHighlightDivider: {
    width: 20,
    borderWidth: 2,
    borderColor: Colors.yellow.main,
  },

  submissionContentText: [
    GlobalStyles.textRegular(),
    {
      marginVertical: 20,
    },
  ],

  submissionSettingButton: {
    width: submissionCardWidth - 40,
    marginBottom: 20,
  },

  actionButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: submissionCardWidth - 40,
  },

  actionButton: {
    width: submissionCardWidth / 2 - 30,
  },

  repaymentContentContainer: {
    padding: 20,
  },

  repaymentCard: {
    flexDirection: 'column',
    marginBottom: 15,
  },

  repaymentCardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: RatioHelper.screenWidth - 70,
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.grey.light,
    paddingBottom: 15,
  },

  repaymentCardHeaderTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(14)),
  ],

  repaymentCardHeaderHighlight: [
    GlobalStyles.textBold(undefined, Colors.orange.default),
  ],

  repaymentCardHeaderButton: {
    width: 150,
  },

  repaymentCardInfoContainer: {
    flexDirection: 'row',
    marginTop: 15,
  },

  repaymentCardChartContainer: {
    width: 100,
    height: 100,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },

  repaymentCardDataRow: {
    flexDirection: 'row',
  },

  repaymentCardDataContainer: {
    marginRight: 15,
    marginBottom: 15,
  },

  repaymentCardDataTitle: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
  ],

  repaymentCardDataHighlight: (color = Colors.black.default) => {
    return [
      GlobalStyles.textBold(RatioHelper.normalize(14), color),
    ];
  },

  repaymentCardChartOuterCircle: {
    borderWidth: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    height: 100,
    width: 100,
    position: 'absolute',
    borderColor: Colors.grey.lightest,
  },

  repaymentCardChartText: [
    GlobalStyles.textBold(RatioHelper.normalize(18)),
  ],

  employeeCardsContainer: {
    padding: 20,
  },

  employeeCard: (selected = false) => {
    return [
      {
        marginRight: 20,
        alignItems: 'center',
        width: RatioHelper.screenWidth * 2 / 3,
        flexDirection: 'row',
        padding: 9.5,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: Colors.grey.light,
      },
      selected && GlobalStyles.shadow,
    ];
  },

  employeeCardImage: {
    width: 45,
    height: 45,
    borderRadius: 45,
    marginRight: 10,
  },

  cardFadeText: [
    GlobalStyles.textRegular(RatioHelper.normalize(12), Colors.grey.default),
    {
      marginBottom: 5,
    },
  ],

  employeeCardBoldText: [
    GlobalStyles.textBold(RatioHelper.normalize(14)),
  ],
  
  helpCard: {
    height: 250,
    padding: 20,
  },

  helpCardTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(18)),
    {
      marginBottom: 10,
    },
  ],

  helpCardHighlight: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 10,
    },
  ],

  helpCardText: [
    GlobalStyles.textRegular(),
    {
      textAlign: 'center',
      marginBottom: 20,
    },
  ],

  helpCardSlider: {
    width: RatioHelper.screenWidth - 40,
  },
};
