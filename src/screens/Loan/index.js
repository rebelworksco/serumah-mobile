import React, { useState } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

// import { useSelector } from 'react-redux';
import {
  Container,
  Button,
  Card,
  BottomCard,
} from 'components';
import {
  // RatioHelper,
  // HooksHelper,
} from 'helpers';
import {
  Colors,
} from 'consts';
import {
  // actions,
} from 'store';
import Slider from '@react-native-community/slider';

import Styles from './style';

const Loan = props => {
  const [activeTab, setActiveTab] = useState(0);
  const [selectedEmployee, setSelectedEmployee] = useState(0);
  const [openHelpCard, setOpenHelpCard] = useState(false);
  
  const tabs = [
    {
      id: 0,
      name: 'Ajuan Pinjaman',
    },
    {
      id: 1,
      name: 'Pelunasan',
    },
  ];

  const submissions = [
    {
      id: 0,
      profile: {
        name: 'Rita Pratiwi Sutrisno',
        role: 'Baby Sitter',
        imageUri: 'https://randomuser.me/api/portraits/women/25.jpg',
      },
    },
    {
      id: 1,
      profile: {
        name: 'John Doe',
        role: 'Supir Pribadi',
        imageUri: 'https://randomuser.me/api/portraits/men/40.jpg',
      },
    },
  ];

  const employees = [
    {
      id: 0,
      name: 'Rita Pratiwi Sutrisno',
      role: 'Baby Sitter',
      imageUri: 'https://randomuser.me/api/portraits/women/25.jpg',
      history: {
        late: 10,
        absent: 4,
        overtime: 28,
      },
    },
    {
      id: 1,
      name: 'John Doe',
      role: 'Supir Pribadi',
      imageUri: 'https://randomuser.me/api/portraits/men/40.jpg',
      history: {
        late: 1,
        absent: 0,
        overtime: 10,
      },
    },
  ];

  const renderTabs = () => (
    <View style={ Styles.tabsContainer }>
      {
        tabs.map(tab => (
          <TouchableOpacity
            key={ tab.id }
            style={ Styles.tabContainer(activeTab === tab.id) }
            onPress={ () => { setActiveTab(tab.id); } }
          >

            <Text style={ Styles.tabText(activeTab === tab.id) }>{ tab.name }</Text>

          </TouchableOpacity>
        ))
      }
    </View>
  );

  const renderSubmissionCards = () => (
    submissions.map(submission => (
      <Card key={ submission.id } style={ Styles.submissionCard }>

        <Image
          style={ Styles.profileImage }
          source={ { uri: submission.profile.imageUri } }
        />

        <View style={ Styles.submissionProfileTopLayer }  />
        <View style={ Styles.submissionProfileBottomLayer }  />

        <View style={ Styles.submissionProfileContainer }>

          <Text style={ Styles.submissionProfileText }>{ submission.profile.role }</Text>
          <Text style={ Styles.submissionProfileTitle }>{ submission.profile.name }</Text>
          <Text style={ Styles.submissionProfileText }>10/10//2019</Text>

        </View>

        <View style={ Styles.submissionContentContainer }>

          <Text style={ Styles.submissionContentTitle }>Total Pinjaman</Text>
          <Text style={ Styles.submissionContentHighlight }>IDR 5,000,000</Text>

          <View style={ Styles.submissionInfoContainer }>

            <View style={ Styles.submissionInfoTitleContainer }>
              <Text style={ Styles.submissionInfoTitle }>Cicilan</Text>
            </View>

            <View style={ Styles.submissionInfoTitleDivider }/>

            <View style={ Styles.submissionInfoTitleContainer }>
              <Text style={ Styles.submissionInfoTitle }>Nominal</Text>
            </View>

          </View>

          <View style={ Styles.submissionInfoContainer }>

            <View style={ Styles.submissionInfoHighlightContainer }>
              <Text style={ Styles.submissionInfoHighlight }>10 bulan</Text>
            </View>

            <View style={ Styles.submissionInfoHighlightDivider }/>

            <View style={ Styles.submissionInfoHighlightContainer }>
              <Text style={ Styles.submissionInfoHighlight }>IDR 500,000</Text>
            </View>

          </View>

          <Text style={ Styles.submissionContentText }>Membeli hadiah ulang tahun untuk anak saya.</Text>

          <Button
            style={ Styles.submissionSettingButton }
            alt
            text='Atur Pinjaman'
          />

          <View style={ Styles.actionButtonContainer }>

            <Button
              style={ Styles.actionButton }
              containerColor={ Colors.red.danger }
              text='Tolak'
            />

            <Button
              style={ Styles.actionButton }
              containerColor={ Colors.green.success }
              text='Terima'
            />

          </View>
        </View>

      </Card>
    ))
  );

  const renderSubmissionContent = () => {
    if (activeTab === 0) {
      return (
        <>

          <ScrollView
            style={ Styles.submissionCardsContainer }
            horizontal
            bounces={ false }
            showsHorizontalScrollIndicator={ false }
          >
            
            { renderSubmissionCards() }

          </ScrollView>

        </>
      );
    }
  };

  const renderEmployeeCards = () => (
    employees.map(employee => (
      <TouchableOpacity
        key={ employee.id }
        onPress={ () => { setSelectedEmployee(employee.id); } }
      >

        <View style={ Styles.employeeCard(employee.id === selectedEmployee) }>

          <Image style={ Styles.employeeCardImage } source={ { uri: employee.imageUri } } />

          <View>

            <Text style={ Styles.cardFadeText }>{ employee.role }</Text>
            <Text style={ Styles.employeeCardBoldText }>{ employee.name }</Text>

          </View>

        </View>

      </TouchableOpacity>
    ))
  );

  const renderRepaymentCards = () => (
    [50, 100, 75, 25].map(percent => (
      <Card key={ percent } style={ Styles.repaymentCard }>
        
        <View style={ Styles.repaymentCardHeader }>

          <View style={ Styles.repaymentCardHeaderContent }>
            <Text style={ Styles.repaymentCardHeaderTitle }>Total Pinjaman</Text>
            <Text style={ Styles.repaymentCardHeaderHighlight }>IDR 5,000,000</Text>
          </View>

          <Button
            style={ Styles.repaymentCardHeaderButton }
            text='Bantu Pelunasan'
            onPress={ () => { setOpenHelpCard(true); } }
          />

        </View>

        <View style={ Styles.repaymentCardInfoContainer }>

            <View style={ Styles.repaymentCardChartContainer }>

              <View style={ Styles.repaymentCardChartOuterCircle } />

              <AnimatedCircularProgress
                size={ 80 }
                width={ 5 }
                fill={ percent }
                tintColor={ Colors.blue.main }
                backgroundColor={ Colors.white.default }
                rotation={ 0 }
              >
                {
                  () => (
                    <Text style={ Styles.repaymentCardChartText }>
                      { percent }%
                    </Text>
                  )
                }
              </AnimatedCircularProgress>

            </View>

            <View>

              <View style={ Styles.repaymentCardDataRow }>

                <View style={ Styles.repaymentCardDataContainer }>

                  <Text style={ Styles.repaymentCardDataTitle }>Lunas</Text>
                  <Text style={ Styles.repaymentCardDataHighlight(Colors.green.success) }>IDR 4,000,000</Text>

                </View>

                <View style={ Styles.repaymentCardDataContainer }>

                  <Text style={ Styles.repaymentCardDataTitle }>Sisa</Text>
                  <Text style={ Styles.repaymentCardDataHighlight(Colors.red.danger) }>IDR 500,000</Text>

                </View>

              </View>

              <View style={ Styles.repaymentCardDataRow }>

                <View style={ Styles.repaymentCardDataContainer }>

                  <Text style={ Styles.repaymentCardDataTitle }>Lunas</Text>
                  <Text style={ Styles.repaymentCardDataHighlight() }>IDR 4,000,000</Text>

                </View>

                <View style={ Styles.repaymentCardDataContainer }>

                  <Text style={ Styles.repaymentCardDataTitle }>Lunas</Text>
                  <Text style={ Styles.repaymentCardDataHighlight() }>IDR 4,000,000</Text>

                </View>

              </View>

            </View>

          </View>

      </Card>
    ))
  );

  const renderRepaymentContent = () => {
    if (activeTab === 1) {
      return (
        <>

          <ScrollView
            style={ Styles.employeeCardsContainer }
            horizontal
            bounces={ false }
            showsHorizontalScrollIndicator={ false }
          >

            { renderEmployeeCards() }

          </ScrollView>

          <View style={ Styles.repaymentContentContainer }>

            { renderRepaymentCards() }

          </View>
        </>
      );
    }
  };

  const renderHelpBottomCard = () => (
    <BottomCard bg isOpen={ openHelpCard } style={ Styles.helpCard } type={ 'custom' }>
      <Text style={ Styles.helpCardTitle }>Bantu Pelunasan</Text>
      <Text style={ Styles.helpCardHighlight }>IDR 2,000,000</Text>

      <Slider
        style={ Styles.helpCardSlider }
        minimumValue={ 0 }
        maximumValue={ 1 }
        minimumTrackTintColor='#FFFFFF'
        maximumTrackTintColor='#000000'
      />
      <Text style={ Styles.helpCardText }>Proses bantuan pelunasan dilakukan secara manual antara Anda dan pekerja.</Text>

      <Button
        text='Bantu Pelunasan'
      />
    </BottomCard>
  );


  return (
    <>
      <Container
        style={ Styles.container }
        bg='default'
        header={ {
          onPressRight: () => Actions.giveLoan(),
          rightButton: 'ios-add',
          rightButtonColor: Colors.white.default,
          middleText: 'Pinjaman',
          middleTextColor: Colors.white.default,
        } }
       >

        { renderTabs() }

        <ScrollView
          style={ Styles.scrollView }
          bounces={ false }
          showsVerticalScrollIndicator={ false }
        >

          { renderSubmissionContent() }

          { renderRepaymentContent() }

        </ScrollView>

        
        
      </Container>

      { renderHelpBottomCard() }
    </>
  );
};

export default Loan;
