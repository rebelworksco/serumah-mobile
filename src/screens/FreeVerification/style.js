import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  diamondTop: GlobalStyles.diamond(),

  diamondBottom: GlobalStyles.diamond(RatioHelper.screenHeight * 3 / 5, RatioHelper.screenHeight / 4),
  
  card: {
    height: 500,
    width: RatioHelper.screenWidth - 40,
    flexDirection: 'column',
    padding: 20,
    alignItems: 'center',
  },

  image: {
    resizeMode: 'contain',
    height: 180,
    width: 180,
    marginBottom: 10,
  },

  title: [
    GlobalStyles.textBold(undefined, undefined, 'center'),
    {
      marginBottom: 30,
    },
  ],

  info: [
    GlobalStyles.textBold(undefined, Colors.blue.main, 'center'),
    {
      marginBottom: 30,
    },
  ],

  text: [
    GlobalStyles.textRegular(undefined, Colors.grey.default, 'center'),
    {
      marginBottom: 20,
    },
  ],

  button: {
    width: RatioHelper.screenWidth - 80,
  },
};
