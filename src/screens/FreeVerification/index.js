import React from 'react';

import {
  Text,
  Image,
} from 'react-native';

import {
  Card,
  Button,
  Container,
} from 'components';

import {
  Images,
} from 'consts';

import {
  Actions,
} from 'react-native-router-flux';

import Styles from './style';

const FreeVerification = () => {

  const onPressContinue = () => {
    Actions.register();
  };

  return (
    <Container
      style={ Styles.container }
      bg='full'
    >
      
      <Card style={ Styles.card }>

        <Image style={ Styles.image } source={ Images.ilu_free_verification } />

        <Text style={ Styles.title }>Selamat! Anda Mendapatkan GRATIS</Text>

        <Text style={ Styles.info }>1 Verifikasi Pekerja</Text>

        <Text style={ Styles.text }>Gunakan untuk memverifikasi pekerja di rumah tangga Anda.</Text>

        <Button
          style={ Styles.button }
          text='Lanjut'
          onPress={ onPressContinue }
          />

      </Card>

    </Container>
  );
};

export default FreeVerification;
