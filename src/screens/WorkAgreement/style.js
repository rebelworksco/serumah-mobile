import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
  },

  title: {
    ...GlobalStyles.textSemiBold(24, Colors.blue.main),
  },

  desc: {
    color: Colors.grey.med,
    marginTop: RatioHelper.normalize(10),
    marginBottom: RatioHelper.normalize(15),
  },

  card: {
    backgroundColor: Colors.blue.mainTransparent,
    flexDirection: 'row',
    marginBottom: RatioHelper.normalize(15),
    marginTop: RatioHelper.normalize(15),
  },

  cardText: {
    color: Colors.blue.main,
    fontSize: RatioHelper.normalize(14),
    paddingRight: RatioHelper.normalize(15),
    paddingLeft: RatioHelper.normalize(15),

  },

  textBold: {
    ...GlobalStyles.textBold(14, Colors.blue.main),
  },

  cardIcon: {
    color: Colors.blue.main,
  },

  sectionTitle: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.navy),
    marginBottom: RatioHelper.normalize(20),
  },

  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: RatioHelper.normalize(20),
  },

  btnWorker: selected => {
    var style = {
      width: RatioHelper.screenWidth / 2 - 30,
      height: RatioHelper.normalize(44),
    };

    if (!selected) {
      style.backgroundColor = Colors.blue.mainTransparent;
    }

    return style;
  },

  input: {
    marginBottom: RatioHelper.normalize(10),
    borderRadius: 8,
    borderWidth: 1,
  },

  overtime: {
    marginTop: RatioHelper.normalize(20),
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },

};
