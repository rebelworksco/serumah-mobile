import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ToggleSwitch from 'toggle-switch-react-native';
import FontAwsome from 'react-native-vector-icons/FontAwesome5';

import { Images, Colors, GlobalStyles } from 'consts';
import { HooksHelper, RatioHelper } from 'helpers';
import { Button, Container, Input, Card, Modal } from 'components';
import Styles from './style';

const WorkAgreement = props => {
  const [form, handleForm] = HooksHelper.useForm({
    workerType: 1,
    role: '',
    salary: '',
    payday: '',
    overtimePrice: '',
    overtimeStart: '',
    overtimeMax: '',
    canOvertime: true,
  });

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [benefitForm, setBenefitForm] = HooksHelper.useForm({
    name: '',
    total: '',
  });

  const renderContent = () => {
    if (form.workerType === 2) {
      return (
        <>
          <Text style={ Styles.sectionTitle }>Gaji Perhari  <Ionicons size={ 25 } color={ Colors.blue.main } name='md-information-circle' /></Text>

          <Input
            value={ form.salary }
            placeholder={ 'Gaji Per Hari' }
            onChangeText={ onChangeText => handleForm(onChangeText, 'salary') }
            style={ Styles.input }
          />
          <Input
            value={ form.payday }
            placeholder={ 'Tanggal gajian' }
            onChangeText={ onChangeText => handleForm(onChangeText, 'payday') }
            style={ Styles.input }
            icon={ 'calendar-alt' }
            iconType={ FontAwsome }
          />
        </>
      );
    } else {
      return (
        <>
          <Text style={ Styles.sectionTitle }>Gaji Pokok Pekerja  <Ionicons size={ 25 } color={ Colors.blue.main } name='md-information-circle' /></Text>
          <Input
            value={ form.salary }
            placeholder={ 'Gaji Per Bulan' }
            onChangeText={ onChangeText => handleForm(onChangeText, 'salary') }
            style={ Styles.input }
          />
          <Input
            value={ form.payday }
            placeholder={ 'Tanggal gajian' }
            onChangeText={ onChangeText => handleForm(onChangeText, 'payday') }
            style={ Styles.input }
            icon={ 'calendar-alt' }
            iconType={ FontAwsome }
          />

          <Text style={ Styles.sectionTitle }>Tunjangan Perhari  <Ionicons size={ 25 } color={ Colors.blue.main } name='md-information-circle' /></Text>
          <Button onPress={ () => setIsModalOpen(true) } alt text={ '+ Tambah Tunjangan' } />

          <View style={ Styles.overtime }>
            <Text style={ Styles.sectionTitle }>Lembur  <Ionicons size={ 25 } color={ Colors.blue.main } name='md-information-circle' /></Text>
            <ToggleSwitch
              isOn={ form.canOvertime }
              onColor={ Colors.blue.main }
              onToggle={ isOn => handleForm(isOn, 'canOvertime') }
            />
          </View>

          { renderOvertime() }
        </>
      );
    }
  };

  const renderOvertime = () => {
    if (form.canOvertime) {
      return (
        <>
          <Input
            value={ form.overtimePrice }
            placeholder={ 'Biaya Lembur per Jam' }
            onChangeText={ onChangeText => handleForm(onChangeText, 'overtimePrice') }
            style={ Styles.input }
          />
          <Input
            value={ form.overtimeStart }
            placeholder={ 'Mulai Jam lembur (WIB)' }
            onChangeText={ onChangeText => handleForm(onChangeText, 'overtimeStart') }
            style={ Styles.input }
            icon= { 'ios-arrow-down' }
            iconType={ Ionicons }
          />
          <Input
            value={ form.overtimeMax }
            placeholder={ 'Maksimal Jam lembur' }
            onChangeText={ onChangeText => handleForm(onChangeText, 'overtimeMax') }
            style={ Styles.input }
            icon= { 'ios-arrow-down' }
            iconType={ Ionicons }
          />
        </>
      );
    }
  };

  const finishForm = () => {
    console.log('tada!');
  };

  const addBenefits = () => {
    console.log('add benefits', benefitForm);
    setIsModalOpen(false);
  };

  const renderModal = () => {
    return (
      <Modal
        height={ 310 }
        style={ { padding: 20, alignItems: 'flex-start' } }
        modalIsOpened={ isModalOpen }
        onPressClose={ () => setIsModalOpen(false) }
        onPress={ () => setIsModalOpen(false) }
      >
        <Text style={ Styles.sectionTitle }>Tambah Tunjangan</Text>
        <Input
          value={ benefitForm.name }
          placeholder={ 'Nama Tunjangan' }
          onChangeText={ onChangeText => setBenefitForm(onChangeText, 'name') }
          style={ Styles.input }
        />
        <Input
          value={ benefitForm.total }
          placeholder={ 'Jumlah (IDR)' }
          onChangeText={ onChangeText => setBenefitForm(onChangeText, 'total') }
          style={ Styles.input }
          keyboardType={ 'numeric' }
        />
        <Button height={ 45 } width={ RatioHelper.screenWidth - 100 } text={ 'Simpan' } onPress={ addBenefits } />
      </Modal>
    );
   
  };

  return (
    <>
      <Container style={ Styles.container }
        header={ { leftButton: 'ios-arrow-back',
        leftButtonColor: Colors.grey.med,
        rightImg: Images.ic_support,
        rightButtonContainerStyle: { justifyContent: 'center', paddingBottom: 7 },
        onPressLeft: () => Actions.pop(),
        rightText2: 'Bantuan',
        rightTextStyle2: GlobalStyles.textSemiBold(16, Colors.blue.main) } }>
        <ScrollView showsVerticalScrollIndicator={ false } bounces={ false } >
        

          <Text style={ Styles.title }>Kesepakatan Kerja</Text>
          <Text style={ Styles.desc }>Atur peran, gaji, lembur pekerja Anda.</Text>

          <Card style={ Styles.card }>
            <Ionicons size={ 20 } style={ Styles.cardIcon } name={ 'ios-information-circle-outline' } />
            <Text style={ Styles.cardText }>Nominal rupiah yang ada di Seruma <Text style={ Styles.textBold }> hanya bersifat informatif</Text>. Kegiatan pembayaran masih bersifat manual antara pemilik rumah dan pekerja.</Text>
          </Card>

          <Text style={ Styles.sectionTitle }>Peran Pekerja  <Ionicons size={ 25 } color={ Colors.blue.main } name='md-information-circle' /></Text>

          <View style={ Styles.btnContainer }>
            <Button textColor={ form.workerType !== 1 ? Colors.blue.main : Colors.white.default } style={ Styles.btnWorker(form.workerType === 1) } text={ 'Pekerja Bulanan' } onPress={ () => handleForm(1, 'workerType') }/>
            <Button textColor={ form.workerType !== 2 ? Colors.blue.main : Colors.white.default } style={ Styles.btnWorker(form.workerType === 2) }  text={ 'Pekerja Harian' } onPress={ () => handleForm(2, 'workerType') }/>
          </View>

          <Input
            value={ form.role }
            placeholder={ 'Peran Pekerja' }
            onChangeText={ onChangeText => handleForm(onChangeText, 'role') }
            style={ Styles.input }
          />

          {
            renderContent()
          }

          <Button text={ 'Selesai' } onPress={ finishForm } />
        </ScrollView>
      </Container>
      {
        renderModal()
      }
    </>
  );
};

export default WorkAgreement;
