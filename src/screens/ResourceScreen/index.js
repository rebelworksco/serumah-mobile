import React from 'react';
import {
  View,
} from 'react-native';
import SuccessNotif  from '../SuccessNotif';
import Styles from './style';

const ResourceScreen = props => {

  // useEffect(() => {
  //   setTimeout(() => {
  //     setmodalIsOpened(true);
  //   }, 2000);
  // }, []);

  return (
    <View style={ Styles.container }>
      {/* <Button text={ 'OK' } alt={ true } icon={ 'ios-add' } />
      <Modal
        height={ 420 }
        modalIsOpened={ modalIsOpened }
        buttonText={ 'OK' }
        title={ 'GRATIS 2 SLOT PEKERJA' }
        desc={ 'Selamat bergabung dengan Seruma. Anda mendapatkan \n GRATIS 2 Slot Pekerja.' }
        boldDesc={ ' GRATIS 2 Slot Pekerja.' }
        onPressClose={ () => setmodalIsOpened(false) }
        onPress={ () => setmodalIsOpened(false) }
        image={ Images.ilu_employee }
      /> */}
      <SuccessNotif title={ 'Pembayaran Berhasil' } desc={ 'Pembayaran telah berhasil. Anda dapat melihat hasil verifikasi pekerja Anda sekarang.' } btnText={ 'Lihat Hasil Verifikasi' } />
    </View>
  );
};
export default ResourceScreen
  ;
