import ResourceScreen from './ResourceScreen';
import Splash from './Splash';
import Login from './Login';
import Pin from './Pin';
import TnC from './TnC';
import ChooseRole from './ChooseRole';
import ReferralCode from './ReferralCode';
import SuccessNotif from './SuccessNotif';
import Subscription from './Subscription';
import Salary from './Salary';
import AddEmployee from './AddEmployee';
import FreeVerification from './FreeVerification';
import Register from './Register';
import Verification from './Verification';
import WorkAgreement from './WorkAgreement';
import NotifSetting from './NotifSetting';
import Profile from './Profile';
import Notification from './Notification';
import Worker from './Worker';
import Result from './Result';
import Payment from './Payment';
import History from './History';
import Review from './Review';
import Attendance from './Attendance';
import Loan from './Loan';
import GiveLoan from './GiveLoan';
import Location from './Location';

export {
	ResourceScreen,
  Splash,
  Login,
  Pin,
  TnC,
  ChooseRole,
  ReferralCode,
  SuccessNotif,
  Subscription,
  Salary,
  FreeVerification,
  Register,
  Verification,
  Result,
  Payment,
  History,
  AddEmployee,
  WorkAgreement,
  NotifSetting,
  Profile,
  Notification,
  Worker,
  Review,
  Attendance,
  Loan,
  GiveLoan,
  Location,
};
