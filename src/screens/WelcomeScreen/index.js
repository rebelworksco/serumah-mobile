import React, { Component } from 'react';
import {
  View,
  Text,
  SafeAreaView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import {  BottomCard } from 'components';
import Styles from './style';

export default class WelcomeScreen extends Component {
  render() {
    return (
      <View style={ Styles.container }>
        <Text style={ Styles.text } onPress={ Actions.pop }>
          Welcome Screen
        </Text>
        <BottomCard title={ ['Total Gaji Bulan Ini', 'IDR 2,300,000'] } type={ 'money' } btnText={ 'Konfirmasi Gajian' } />
      </View>
    );
  }
}
