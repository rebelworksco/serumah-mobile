import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    flex: 1,
    alignItems: 'center',
  },

  image: {
    width: 180,
    height: 180,
    resizeMode: 'contain',
    marginVertical: 20,
  },

  title: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 20,
    },
  ],

  semiTitle: GlobalStyles.textBold(RatioHelper.normalize(16)),

  input: {
    marginTop: 20,
    marginBottom: 10,
    width: 200,
  },

  text: [
    GlobalStyles.textRegular(),
    {
      textAlign: 'center',
      marginBottom: 50,
    },
  ],

  textBold: GlobalStyles.textBold(RatioHelper.normalize(14)),

  button: {
    marginBottom: 20,
  },

  link: GlobalStyles.textBold(RatioHelper.normalize(14), Colors.blue.main),
};
