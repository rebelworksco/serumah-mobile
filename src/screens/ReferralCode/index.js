import React from 'react';
import {
  Text,
  ScrollView,
  Image,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import { useSelector } from 'react-redux';
import {
  Container,
  Button,
  Input,
} from 'components';
import {
  // RatioHelper,
  // HooksHelper,
} from 'helpers';
import {
  // actions,
} from 'store';
import {
  Images,
} from 'consts';

import Styles from './style';

const ReferralCode = props => {
  
  const onPressContinue = () => {
    Actions.freeVerification();
  };

  return (
    <ScrollView showsVerticalScrollIndicator={ false }>

      <Container
        style={ Styles.container }
        header={ {
          onPressLeft: () => Actions.pop(),
          leftButton: 'ios-arrow-round-back',
        } }
      >

        <Image style={ Styles.image } source={ Images.ilu_free } />
        <Text style={ Styles.title }>Gratis 1 Verifikasi Pekerja</Text>
        <Text style={ Styles.semiTitle }>Masukkan Kode Referal dari Teman Anda</Text>

        <Input
          default
          placeholder='Kode Referal'
          style={ Styles.input }
          />

        <Text style={ Styles.text }>Gunakan Verifikasi Pekerja untuk <Text style={ Styles.textBold }>memverifikasi pekerja</Text> di rumah tangga Anda.</Text>
        
        <Button
          text='Lanjut'
          style={ Styles.button }
          onPress={ onPressContinue }
          />

        <Text style={ Styles.link }>Tidak punya Kode Referal</Text>

      </Container>

    </ScrollView>
  );
};

export default ReferralCode;
