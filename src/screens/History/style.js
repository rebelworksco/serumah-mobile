import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    flex: 1,
    paddingBottom: 0,
  },

  title: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 10,
    },
  ],

  text: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
    {
      marginBottom: 20,
    },
  ],

  profileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },

  profileVerifiedIcon: {
    position: 'absolute',
    top: 40,
    left: 40,
    height: 20,
    width: 20,
    borderRadius: 20,
  },

  profileImage: {
    height: 60,
    width: 60,
    borderRadius: 60,
    marginRight: 10,
  },

  profileTextContainer: {
    flexGrow: 1,
  },

  profileFadeText: [
    GlobalStyles.textRegular(RatioHelper.normalize(12), Colors.grey.default),
  ],

  profileBoldText: [
    GlobalStyles.textBold(RatioHelper.normalize(14)),
    {
      marginVertical: 4,
    },
  ],
};
