import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import { useSelector } from 'react-redux';
import {
  Container,
  Card,
  Icon,
} from 'components';
import {
  // RatioHelper,
  // HooksHelper,
} from 'helpers';
import {
  Colors,
} from 'consts';
import {
  // actions,
} from 'store';

import Styles from './style';

const employees = [
  {
    id: 0,
    role: 'Baby Sitter',
    name: 'Rita Pratiwi Sutrisno',
    age: 24,
    imageUri: 'https://randomuser.me/api/portraits/women/25.jpg',
    verified: true,
  },
  {
    id: 2,
    role: 'Supir Pribadi',
    name: 'Budi Sudarsono',
    age: 24,
    imageUri: 'https://randomuser.me/api/portraits/men/40.jpg',
    verified: false,
  },
];
const renderEmployeeCards = () => (
  employees.map(employee => (
    <TouchableOpacity key={ employee.id }>
      <Card style={ Styles.profileContainer }>

        <View style={ Styles.profileImageContainer }>
          <Image style={ Styles.profileImage } source={ { uri: employee.imageUri } } />
          <Image style={ Styles.profileVerifiedIcon } source={ { uri: 'https://thumbs.dreamstime.com/b/tick-icon-vector-symbol-green-checkmark-isolated-white-background-checked-icon-correct-choice-sign-round-shape-check-mark-129190246.jpg' } } />
        </View>

        <View style={ Styles.profileTextContainer }>
          <Text style={ Styles.profileFadeText } >{ employee.role }</Text>
          <Text style={ Styles.profileBoldText }>{ employee.name }</Text>
          <Text style={ Styles.profileFadeText } >{ employee.age } tahun</Text>
        </View>

        <Icon
          name='arrow-forward'
          color={ Colors.grey.default }
          size={ 20 }
          />

      </Card>
    </TouchableOpacity>
  ))
);
const History = props => {

  return (
    <ScrollView style={ Styles.container }>

      <Container
        style={ Styles.container }
        header={ {
          onPressLeft: () => Actions.pop(),
          leftButton: 'ios-arrow-back',
        } }
       >

        <Text style={ Styles.title }>Riwayat Pekerja</Text>
        <Text style={ Styles.text }>Berikut daftar riwayat pekerja Anda.</Text>

        { renderEmployeeCards() }

      </Container>
    </ScrollView>
  );
};

export default History;
