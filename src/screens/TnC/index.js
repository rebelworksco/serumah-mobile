import React from 'react';
import {
  View,
  Text,
  ScrollView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import { useSelector } from 'react-redux';
import {
  Container,
  Button,
  Loader,
  BottomCard,
} from 'components';
import {
  // RatioHelper,
  // HooksHelper,
} from 'helpers';
import {
  // actions,
} from 'store';

import Styles from './style';

const TnC = props => {
  const tncContentProps = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`;
  const isLoadingProps = false;

  // const getTnCProps = HooksHelper.useCustomDispatch(actions.MiscAction.getTnC);

  // useEffect(() => {
  //   if (!tncContentProps) {
  //     getTnCProps();
  //   }
  // }, [tncContentProps]); // eslint-disable-line react-hooks/exhaustive-deps

  const onPressAgree = () => {
    Actions.chooseRole();
  };

  const renderBottomCard = () => (
    <BottomCard>

      <View style={ Styles.bottomTextContainer }>
        <Text style={ Styles.bottomText }>Dengan menekan tombol “Ya, Saya Setuju”. Anda telah menyetujui Syarat & Ketentuan VeriJelas</Text>
      </View>
      
      <Button
        text={ 'Ya, Saya Setuju' }
        onPress={ onPressAgree }
      />

    </BottomCard>
  );

  return (
    <>
      <Container
        style={ Styles.container }
        header={ {
          onPressLeft: () => Actions.pop(),
          leftButton: 'ios-arrow-round-back',
        } }
      >

        <ScrollView
          contentContainerStyle={ props.fromRegister ? { paddingBottom: 150 } : { paddingBottom: 30 } }
          bounces={ false }
          showsVerticalScrollIndicator={ false }
        >

          <Text style={ Styles.title }>Syarat & Ketentuan</Text>

          {
            isLoadingProps
            ?
            <Loader style={ Styles.loaderContainer } imageStyle={ { height: 30, width: 30 } } content/>
            :
            <Text style={ Styles.text }>{ tncContentProps }</Text>
          }

        </ScrollView>

      </Container>
      
      { renderBottomCard() }
    </>
  );
};

export default TnC;
