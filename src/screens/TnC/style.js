import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    flex: 1,
    paddingBottom: 0,
  },

  title: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 20,
    },
  ],

  text: [
    GlobalStyles.textRegular(),
    {
      lineHeight: 20,
    },
  ],

  bottomText: [
    GlobalStyles.textRegular(),
    {
      textAlign: 'center',
      marginBottom: 20,
    },
  ],

  loaderContainer: {
    height: 175,
    // borderWidth: 1,
  },
};
