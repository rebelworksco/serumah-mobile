import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
    zIndex: 20,
    justifyContent: 'flex-start',
  },

  profileContainer: {
    flexDirection: 'row',
  },

  imgBtn: {
    backgroundColor: Colors.grey.borderLight,
    width: RatioHelper.normalize(80),
    height: RatioHelper.normalize(80),
    textAlign: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    ...GlobalStyles.shadow,
  },

  profileIcon: {
    alignSelf: 'center',
    color: Colors.grey.text,
  },

  profileImg: {
    width: RatioHelper.normalize(80),
    height: RatioHelper.normalize(80),
    alignSelf: 'center',
    borderRadius: 100,
  },

  camIconCon: {
    width: RatioHelper.normalize(24),
    height: RatioHelper.normalize(24),
    backgroundColor: Colors.white.default,
    textAlign: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    position: 'absolute',
    right: 0,
    bottom: 0,
    ...GlobalStyles.shadow,
  },

  camIcon: {
    color: Colors.blue.main,
    alignSelf: 'center',
  },

  profileTextContainer: {
    marginLeft: RatioHelper.normalize(25),
    flex: 1,
    justifyContent: 'space-between',
  },

  name: {
    ...GlobalStyles.textSemiBold(18, Colors.white.default),
  },

  phone: {
    color: Colors.white.default,
    fontSize: RatioHelper.normalize(14),
  },

  option: {
    ...GlobalStyles.textSemiBold(14, Colors.yellow.bright),
  },

  card: {
    flexDirection: 'column',
    marginTop: RatioHelper.normalize(20),
    marginBottom: RatioHelper.normalize(5),
  },

  cardTitle: {
    color: Colors.grey.med,
    marginBottom: RatioHelper.normalize(5),
  },

  cardPrice: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.main),
    marginBottom: RatioHelper.normalize(5),
  },

  innerCard: {
    flexDirection: 'row',
    marginBottom: RatioHelper.normalize(15),
  },

  referralImg: {
    height: RatioHelper.normalize(80),
    width: RatioHelper.normalize(72),
  },

  cardTitle2: {
    ...GlobalStyles.textSemiBold(16, Colors.blue.main),
    marginBottom: RatioHelper.normalize(10),
  },

  cardDesc: {
    fontSize: RatioHelper.normalize(12),
    
  },
  
  cardBold: {
    ...GlobalStyles.textSemiBold(12),
  },

  cardText: {
    paddingLeft: RatioHelper.normalize(15),
    paddingRight: RatioHelper.normalize(10),
    paddingTop: RatioHelper.normalize(10),
    flex: 1,
  },

  sectionTitle: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.navy),
    marginTop: RatioHelper.normalize(20),
    marginBottom: RatioHelper.normalize(20),
  },

  optionSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: RatioHelper.normalize(20),
    paddingBottom: RatioHelper.normalize(20),
    borderBottomWidth: 1,
    borderBottomColor: Colors.grey.border,
  },

  optionLabel: (color = Colors.blue.navy) => {
    return {
      color,
    };
  },

};
