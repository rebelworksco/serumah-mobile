import React, { useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import FontAwsome from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-picker';

import { Colors, Images } from 'consts';
import { Container, Card, LineChart, Button } from 'components';
import Styles from './style';

const Profile = props => {
  const [photo, setPhoto] = useState('');

  const options = [
    {
      label: 'Syarat & Ketentuan',
      to: 'tnc',
    }, {
      label: 'Atur Notifikasi',
      to: 'notifSetting',
    }, {
      label: 'Keluar',
      to: 'login',
      color: Colors.red.mutedPastel,
    },
  ];

  const onPressShare = () => {
    console.log('share refferal code');
  };

  const renderOptions = () => {
    return options.map(x => {
      return (
        <TouchableOpacity onPress={ Actions[x.to] } style={ Styles.optionSection } key={ x.label }>
          <Text style={ Styles.optionLabel(x.color) }>{ x.label }</Text>
          <Ionicons size={ 23 } name={ 'ios-arrow-forward' } color={ x.color ? x.color : Colors.grey.med } />
        </TouchableOpacity>
      );
    });
  };

  const onPickImg = () => {
    const imgPickerOptions = {
      title: 'Foto Profil',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(imgPickerOptions, response => {
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: 'data:image/jpeg;base64,' + response.data };
        setPhoto(source);
      }
    });
  };

  return (
    <View style={ Styles.container }>
      <ScrollView bounces={ false }>
        <Container style={ Styles.container } noPaddingTop bg='custom'
          header={ { middleText: 'Profil', rightButton: 'ios-notifications', rightButtonColor: Colors.white.default, onPressRight: () => { Actions.notification(); } } }>

            <View style={ Styles.profileContainer }>
              <View>
                <TouchableOpacity onPress={ onPickImg } style={ Styles.imgBtn }>
                  {
                    photo && photo.uri ? <Image source={ photo } style={ Styles.profileImg } /> : <FontAwsome style={ Styles.profileIcon } size={ 38 } name={ 'user' } solid />
                  }
                </TouchableOpacity>
                <View style={ Styles.camIconCon }>
                  <FontAwsome style={ Styles.camIcon } name={ 'camera' } />
                </View>
              </View>
              <View style={ Styles.profileTextContainer }>
                <Text style={ Styles.name }>
                  Mickey Mouse
                </Text>
                <Text style={ Styles.phone }>
                  +6281234567890
                </Text>
                <Text style={ Styles.option }>
                  Atur Profil
                </Text>
              </View>
            </View>

            <Card style={ Styles.card }>
              <Text style={ Styles.cardTitle }>Pengeluaran Bulan Ini</Text>
              <Text style={ Styles.cardPrice }>IDR 6,300,000</Text>
              <LineChart height={ 140 } />
            </Card>

            <Card style={ Styles.card }>

              <View style={ Styles.innerCard }>
                <Image style={ Styles.referralImg } source={ Images.ilu_referral }/>

                <View style={ Styles.cardText }>
                  <Text style={ Styles.cardTitle2 }>GRATIS 1 Verifikasi Pekerja!</Text>
                  <Text style={ Styles.cardDesc }>Kirim kode referal Anda khusus untuk <Text style={ Styles.cardBold }>sesama pemilik rumah.</Text></Text>
                </View>

              </View>

              <Button solid iconRight={ 'share' } width={ '100%' } text={ 'S3RuM4H2' } onPress={ onPressShare }/>
            </Card>

            <Text style={ Styles.sectionTitle }>Pengaturan</Text>

            { renderOptions() }
        </Container>
      </ScrollView>

    </View>
  );
};

export default Profile;
