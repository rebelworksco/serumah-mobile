import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
  },

  title: {
    ...GlobalStyles.textSemiBold(24, Colors.blue.main),
  },

  desc: {
    color: Colors.grey.med,
    marginTop: RatioHelper.normalize(10),
    marginBottom: RatioHelper.normalize(15),
  },

  item: {
    flexDirection: 'row',
    marginTop: RatioHelper.normalize(10),
    paddingBottom: RatioHelper.normalize(15),
    paddingTop: RatioHelper.normalize(10),

    borderBottomWidth: 1,
    borderBottomColor: Colors.grey.borderLight,
  },

  itemInner: {
    flex: 1,
    flexDirection: 'column',
  },

  bullet: index => {
    return {
      color: index === 0 ? Colors.blue.main : Colors.grey.text,
      marginRight: RatioHelper.normalize(15),
      marginTop: RatioHelper.normalize(5),
    };
  },

  text: {
    color: Colors.blue.navy,
    fontSize: RatioHelper.normalize(14),
    marginBottom: RatioHelper.normalize(10),
  },

  clockIcon: {
    color: Colors.grey.med,
  },

  date: {
    color: Colors.grey.med,
    fontSize: RatioHelper.normalize(12),
  },

};
