import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import FontAwsome from 'react-native-vector-icons/FontAwesome5';

import { Colors } from 'consts';
import { Container } from 'components';
import Styles from './style';

const Notification = props => {

  const notif = [{
    val: 'Hari ini adalah hari gajian pekerja Anda, segera konfirmasi gajian mereka sekarang.',
    date: 'Rabu 02/01/2019, 08:00',
  }, {
    val: 'Selamat bergabung dengan Seruma. Anda telah mendapatkan gratis 2 slot pekerja.',
    date: 'Rabu 01/01/2019, 08:00',
  }];

  const renderNotif = list => {
    return list.map((x, i) => {
      return (
        <View key={ x.date } style={ Styles.item }>
          <FontAwsome size={ 10 } style={ Styles.bullet(i) } solid name={ 'circle' } />
          <View style={ Styles.itemInner }>
            <Text style={ Styles.text }>
              {x.val}
            </Text>
              
            <Text style={ Styles.date }>
              <FontAwsome style={ Styles.clockIcon } name={ 'clock' } />  {x.date}
            </Text>
          </View>
        </View>
      );
    });
  };


  return (
    <View style={ Styles.container }>
      <Container style={ Styles.container }
        header={ { leftButton: 'ios-arrow-back',
        onPressLeft: () => Actions.pop(),
        leftButtonColor: Colors.grey.med } }>
      
          <Text style={ Styles.title }>Notifikasi</Text>
          <Text style={ Styles.desc }>Berikut daftar notifikasi Anda.</Text>
          { renderNotif(notif) }
      </Container>
    </View>
  );
};

export default Notification;
