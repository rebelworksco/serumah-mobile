import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import { useSelector } from 'react-redux';
import {
  Container,
  Button,
  Icon,
  Card,
} from 'components';
import {
  // RatioHelper,
  // HooksHelper,
} from 'helpers';
import {
  Colors,
} from 'consts';
import {
  // actions,
} from 'store';
import { Calendar, LocaleConfig } from 'react-native-calendars';

import Styles from './style';

LocaleConfig.locales.fr = {
  monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
  monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
  dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
};
LocaleConfig.defaultLocale = 'fr';


const Attendance = props => {
  const [activeTab, setActiveTab] = useState(0);
  const [selectedDate, setSelectedDate] = useState('');
  const [selectedEmployee, setSelectedEmployee] = useState(0);
  const [markedDates, setMarkedDates] = useState();

  useEffect(() => {
    setMarkedDates(
      {
        '2020-03-03': { dots: [lateDot] },
        '2020-03-01': { dots: [absentDot, overtimeDot] },
      }
    );
  }, [absentDot, lateDot, overtimeDot]);

  const lateDot = { key: 'late', color: Colors.purple.default, selectedDotColor: Colors.white.default };
  const absentDot = { key: 'absent', color: Colors.red.danger, selectedDotColor: Colors.white.default };
  const overtimeDot = { key: 'overtime', color: Colors.green.success, selectedDotColor: Colors.white.default };

  const tabs = [
    {
      id: 0,
      name: 'Kehadiran',
    },
    {
      id: 1,
      name: 'Riwayat',
    },
  ];

  const employees = [
    {
      id: 0,
      name: 'Rita Pratiwi Sutrisno',
      role: 'Baby Sitter',
      imageUri: 'https://randomuser.me/api/portraits/women/25.jpg',
      history: {
        late: 10,
        absent: 4,
        overtime: 28,
      },
    },
    {
      id: 1,
      name: 'John Doe',
      role: 'Supir Pribadi',
      imageUri: 'https://randomuser.me/api/portraits/men/40.jpg',
      history: {
        late: 1,
        absent: 0,
        overtime: 10,
      },
    },
  ];

  const activities = [
    {
      id: 0,
      time: '00:00',
      name: 'Pulang',
      buttonText: 'Lokasi Pulang',
    },
    {
      id: 1,
      time: '22:00',
      name: 'Lembur',
    },
    {
      id: 2,
      time: '22:00',
      name: 'Datang',
      buttonText: 'Lokasi Datang',
    },
  ];

  const historyItems = [
    {
      id: 0,
      name: 'late',
      color: Colors.purple.default,
      text: 'Telat',
      unit: 'kali',
    },
    {
      id: 1,
      name: 'absent',
      color: Colors.red.danger,
      text: 'Absen',
      unit: 'hari',
    },
    {
      id: 2,
      name: 'overtime',
      color: Colors.green.success,
      text: 'Lembur',
      unit: 'jam',
    },
  ];

  const renderTabs = () => (
    <View style={ Styles.tabsContainer }>
      {
        tabs.map(tab => (
          <TouchableOpacity
            key={ tab.id }
            style={ Styles.tabContainer(activeTab === tab.id) }
            onPress={ () => { setActiveTab(tab.id); } }
          >

            <Text style={ Styles.tabText(activeTab === tab.id) }>{ tab.name }</Text>

          </TouchableOpacity>
        ))
      }
    </View>
  );

  const renderEmployeeCards = () => (
    employees.map(employee => (
      <TouchableOpacity
        key={ employee.id }
        onPress={ () => { setSelectedEmployee(employee.id); } }
      >

        <View style={ Styles.employeeCard(employee.id === selectedEmployee) }>

          <Image style={ Styles.employeeCardImage } source={ { uri: employee.imageUri } } />

          <View>

            <Text style={ Styles.cardFadeText }>{ employee.role }</Text>
            <Text style={ Styles.employeeCardBoldText }>{ employee.name }</Text>

          </View>

        </View>

      </TouchableOpacity>
    ))
  );
  
  const handleDayPress = day => {
    const prevDate = selectedDate;
    const date = day.dateString;
    
    // copy markedDates to manipulate new one
    let newMarkedDates = { ...markedDates };

    // unselect the previous selected date
    if (newMarkedDates[`${prevDate}`] && newMarkedDates[`${prevDate}`].dots) {
      // if the date have dots, keep the dots property's value
      const prevDateDots = [...newMarkedDates[`${prevDate}`].dots];
      newMarkedDates[`${prevDate}`] = { dots: prevDateDots };
    } else {
      delete newMarkedDates[`${prevDate}`];
    }

    // select the new date
    if (newMarkedDates[`${date}`] && newMarkedDates[`${date}`].dots) {
      // if the date have dots, include the dots property
      newMarkedDates[`${date}`] = { ...newMarkedDates[`${date}`], selected: true, selectedColor: Colors.blue.main };
    } else {
      newMarkedDates[`${date}`] = { selected: true, selectedColor: Colors.blue.main, fontWeight: 'bold'  };

    }

    setSelectedDate(date);
    setMarkedDates(newMarkedDates);
  };

  const renderCalendar = () => (
    <Calendar
      current={ selectedDate }
      markedDates={ markedDates }
      markingType={ 'multi-dot' }
      onDayPress= { handleDayPress }
      renderArrow={ direction => {
        if (direction === 'left') {
          return (
            <Icon
              name='arrow-back'
              size={ 25 }
              color={ Colors.grey.default }
            />
          );
        } else if (direction === 'right') {
          return (
            <Icon
              name='arrow-forward'
              size={ 25 }
              color={ Colors.grey.default }
            />
          );
        }
      } }
    />
  );

  const renderActivities = () => (
    activities.map(activity => (
      <View key={ activity.id } style={ Styles.activityContainer }>

        <View style={ Styles.activityDot(activity.buttonText ?
        Colors.purple.default : Colors.green.success) } />

        <Text style={ Styles.activityFadeText }>{ activity.time }</Text>

        <View style={ Styles.activityTextContainer }>

          <Text style={ Styles.activityText }>{ activity.name }</Text>

        </View>

        {
          activity.buttonText ?
          (
            <Button
              text={ activity.buttonText }
              width={ 130 }
              height={ 40 }
              onPress={ () => { Actions.location(); } }
            />
          ) : (
            <>
              <TouchableOpacity>
                <Icon
                  style={ Styles.activityIcon }
                  name='close-circle'
                  color={ Colors.red.danger }
                  size={ 50 }
                />
              </TouchableOpacity>

              <TouchableOpacity>
                <Icon
                  style={ Styles.activityIcon }
                  name='checkmark-circle'
                  color={ Colors.green.success }
                  size={ 50 }
                />
              </TouchableOpacity>
            </>
          )
        }

      </View>
    ))
  );

  const renderAttendanceContent = () => {
    if (activeTab === 0) {
      return (
        <>

          <ScrollView
            style={ Styles.employeeCardsContainer }
            horizontal
            bounces={ false }
            showsHorizontalScrollIndicator={ false }
          >
            
            { renderEmployeeCards() }

          </ScrollView>

          { renderCalendar() }

          <View style={ Styles.activitiesContainer }>

            <Text style={ Styles.activitiesTitle }>Aktivitas</Text>

            { renderActivities() }

          </View>

        </>
      );
    }
  };

  const renderHistoryItems = employee => (
    historyItems.map(item => (
      <View key={ item.id } style={ [Styles.historyItemContainer, item.id === 1 && Styles.historyItemContainerCenter] }>

        <Text style={ Styles.cardFadeText }>{ item.text }</Text>
        <Text style={ Styles.historyText(item.color) }>
          { employee.history[`${ item.name }`] ?
            `${ employee.history[`${ item.name }`] } ${ item.unit }` :
            '-'
          }
        </Text>

      </View>
    ))
  );

  const renderHistoryCards = () => (
    employees.map(employee => (
      <Card key={ employee.id } style={ Styles.historyCard }>

        <View style={ Styles.historyCardProfileContainer }>

          <Image style={ Styles.employeeCardImage } source={ { uri: employee.imageUri } } />

          <View>

            <Text style={ Styles.cardFadeText }>{ employee.role }</Text>
            <Text style={ Styles.employeeCardBoldText }>{ employee.name }</Text>

          </View>

        </View>

        <View style={ Styles.historyListContainer } >

          { renderHistoryItems(employee) }

        </View>

      </Card>
    ))
  );

  const renderHistoryContent = () => {
    if (activeTab === 1) {
      return (
        <View style={ Styles.historyContentContainer }>

          <Text style={ Styles.historyTitle }>Bulan Januari 2019</Text>

          { renderHistoryCards() }

        </View>
      );
    }
  };


  return (
      <Container
        style={ Styles.container }
        bg='default'
        header={ {
          onPressRight: () => Actions.notification(),
          rightButton: 'ios-notifications',
          rightButtonColor: Colors.white.default,
          middleText: 'Absensi',
          middleTextColor: Colors.white.default,
        } }
       >

        { renderTabs() }

        <ScrollView
          style={ Styles.scrollView }
          bounces={ false }
          showsVerticalScrollIndicator={ false }
        >

          { renderAttendanceContent() }

          { renderHistoryContent() }

        </ScrollView>

      </Container>
  );
};

export default Attendance;
