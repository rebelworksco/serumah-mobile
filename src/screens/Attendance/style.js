import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    paddingHorizontal: 0,
    paddingTop: 0,
    paddingBottom: 0,
  },

  tabsContainer: {
    flexDirection: 'row',
  },

  tabContainer: (active = false) => {
    return {
      width: RatioHelper.screenWidth / 2,
      alignItems: 'center',
      height: 50,
      justifyContent: 'center',
      borderBottomWidth: active ? 2 : 0.5,
      borderBottomColor: active ? Colors.blue.main : Colors.grey.light,
    };
  },

  tabText: (active = false) => {
    return [
      active ?
      GlobalStyles.textBold(RatioHelper.normalize(14), Colors.blue.main) :
      GlobalStyles.textRegular(undefined, Colors.grey.default),
    ];
  },

  employeeCardsContainer: {
    padding: 20,
  },

  employeeCard: (selected = false) => {
    return [
      {
        marginRight: 20,
        alignItems: 'center',
        width: RatioHelper.screenWidth * 2 / 3,
        flexDirection: 'row',
        padding: 9.5,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: Colors.grey.light,
      },
      selected && GlobalStyles.shadow,
    ];
  },

  employeeCardImage: {
    width: 45,
    height: 45,
    borderRadius: 45,
    marginRight: 10,
  },

  cardFadeText: [
    GlobalStyles.textRegular(RatioHelper.normalize(12), Colors.grey.default),
    {
      marginBottom: 5,
    },
  ],

  employeeCardBoldText: [
    GlobalStyles.textBold(RatioHelper.normalize(14)),
  ],

  activitiesContainer: {
    padding: 20,
  },

  activitiesTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(20)),
  ],

  activityContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    paddingVertical: 10,
    borderBottomColor: Colors.grey.light,
  },

  activityDot: color => {
    return {
      height: 10,
      width: 10,
      borderRadius: 10,
      backgroundColor: color,
      marginRight: 15,
    };
  },

  activityTextContainer: {
    flexGrow: 1,
  },

  activityFadeText: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
    {
      marginRight: 15,
    },
  ],

  activityText: [
    GlobalStyles.textRegular(),
  ],

  activityIcon: {
    marginLeft: 15,
  },

  historyContentContainer: {
    padding: 20,
  },

  historyTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(20)),
    {
      marginBottom: 15,
    },
  ],

  historyCard: {
    flexDirection: 'column',
    marginBottom: 15,
  },

  historyCardProfileContainer: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.grey.light,
    paddingBottom: 15,
    width: RatioHelper.screenWidth - 80,
  },

  historyListContainer: {
    flexDirection: 'row',
  },

  historyItemContainer: {
    width: (RatioHelper.screenWidth - 80) / 3,
    marginTop: 15,
    alignItems: 'center',
  },

  historyItemContainerCenter: {
    borderRightWidth: 0.5,
    borderRightColor: Colors.grey.light,
    borderLeftWidth: 0.5,
    borderLeftColor: Colors.grey.light,
  },

  historyText: color => {
    return [
      GlobalStyles.textBold(RatioHelper.normalize(18), color),
    ];
  },
};
