import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  scrollView: {
    marginHorizontal: -20,
    paddingHorizontal: 20,
  },

  title: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 10,
    },
  ],

  text: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
    {
      marginBottom: 20,
    },
  ],

  profileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },

  profileImage: {
    height: 60,
    width: 60,
    borderRadius: 60,
    marginRight: 10,
  },

  profileFadeText: [
    GlobalStyles.textRegular(RatioHelper.normalize(12), Colors.grey.default),
  ],

  profileBoldText: [
    GlobalStyles.textBold(RatioHelper.normalize(14)),
    {
      marginVertical: 4,
    },
  ],

  rateContainer: {
    borderTopWidth: 0.5,
    borderBottomWidth: 0.5,
    borderTopColor: Colors.grey.light,
    borderBottomColor: Colors.grey.light,
    paddingVertical: 20,
    alignItems: 'center',
  },

  rateTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(14)),
    {
      marginVertical: 4,
    },
  ],

  rateText: [
    GlobalStyles.textRegular(),
  ],

  starContainer: {
    marginVertical: 20,
    flexDirection: 'row',
  },

  starIcon: {
    marginHorizontal: 5,
  },

  formContainer: {
    marginTop: 20,
  },

  formTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(18)),
    {
      marginBottom: 15,
    },
  ],

  formFadeText: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
    {
      fontWeight: 'normal',
    },
  ],

  input: {
    height: 180,
    marginBottom: 50,
    fontSize: RatioHelper.normalize(14),
    fontWeight: 'normal',
    textAlign: 'left',
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
  },

  button: {
    marginBottom: 20,
  },
};
