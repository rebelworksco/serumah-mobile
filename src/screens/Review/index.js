import React, { useState } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import { useSelector } from 'react-redux';
import {
  Container,
  Button,
  Icon,
  Input,
} from 'components';
import {
  // RatioHelper,
  // HooksHelper,
} from 'helpers';
import {
  Colors,
} from 'consts';
import {
  // actions,
} from 'store';

import Styles from './style';



const Review = props => {
  const [rate, setRate] = useState(3);

  const renderStars = () => (
    <View style={ Styles.starContainer }>
      {
        [1, 2, 3, 4, 5].map(star => (
          <TouchableOpacity
            key={ star }
            onPress={ () => { setRate(star); } }
          >

            <Icon
              style={ Styles.starIcon }
              name='star'
              size={ 40 }
              color={ star <= rate ? Colors.blue.main : Colors.grey.light }
            />

          </TouchableOpacity>
        ))
      }
    </View>
  );

  const getRateText = () => {
    if (rate === 1) { return 'Sangat Buruk'; } else if (rate === 2) { return 'Buruk'; } else if (rate === 3) { return 'Cukup Baik'; } else if (rate === 4) { return 'Baik'; } else if (rate === 5) { return 'Sangat Baik'; }
  };

  return (
      <Container
        header={ {
          onPressLeft: () => Actions.pop(),
          leftButton: 'ios-arrow-back',
          leftButtonColor: Colors.grey.default,
        } }
       >

        <ScrollView
          style={ Styles.scrollView }
          bounces={ false }
          showsVerticalScrollIndicator={ false }
        >

          <Text style={ Styles.title }>Review</Text>
          <Text style={ Styles.text }>Berikan review terhadap pekerja Anda.</Text>

          <View style={ Styles.profileContainer }>

            <View style={ Styles.profileImageContainer }>

              <Image style={ Styles.profileImage } source={ { uri: 'https://randomuser.me/api/portraits/women/25.jpg' } } />

            </View>

            <View>

              <Text style={ Styles.profileFadeText } >Baby Sitter</Text>
              <Text style={ Styles.profileBoldText }>Rita Pratiwi Sutrisno</Text>
              <Text style={ Styles.profileFadeText } >24 tahun</Text>

            </View>

          </View>

          <View style={ Styles.rateContainer }>

            <Text style={ Styles.rateTitle } >Bagaimana kinerja pekerja?</Text>
            
            { renderStars() }

            <Text style={ Styles.rateText } >{ getRateText() }</Text>

          </View>

          <View style={ Styles.formContainer }>
            
            <Text style={ Styles.formTitle }>Kesan Anda <Text style={ Styles.formFadeText } >(Opsional)</Text></Text>

            <Input
              default
              placeholder='tuliskan kesan dan pesan Anda...'
              style={ Styles.input }
              multiline
            />

          </View>

          <Button
            style={ Styles.button }
            text='Kirim'
          />

        </ScrollView>

      </Container>
  );
};

export default Review;
