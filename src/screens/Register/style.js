import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    flex: 1,
  },

  title: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 10,
    },
  ],

  text: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
    {
      marginBottom: 30,
    },
  ],

  bottomContainer: {
    position: 'absolute',
    top: RatioHelper.screenHeight - 300,
    height: 300,
    width: RatioHelper.screenWidth,
    alignItems: 'center',
    padding: 30,
  },

  button: {
    marginBottom: 20,
  },

  bottomText: GlobalStyles.textRegular(),

  link: GlobalStyles.textBold(RatioHelper.normalize(14), Colors.blue.main),
};
