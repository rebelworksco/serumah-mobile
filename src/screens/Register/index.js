import React from 'react';
import {
  Text,
  View,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import { useSelector } from 'react-redux';
import {
  Container,
  Button,
  Input,
} from 'components';
import {
  // RatioHelper,
  HooksHelper,
} from 'helpers';
import {
  // actions,
} from 'store';

import Styles from './style';

const Register = props => {
  const [values, handleChange] = HooksHelper.useForm({
    name: '',
    phone: '',
    email: '',
  });
  
  const onPressContinue = () => {
    Actions.verification();
  };

  return (
    <Container
        style={ Styles.container }
        header={ {
          onPressLeft: () => Actions.pop(),
          leftButton: 'ios-arrow-round-back',
        } }
      >

      <Text style={ Styles.title }>Registrasi</Text>
      <Text style={ Styles.text }>Lengkapi informasi Anda untuk mendaftar.</Text>

      <Input
        value={ values.name }
        onChangeText={ onChangeName => handleChange(onChangeName, 'name') }
        placeholder={ 'Nama Lengkap' }
      />

      <Input
        value={ values.phone }
        onChangeText={ onChangePhone => handleChange(onChangePhone, 'phone') }
        placeholder={ 'Nomor Telepon' }
        keyboardType={ 'phone-pad' }
        maxLength={ 20 }
      />

      <Input
        value={ values.email }
        onChangeText={ onChangeEmail => handleChange(onChangeEmail, 'email') }
        placeholder={ 'Email (Optional)' }
        keyboardType={ 'phone-pad' }
        maxLength={ 20 }
      />

      <View style={ Styles.bottomContainer }>

        <Button
          style={ Styles.button }
          text='Lanjut'
          onPress={ onPressContinue }
        />

        <Text style={ Styles.bottomText }>Sudah punya akun? <Text style={ Styles.link }>Masuk</Text></Text>

      </View>

    </Container>
  );
};

export default Register;
