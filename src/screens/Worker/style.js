import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
  },

  headerBtn2: {
    marginLeft: RatioHelper.normalize(15),
    marginRight: RatioHelper.normalize(10),

  },

  cardNotif: {
    backgroundColor: Colors.blue.mainTransparent,
    flexDirection: 'row',
    marginBottom: RatioHelper.normalize(15),
    marginTop: RatioHelper.normalize(5),
    justifyContent: 'space-between',
    padding: RatioHelper.normalize(10),
  },

  cardNotifTitle: {
    ...GlobalStyles.textSemiBold(14, Colors.blue.main),
  },

  cardNotifDesc: {
    color: Colors.blue.main,
    fontSize: 12,
    marginTop: RatioHelper.normalize(3),

  },

  useBtn: {
    borderRadius: 4,
    width: RatioHelper.normalize(90),
    height: RatioHelper.normalize(35),
  },

  

};
