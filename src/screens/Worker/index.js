import React, { useState } from 'react';
import {
  View,
  Text,
  ScrollView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import { Colors, Images } from 'consts';
import { Container, Card, Button } from 'components';

import EmptyCard from './components/EmptyCard';
import WorkerCard from './components/WorkerCard';
import Styles from './style';

const Worker = props => {
  const [workerSlot, setWorkerSlot] = useState([{
    name: 'Rita Pratiwi Sutrisno',
    role: 'Baby Sitter',
    status: 'Hari ini Lembur',
    salary: 'IDR 2,300,000',
    isVerif: false,
    img: Images.bg_houses,
  }, {
    name: 'Rita Pratiwi Sutrisno',
    role: 'Baby Sitter',
    status: 'Hari ini Absen',
    salary: 'IDR 2,300,000',
    isVerif: true,
    img: Images.ilu_free,
  }, {}]);

  const onPressUseVoucher = () => {
    console.log('pake voucher');
  };

  const addWorkerSlot = () => {
    setWorkerSlot([...workerSlot, {}]);
    Actions.subscription();
  };

  const useWorkerSlot = () => {
    Actions.addEmployee();
  };

  const toSalary = () => {
    Actions.salary();
  };

  const renderWorker = (list = workerSlot) => {
    return list.map((x, i) => {
      if (!x.name) {
        return <EmptyCard key={ i } useWorkerSlot={ useWorkerSlot } />;
      } else {
        return <WorkerCard key={ i } data={ x } confirmPayday={ toSalary }/>;
      }
    });
  };

  const renderVoucher = voucher => {
    return (
      <Card style={ Styles.cardNotif }>
        <View>
          <Text style={ Styles.cardNotifTitle }>Anda punya 1 Verifikasi Pekerja!</Text>
          <Text style={ Styles.cardNotifDesc }>Gunakan untuk verifikasi pekerja Anda</Text>
        </View>
        <Button text={ 'Gunakan' } onPress={ onPressUseVoucher } style={ Styles.useBtn } fontSize={ 14 }/>
      </Card>
    );
  };

  return (
    <ScrollView bounces={ false }>
      <Container
        style={ Styles.container }
        bg='default'
        header={ {
          middleText: 'Pekerja',
          rightBtnFontAwsome: true,
          rightButton: 'history',
          rightButtonColor: Colors.white.default,
          rightButtonSize: 22,
          rightButton2: 'ios-notifications',
          rightButton2Color: Colors.white.default,
          rightButton2Size: 28,
          rightButton2ContainerStyle: Styles.headerBtn2,
        } }>
        
          { renderVoucher() }

          { renderWorker() }

          <Button text={ '+ Tambah Slot Pekerja' } alt onPress={ addWorkerSlot } />

      </Container>
    </ScrollView>
  );
};

export default Worker;
