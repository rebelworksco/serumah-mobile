import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  useBtn: {
    borderRadius: 4,
    width: RatioHelper.normalize(90),
    height: RatioHelper.normalize(35),
  },

  card: {
    borderRadius: 8,
    borderStyle: 'dashed',
    borderColor: Colors.grey.text,
    borderWidth: 1,
    flex: 1,
    padding: RatioHelper.normalize(15),
    marginBottom: RatioHelper.normalize(25),
  },

  cardInner: {
    flexDirection: 'row',
    paddingBottom: RatioHelper.normalize(20),
    borderBottomWidth: 1,
    borderBottomColor: Colors.grey.borderLight,
  },

  imgContainer: {
    borderRadius: 100,
    backgroundColor: Colors.blue.mainTransparent,
    width: RatioHelper.normalize(56),
    height: RatioHelper.normalize(56),
    justifyContent: 'center',
    alignSelf: 'center',
    marginRight: RatioHelper.normalize(20),
  },

  plusIcon: {
    textAlign: 'center',
    paddingTop: 5,
  },

  cardTextContainer: {
    flex: 2,
    flexDirection: 'column',
  },

  btnContainer: {
    flex: 1,
    justifyContent: 'center',
  },

  cardBottom: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: RatioHelper.normalize(10),
    paddingTop: RatioHelper.normalize(15),
  },

  cardDesc: {
    color: Colors.grey.text,
    marginTop: RatioHelper.normalize(5),

  },

  cardName: {
    ...GlobalStyles.textSemiBold(14, Colors.grey.text),
    marginTop: RatioHelper.normalize(5),
  },

  cardBold: {
    ...GlobalStyles.textSemiBold(14, Colors.grey.text, 'center'),
    marginTop: RatioHelper.normalize(5),
  },

  cardBottomSection1: {
    flex: 1,
    borderRightWidth: 1,
    borderRightColor: Colors.grey.borderLight,
  },

  cardBottomSection2: {
    flex: 1,
  },

};
