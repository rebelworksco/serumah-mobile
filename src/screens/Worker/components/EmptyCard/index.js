import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { Colors } from 'consts';
import { Button } from 'components';
import Styles from './style';

const EmptyCard = props => {
  const { useWorkerSlot } = props;

  return (
    <View style={ Styles.card }>
      <View style={ Styles.cardInner }>
        <View style={ Styles.imgContainer }>
          <Ionicons style={ Styles.plusIcon } name={ 'ios-add' } color={ Colors.blue.main } size={ 38 }/>
        </View>
        <View style={ Styles.cardTextContainer }>
          <Text style={ Styles.cardDesc }>Peran</Text>
          <Text style={ Styles.cardName }>Nama Pekerja</Text>
          <Text style={ Styles.cardDesc }>Status Gajian</Text>
        </View>
        <View style={ Styles.btnContainer }>
          <Button text={ 'Gunakan' } onPress={ useWorkerSlot } style={ Styles.useBtn } fontSize={ 14 }/>
        </View>
      </View>
      <View style={ Styles.cardBottom }>
        <TouchableOpacity style={ Styles.cardBottomSection1 }>
          <Text style={ Styles.cardBold }>Konfirmasi Gajian</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ Styles.cardBottomSection2 }>
          <Text style={ Styles.cardBold }>Beri Pinjaman</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

};

export default EmptyCard;
