import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { Colors } from 'consts';
import { Button, Card } from 'components';
import Styles from './style';

const EmptyCard = props => {
  const { confirmPayday, giveLoan, data } = props;
  const { name, role, img, isVerif, status, salary } = data;

  return (
    <Card style={ Styles.card } >
      <View style={ Styles.salaryContainer }>
        <Text style={ Styles.salary }>{ salary }</Text>
      </View>
      <View style={ Styles.cardInner }>
        <View>
          <Image source={ img } style={ Styles.img } />
          <Ionicons  style={ Styles.icon(isVerif) } name={ isVerif ? 'ios-checkmark-circle' : 'ios-alert' } size={ 24 } />
        </View>
        <View style={ Styles.cardTextContainer }>
          <Text style={ Styles.cardDesc }>{ role }</Text>
          <Text style={ Styles.name }>{ name }</Text>
          <Text style={ Styles.status(status) }>{ status }</Text>
        </View>
      </View>
      <View style={ Styles.cardBottom }>
        <TouchableOpacity style={ Styles.cardBottomSection1 } onPress={ confirmPayday }>
          <Text style={ Styles.cardBottomText }>Konfirmasi Gajian</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ Styles.cardBottomSection2 }  onPress={ giveLoan }>
          <Text style={ Styles.cardBottomText }>Beri Pinjaman</Text>
        </TouchableOpacity>
      </View>
    </Card>
  );

};

export default EmptyCard;
