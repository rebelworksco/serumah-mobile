import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  card: {
    flex: 1,
    paddingBottom: RatioHelper.normalize(15),
    paddingTop: 0,
    paddingRight: 0,
    paddingLeft: RatioHelper.normalize(15),
    marginBottom: RatioHelper.normalize(25),
    flexDirection: 'column',
  },

  icon: isVerif =>  {
    return {
      color: isVerif ? Colors.green.success : Colors.red.mutedPastel,
      alignSelf: 'center',
      position: 'absolute',
      right: 17,
      bottom: 0,
    };
  },

  img: {
    borderRadius: 100,
    width: RatioHelper.normalize(56),
    height: RatioHelper.normalize(56),
    marginRight: RatioHelper.normalize(20),
  },

  cardInner: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: RatioHelper.normalize(20),
    borderBottomWidth: 1,
    borderBottomColor: Colors.grey.borderLight,
    marginTop: RatioHelper.normalize(15),
    marginRight: RatioHelper.normalize(15),
  },

  cardTextContainer: {
    flex: 2,
    flexDirection: 'column',
  },

  btnContainer: {
    flex: 1,
    justifyContent: 'center',
  },

  cardBottom: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: RatioHelper.normalize(10),
    paddingTop: RatioHelper.normalize(15),
  },

  cardDesc: {
    color: Colors.grey.text,
    marginTop: RatioHelper.normalize(5),
  },

  name: {
    ...GlobalStyles.textSemiBold(14, Colors.blue.navy),
    marginTop: RatioHelper.normalize(5),
  },

  status: stat => {
    let color = stat.includes('Absen') ? Colors.red.mutedPastel : Colors.green.success;
    return {
      ...GlobalStyles.textSemiBold(12, color),
      marginTop: RatioHelper.normalize(5),
    };
  },

  cardBold: {
    ...GlobalStyles.textSemiBold(14, Colors.grey.text, 'center'),
    marginTop: RatioHelper.normalize(5),
  },

  cardBottomSection1: {
    flex: 1,
    borderRightWidth: 1,
    borderRightColor: Colors.grey.borderLight,
  },

  cardBottomSection2: {
    flex: 1,
  },

  cardBottomText: {
    ...GlobalStyles.textSemiBold(14, Colors.blue.main, 'center'),
  },

  salaryContainer: {
    backgroundColor: Colors.blue.mainTransparent,
    position: 'absolute',
    top: 0,
    right: 0,
    padding: RatioHelper.normalize(10),
    borderTopRightRadius: 8,
    borderBottomLeftRadius: 8,
  },

  salary: {
    ...GlobalStyles.textSemiBold(14, Colors.blue.main),
  },

};
