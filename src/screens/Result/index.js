import React, { useState } from 'react';

import {
  ScrollView,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';

import {
  Icon,
  BottomCard,
  Input,
  Card,
  Button,
  Link,
  Container,
  Modal,
} from 'components';

import {
  Colors,
  Images,
} from 'consts';

import { Actions } from 'react-native-router-flux';
import Styles from './style';


const Result = props => {
  const [selectedTab, setSelectedTab] = useState(0);
  const [editing, setEditing] = useState(false);
  const [employed, setEmployed] = useState(false);
  const [verified, setVerified] = useState(false);
  const [openModalVerify, setOpenModalVerify] = useState(false);
  const [openModalTerminate, setOpenModalTerminate] = useState(false);

  const tabs = [
    {
      id: 0,
      text: 'Identitas',
    },
    {
      id: 1,
      text: 'Catatan Kepolisian',
    },
    {
      id: 2,
      text: 'Review',
    },
  ];

  const bioData = [
    {
      id: 0,
      field: 'NIK',
      value: '1271191002960013',
      status: 0,
    },
    {
      id: 1,
      field: 'Nomor Telepon',
      value: '+6181234567890',
      status: 1,
    },
    {
      id: 2,
      field: 'Jenis Kelamin',
      value: 'Female',
      status: 0,
    },
    {
      id: 3,
      field: 'Tempat/Tgl Lahir',
      value: 'Medan, 01 Januari 1996',
      status: 0,
    },
    {
      id: 4,
      field: 'Alamat',
      value: 'Jl. Sultan Iskandar Muda No.6B, RT.7/RW.9, Kby. Lama Sel., Kec. Kby. Lama, Kebayoran Lama, Daerah Khusus Ibukota Jakarta 12240',
      status: 2,
    },
    {
      id: 5,
      field: 'Agama',
      value: 'Islam',
      status: 0,
    },
    {
      id: 6,
      field: 'Status Perkawinan',
      value: 'Belum Kawin',
      status: 0,
    },
    {
      id: 7,
      field: 'Pekerjaan',
      value: 'Belum/Tidak Bekerja',
      status: 0,
    },
  ];

  const reviews = [
    {
      id: 0,
      imageUri: { uri: 'https://randomuser.me/api/portraits/women/42.jpg' },
      name: 'Jane Wiraga',
      month: 24,
      star: 4,
      review: 'Kinerja Rita Pratiwi Sutrisno overall sangat bagus selama bekerja dengan saya, anaknya sangat rajin dan selalu beres mengerjakan task dari saya. Kekurangannya paling sering cuti duluan sebelum waktunya libur.',
    },
    {
      id: 1,
      imageUri: { uri: 'https://randomuser.me/api/portraits/women/42.jpg' },
      name: 'Jane Wiraga',
      month: 24,
      star: 4,
      review: 'Kinerja Rita Pratiwi Sutrisno overall sangat bagus selama bekerja dengan saya, anaknya sangat rajin dan selalu beres mengerjakan task dari saya. Kekurangannya paling sering cuti duluan sebelum waktunya libur.',
    },
  ];

  const renderTabs = () => (
    <View style={ Styles.tabsContainer } >
      {
        tabs.map(tab => (
          <TouchableOpacity
            key={ tab.id }
            style={ Styles.tabContainer(tab.id === selectedTab) }
            onPress={ () => { setSelectedTab(tab.id); } }>
            <Text style={ Styles.tabText(tab.id === selectedTab) }>{ tab.text }</Text>
          </TouchableOpacity>
        ))
      }
    </View>
  );

  const renderModalVerify = () => (
    <Modal
      type='info'
      height={ 400 }
      image={ Images.ilu_alert }
      title='Pekerjakan Tanpa Verifikasi?'
      desc='Anda akan mempekerjakan pekerja tanpa verifikasi. Segala resiko akan ditanggung oleh pemilik rumah.'
      buttonAlt
      buttonText='Pekerjakan'
      buttonIcon='add'
      button2Text='Batal'
      button2Type='link'
      button2Color={ Colors.red.danger }
      modalIsOpened={ openModalVerify }
      onPress={ () => { Actions.workAgreement(); setOpenModalVerify(false);  } }
      onPress2={ () => setOpenModalVerify(false) }
      onPressClose={ () => setOpenModalVerify(false) }
    />
  );

  const renderModalTerminate = () => (
    <Modal
      type='info'
      height={ 450 }
      image={ Images.ilu_alert }
      title='Putus Hubungan Kerja?'
      desc='Anda akan memutuskan hubungan kerja dengan Rita Pratiwi Sutrisno. Apakah Anda Yakin?'
      buttonAlt
      buttonText='Ya'
      button2Text='Tidak'
      modalIsOpened={ openModalTerminate }
      onPress={ () => { Actions.workAgreement(); setOpenModalTerminate(false);  } }
      onPress2={ () => setOpenModalTerminate(false) }
      onPressClose={ () => setOpenModalTerminate(false) }
    />
  );

  const renderBio = () => (
    bioData.map(data => (
      <View key={ data.id } style={ Styles.formRow }>
      <Text style={ Styles.field }>{ data.field }</Text>
      {
        editing ?
        (
          <>
            <Input
              default
              multiline
              style={ [Styles.input, data.id === 4 && { height: 109, width: 250, textAlign: 'auto' }] }
              value={ data.value }
            />
          </>
        ) :
        (
          <>
            <Text style={ Styles.value }>{ data.value }</Text>
            {
              verified ?
              (
                <Icon
                  style={ Styles.icon }
                  name={ 'checkmark-circle' }
                  color={ Colors.green.success }
                />
              ) : (
                <Icon
                  style={ Styles.icon }
                  name={ data.status ? data.status === 1 ? 'checkmark-circle' : 'alert' : 'remove-circle' }
                  color={ data.status ? data.status === 1 ? Colors.green.success : Colors.red.danger : Colors.grey.light }
                />
              )
            }
          </>
        )
      }
      </View>
    ))
  );

  const renderEditButton = () => {
    if (editing) {
      return (
        <Link
          text='Selesai'
          onPress={ () => { setEditing(!editing); } }
        />
      );
    } else {
      return (
        <Link
          text='Atur Data Diri'
          icon='create'
          onPress={ () => { setEditing(!editing); } }
        />
      );
    }
  };

  const renderIdentityActions = () => {
    if (verified && employed) {
      return (
        <Link
          style={ Styles.identityLink }
          text='Putus hubungan kerja'
          color={ Colors.red.danger }
          onPress={ () => { setOpenModalTerminate(true); } }
        />
      );
    } else if (verified && !employed) {
      return (
        <>
          <Button
            style={ Styles.identityButton }
            text='Pekerjakan'
            icon='add'
            onPress={ () => { setEmployed(true); } }
          />
          
          <Link
            style={ Styles.identityLink }
            text='Lewati'
          />
        </>
      );
    }
  };

  const renderIdentityContent = () => {
    if (selectedTab === 0) {
      return (
        <>
          <View style={ Styles.formRow }>

            <Text style={ Styles.title }>Data Diri</Text>

            { renderEditButton() }

          </View>

          { renderBio() }

          <View style={ Styles.iconInfosContainer }>

            <View style={ Styles.iconInfoContainer }>

              <Icon style={ Styles.iconInfo } name='checkmark-circle' color={ Colors.green.success }/>
              <Text style={ Styles.iconInfoText }>Terverifikasi</Text>

            </View>

            <View style={ Styles.iconInfoContainer }>

              <Icon style={ Styles.iconInfo } name='alert' color={ Colors.red.danger } />
              <Text style={ Styles.iconInfoText }>Tidak Sesuai</Text>

            </View>
            
            <View style={ Styles.iconInfoContainer }>

              <Icon style={ Styles.iconInfo } name='remove-circle' color={ Colors.grey.light }/>
              <Text style={ Styles.iconInfoText }>Tidak Terverifikasi</Text>

            </View>
          </View>

          <View style={ Styles.ktpContainer }>

            <Text style={ Styles.title }>Foto KTP</Text>
            <Image style={ Styles.ktpImage } source={ { uri: 'https://printyuk.com/img/cms/LP-ID%20Card%20PVC/ID%20Card%20PVC%202.jpg' } } />

          </View>

          { renderIdentityActions() }

        </>
      );
    }
  };

  const renderCriminalRecordContent = () => {
    if (selectedTab === 1 && verified) {
      return (
        <>
          <Card style={ Styles.criminalRecordCard }>

            <View style={ Styles.criminalRecordTitleContainer }>

              <Image style={ Styles.criminalRecordImage } source={ Images.logo_polri } />
              <Text style={ Styles.criminalRecordTitle }>Catatan Kepolisian RI</Text>

            </View>

            <Text style={ Styles.criminalRecordText }>Pekerja ini masuk dalam catatan kepolisian</Text>

          </Card>

          <Card style={ Styles.criminalRecordDisclaimerCard }>

            <Icon style={ Styles.iconInfo } name='information-circle-outline' color={ Colors.red.danger }/>
            <View style={ Styles.criminalRecordDisclaimerContainer }>

              <Text style={ [Styles.criminalRecordDisclaimerText, { marginBottom: 15 }] }>Seruma <Text style={ Styles.bold }>tidak dapat menampilkan kasus yang terkait</Text> dengan catatan kepolisian.</Text>
              <Text style={ [Styles.criminalRecordDisclaimerText, { marginBottom: 10 }] }>Perlu diketahui memiliki catatan kepolisian:</Text>

              <View style={ [Styles.row, { marginBottom: 5 }] }>
                <Text style={ [Styles.criminalRecordDisclaimerText, { width: 15 }] }>1.</Text>
                <View>
                  <Text style={ Styles.criminalRecordDisclaimerText }><Text style={ Styles.bold }>Bukan berarti</Text> pekerja melakukan tindakan kriminal berat (membunuh, dll.)</Text>
                </View>
              </View>
              <View style={ Styles.row }>
                <Text style={ [Styles.criminalRecordDisclaimerText, { width: 15 }] }>2.</Text>
                <View>
                  <Text style={ Styles.criminalRecordDisclaimerText }><Text style={ Styles.bold }>Bukan berarti</Text> pekerja merupakan orang jahat</Text>
                </View>
              </View>

              <Text style={ [Styles.criminalRecordDisclaimerText, { marginTop: 15 }] }>Keputusan untuk mempekerjakan pekerja berada sepenuhnya di tangan pemilik rumah, dan <Text style={ Styles.bold }>bukan merupakan tanggung jawab Seruma.</Text></Text>

            </View>

          </Card>

          <Link
            text='Simpan Data Pekerja'
          />
        </>
      );
     } else if (selectedTab === 1 && !verified) {
        return (
          <View style={ Styles.unverifiedContainer }>

            <Image style={ Styles.unverifiedIluImage } source={ Images.ilu_criminal_record } />
            <Text style={ Styles.unverifiedTitle }>Catatan Kepolisian</Text>
            <Text style={ Styles.unverifiedText }>Lakukan verifikasi untuk melihat catatan kepolisian pekerja.</Text>

          </View>
        );
      }
  };

  const renderReviews = () => (
    reviews.map(review => (
      <Card key={ review.id } style={ Styles.reviewCard }>

        <View style={ Styles.row }>

          <Image style={ Styles.reviewerImage } source={ review.imageUri } />

          <View style={ Styles.reviewerBioContainer }>

            <Text style={ Styles.reviewerBioTitle }>{ review.name }</Text>
            <Text style={ Styles.reviewerBioText }>{ review.month / 12 } thn bekerja sama</Text>

          </View>

          <View style={ Styles.reviewerStarContainer }>

            <Text style={ Styles.reviewerStarText }><Icon name='star' size={ 22 }/> { review.star }<Text style={ Styles.reviewerStarDivider }> / 5</Text></Text>

          </View>

        </View>
        
        <Text style={ Styles.reviewText }>{ review.review }</Text>

      </Card>
    ))
  );

  const renderReviewContent = () => {
    if (selectedTab === 2) {
      if (verified) {
        return (
          <>
            <View style={ Styles.reviewsContainer }>

              { renderReviews() }

            </View>

            <Link
              text='Putus hubungan kerja'
              color={ Colors.red.danger }
              onPress={ () => { setOpenModalTerminate(true); } }
            />
          </>
        );
      } else {
        return (
          <View style={ Styles.unverifiedContainer }>

            <Image style={ Styles.unverifiedIluImage } source={ Images.ilu_review } />
            <Text style={ Styles.unverifiedTitle }>Terdapat 3 Review</Text>
            <Text style={ Styles.unverifiedText }>Review akan terbuka apabila Anda sudah melakukan verifikasi</Text>

          </View>
        );
      }
    }
  };

  const onPressVerify = () => {
    setVerified(true);
    Actions.payment();
  };

  const renderBottomCard = () => {
    if (!verified) {
      return (
        <BottomCard
          title={ ['Harga', 'IDR 20,000'] }
          type={ 'money' }
          btnText={ 'Verifikasi Sekarang' }
          onPressBtn={ onPressVerify }
        />
      );
    }
  };

  return (
    <>
      <Container
        noPaddingTop
        bg='custom'
        header={
          {
            leftButton: 'ios-arrow-back',
            leftButtonColor: Colors.white.default,
            onPressLeft: () => Actions.pop(),
            rightText: verified ? 'Lihat Gaji' : 'Pekerjakan',
            onPressRight: () => { verified ? Actions.salary() : setOpenModalVerify(true); },
          }
        }
      >
        
        <View style={ Styles.profileContainer } >

          <View style={ Styles.header } />

          <Image style={ Styles.profileImage } source={ { uri: 'https://randomuser.me/api/portraits/women/25.jpg' } } />
          <Image style={ Styles.profileImageBadge } source={ Images.ic_female } />

          <Text style={ Styles.profileText }>Baby Sitter</Text>
          <Text style={ Styles.profileTitle }>Ratna Pratiwi Sutrisno, 24</Text>

          { renderTabs() }

        </View>

        <ScrollView
          style={ Styles.scrollView }
          bounces={ false }
          showsVerticalScrollIndicator={ false }
        >

          <View style={ [Styles.contentContainer, !verified && { marginBottom: 60 }] }>

            { renderIdentityContent() }

            { renderCriminalRecordContent() }

            { renderReviewContent() }

          </View>

        </ScrollView>

      </Container>

      { renderBottomCard() }

      { renderModalVerify() }
      { renderModalTerminate() }

    </>
  );
};

export default Result;
