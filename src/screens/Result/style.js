import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    flex: 1,
  },

  profileContainer: {
    alignItems: 'center',
  },

  diamondLeft: GlobalStyles.diamond(0 - RatioHelper.screenHeight / 3, 0 + RatioHelper.screenWidth / 2),
  diamondCenter: GlobalStyles.diamond(0 - RatioHelper.screenHeight / 3, 0),
  diamondRight: GlobalStyles.diamond(0 - RatioHelper.screenHeight / 3, 0 - RatioHelper.screenWidth / 2),

  profileImage: {
    height: 80,
    width: 80,
    borderRadius: 80,
    marginBottom: 20,
  },

  profileImageBadge: {
    height: 40,
    width: 40,
    position: 'absolute',
    left: RatioHelper.screenWidth / 2 - 10,
    top: 50,
  },

  profileText: [
    GlobalStyles.textRegular(undefined, Colors.white.default),
    {
      marginBottom: 10,
    },
  ],

  profileTitle: [
    GlobalStyles.textBold(undefined, Colors.white.default),
    {
      marginBottom: 10,
    },
  ],

  tabsContainer: {
    flexDirection: 'row',
  },

  tabContainer: (active = false) => {
    return {
      height: 45,
      width: RatioHelper.screenWidth / 3,
      justifyContent: 'center',
      alignItems: 'center',
      borderBottomWidth: active ? 3 : null,
      borderColor: active ? Colors.yellow.main : null,
    };
  },

  tabText: (active = false) => {
    return GlobalStyles.textBold(RatioHelper.normalize(14), active ? Colors.yellow.main : Colors.white.opacityHalf);
  },

  scrollView: {
    marginHorizontal: -20,
  },

  contentContainer: {
    paddingVertical: 20,
    alignItems: 'center',
  },

  unverifiedContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: RatioHelper.screenHeight - 315 - 175,
  },

  unverifiedIluImage: {
    height: RatioHelper.screenWidth / 3,
    width: RatioHelper.screenWidth / 3,
    marginBottom: 20,
  },

  unverifiedTitle: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 10,
    },
  ],

  unverifiedText: [
    GlobalStyles.textRegular(),
    {
      width: RatioHelper.screenWidth - 100,
      textAlign: 'center',
    },
  ],

  formRow: {
    width: RatioHelper.screenWidth - 40,
    flexDirection: 'row',
    marginBottom: 20,
    justifyContent: 'space-between',
  },

  title: GlobalStyles.textBold(RatioHelper.normalize(22)),

  field: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
    {
      flexGrow: 1,
    },
  ],
  
  value: [
    GlobalStyles.textBold(RatioHelper.normalize(14)),
    {
      width: 200,
      textAlign: 'right',
    },
  ],

  icon: {
    fontSize: 18,
    marginLeft: 10,
  },

  input: {
    height: 28,
    fontSize: RatioHelper.normalize(14),
    textAlign: 'right',
    width: 200,
    marginBottom: -23,
    paddingRight: 10,
    paddingLeft: 10,
    paddingTop: 4,
  },

  iconInfosContainer: {
    backgroundColor: Colors.grey.lightest,
    width: RatioHelper.screenWidth - 40,
    borderRadius: 10,
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },

  iconInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  iconInfo: {
    fontSize: 22,
    marginRight: 5,
  },

  iconInfoText: GlobalStyles.textRegular(RatioHelper.normalize(10)),

  ktpContainer: {
    width: RatioHelper.screenWidth - 40,
    textAlign: 'left',
    marginBottom: 10,
  },

  ktpImage: {
    width: RatioHelper.screenWidth - 40,
    height: 200,
    marginVertical: 20,
    borderRadius: 20,
  },

  identityButton: {
    marginBottom: 20,
  },

  identityLink: {
    marginBottom: 30,
  },

  criminalRecordCard: {
    width: RatioHelper.screenWidth - 40,
    flexDirection: 'column',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },

  criminalRecordTitleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },

  criminalRecordImage: {
    height: 40,
    width: 40,
    marginRight: 10,
  },

  criminalRecordTitle: GlobalStyles.textBold(RatioHelper.normalize(22)),

  criminalRecordText: GlobalStyles.textRegular(),

  criminalRecordDisclaimerCard: {
    backgroundColor: Colors.red.light,
    width: RatioHelper.screenWidth - 40,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    marginBottom: 30,
  },

  criminalRecordDisclaimerContainer: {
    marginLeft: 5,
    width: RatioHelper.screenWidth - 100,
  },

  criminalRecordDisclaimerText: {
    color: Colors.red.danger,
  },

  bold: {
    fontWeight: 'bold',
  },

  row: {
    flexDirection: 'row',
  },

  reviewCard: {
    width: RatioHelper.screenWidth - 40,
    flexDirection: 'column',
    marginBottom: 20,
  },

  reviewsContainer: {
    marginBottom: 30,
  },

  reviewerImage: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginRight: 10,
  },

  reviewerBioContainer: {
    width: RatioHelper.screenWidth - 80 - 60 - 70,
  },

  reviewerBioTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(16)),
    {
      marginBottom: 5,
    },
  ],

  reviewerBioText: GlobalStyles.textRegular(undefined, Colors.grey.default),

  reviewerStarContainer: {
    width: 70,
  },

  reviewerStarText: [
    GlobalStyles.textBold(RatioHelper.normalize(22), Colors.blue.main),
    {
      textAlign: 'right',
    },
  ],

  reviewerStarDivider: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
    {
      fontWeight: 'normal',
    },
  ],

  reviewText: [
    GlobalStyles.textRegular(),
    {
      marginTop: 20,
    },
  ],
};
