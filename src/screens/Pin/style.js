import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    paddingTop: RatioHelper.screenHeight / 6,
  },

  title: [
    GlobalStyles.textBold(),
    {
      color: Colors.white.default,
      textAlign: 'center',
      marginBottom: 30,
    },
  ],

  textContainer: {
    flex: -1,
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth: 1,
  },

  text: [
    GlobalStyles.textRegular(),
    {
      color: Colors.grey.preset1,
      lineHeight: 24,
      textAlign: 'center',
    },
  ],

  pinContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 30,
    // borderWidth: 1,
  },

  textNomor: [
    GlobalStyles.textRegular(RatioHelper.normalize(24)),
    {
      textAlign: 'center',
    },
  ],

  linkContainer: {
    flex: -1,
  },

  link: [
    GlobalStyles.textBold(RatioHelper.normalize(14), Colors.green.preset1, 'center'),
    {
      color: '#FBFE27',
    },
  ],

  inputContainer: {
    flex: -1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    borderRadius: 5,
  },

  input: [
    GlobalStyles.textBold(),
    {
      fontSize: 50,
      width: 50,
      textAlign: 'center',
      color: Colors.white.default,
      // borderWidth: 1,
    },
  ],
};
