import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Keyboard,
  ScrollView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import {
  Container,
  // Button,
  // Loader,
} from 'components';
import {
  HooksHelper,
  // MiscHelper,
  RatioHelper,
} from 'helpers';
import {
  Colors,
} from 'consts';
// import {
//   actions,
// } from 'store';

import Styles from './style';

const Pin = props => {
  // const userIdProps = useSelector(HooksHelper.useImmutableSelector(state => state.user.get('id')));
  // const emailProps = useSelector(HooksHelper.useImmutableSelector(state => state.user.getIn(['user', 'email'])));
  // const isLoadingProps = useSelector(HooksHelper.useImmutableSelector(state => state.user.get('isLoading')));
  // const errorMessageProps = useSelector(HooksHelper.useImmutableSelector(state => state.error.get('content_message')));

  // const requestOtpProps = HooksHelper.useCustomDispatch(actions.UserAction.requestOtp);
  // const validateOtpProps = HooksHelper.useCustomDispatch(actions.UserAction.validateOtp);

  const [values, handleChange] = HooksHelper.useForm({
    firstDigit: '',
    secondDigit: '',
    thirdDigit: '',
    fourthDigit: '',
    fifthDigit: '',
    sixthDigit: '',
   });
  const [disabled, setDisabled] = useState(true);

  const inputFirstRef = React.createRef();
  const inputSecondRef = React.createRef();
  const inputThirdRef = React.createRef();
  const inputFourthRef = React.createRef();
  const inputFifthRef = React.createRef();
  const inputSixthRef = React.createRef();

  // useEffect(() => {
  //   if (errorMessageProps) {
  //     setDisabled(true);
  //   }
  // }, [errorMessageProps]);

  // const handleEmpty = () => {
  //   return values.firstDigit === '' || values.secondDigit === '' || values.thirdDigit === '' ||
  //   values.fourthDigit === '' || values.fifthDigit === '' || values.sixthDigit === '';
  // };

  const onKeyPress = (e, refBack = null, refNext = null) => {
    if (e.nativeEvent.key === 'Backspace') {
      refBack ? refBack.current.focus() : null;
    } else {
      refNext ? refNext.current.focus() : onPressNext();
    }
  };

  const onPressNext = () => {
    // Keyboard.dismiss();

    // setDisabled(false);

    // validateOtpProps({
    //   id: userIdProps,
    //   otp: values.firstDigit + values.secondDigit + values.thirdDigit + values.fourthDigit,
    // });
    props.onPressNext();
  };

  const onPressRedo = () => {
    Keyboard.dismiss();

    setDisabled(false);

    setTimeout(() => {
      setDisabled(true);
    }, 3000);

    // requestOtpProps({
    //   username: emailProps,
    // }, true);
  };

  // const renderLoader = () => {
  //   if (isLoadingProps) {
  //     return <Loader/>;
  //   }

  //   return null;
  // };

  return (
    <Container
      style={ Styles.container }
      bg='full'
    >

      <ScrollView
        contentContainerStyle={ { paddingBottom: 30 } }
        bounces={ false }
        showsVerticalScrollIndicator={ false }
      >

        <Text style={ Styles.title }>{ props.title }</Text>

        <View style={ Styles.pinContainer }>
          <View style={ Styles.inputContainer }>
            <TextInput
              ref={ inputFirstRef }
              style={ Styles.input }
              placeholder={ '\u2022' }
              placeholderTextColor={ Colors.white.default }
              keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
              onChangeText={ e => handleChange(e, 'firstDigit') }
              value={ values.firstDigit }
              maxLength={ 1 }
              autoFocus={ true }
              caretHidden={ true }
              underlineColorAndroid={ 'transparent' }
              onKeyPress={ e => onKeyPress(e, null, inputSecondRef) }
            />
            <TextInput
              ref={ inputSecondRef }
              style={ Styles.input }
              placeholder={ '\u2022' }
              placeholderTextColor={ Colors.white.default }
              keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
              onChangeText={ e => handleChange(e, 'secondDigit') }
              value={ values.secondDigit }
              maxLength={ 1 }
              caretHidden={ true }
              underlineColorAndroid={ 'transparent' }
              onKeyPress={ e => onKeyPress(e, inputFirstRef, inputThirdRef) }
            />
            <TextInput
              ref={ inputThirdRef }
              style={ Styles.input }
              placeholder={ '\u2022' }
              placeholderTextColor={ Colors.white.default }
              keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
              onChangeText={ e => handleChange(e, 'thirdDigit') }
              value={ values.thirdDigit }
              maxLength={ 1 }
              caretHidden={ true }
              underlineColorAndroid={ 'transparent' }
              onKeyPress={ e => onKeyPress(e, inputSecondRef, inputFourthRef) }
            />
            <TextInput
              ref={ inputFourthRef }
              style={ Styles.input }
              placeholder={ '\u2022' }
              placeholderTextColor={ Colors.white.default }
              keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
              onChangeText={ e => handleChange(e, 'fourthDigit') }
              value={ values.fourthDigit }
              maxLength={ 1 }
              caretHidden={ true }
              underlineColorAndroid={ 'transparent' }
              onKeyPress={ e => onKeyPress(e, inputThirdRef, inputFifthRef) }
            />
            <TextInput
              ref={ inputFifthRef }
              style={ Styles.input }
              placeholder={ '\u2022' }
              placeholderTextColor={ Colors.white.default }
              keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
              onChangeText={ e => handleChange(e, 'fifthDigit') }
              value={ values.fifthDigit }
              maxLength={ 1 }
              caretHidden={ true }
              underlineColorAndroid={ 'transparent' }
              onKeyPress={ e => onKeyPress(e, inputFourthRef, inputSixthRef) }
            />
            <TextInput
              ref={ inputSixthRef }
              style={ Styles.input }
              placeholder={ '\u2022' }
              placeholderTextColor={ Colors.white.default }
              keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
              onChangeText={ e => handleChange(e, 'sixthDigit') }
              value={ values.sixthDigit }
              maxLength={ 1 }
              caretHidden={ true }
              underlineColorAndroid={ 'transparent' }
              onKeyPress={ e => onKeyPress(e, inputFifthRef, null) }
            />
          </View>
        </View>

        {/* <Button
          text={ 'Verifikasi' }
          onPress={ onPressNext }
          disabled={ handleEmpty() || !disabled }
        /> */}

        <TouchableOpacity
          style={ Styles.linkContainer }
          onPress={ onPressRedo }
          activeOpacity={ 0.5 }
          disabled={ !disabled }
        >
          <Text style={ Styles.link }>{ props.link }</Text>
        </TouchableOpacity>

      </ScrollView>

    </Container>
  );
};

export default Pin;
