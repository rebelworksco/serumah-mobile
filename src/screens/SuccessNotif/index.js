import React from 'react';
import {
  View,
  Text,
  Image,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Images } from 'consts';
import { RatioHelper } from 'helpers';
import { Button, Container } from 'components';
import Styles from './style';

const SuccessNotif = props => {
  const { title, desc, onPress, btnText } = props;

  return (
    <View style={ { flex: 1 } }>
      <Container style={ Styles.container }>
        <Image style={ Styles.img } source={ Images.ilu_success } />
        <Text style={ Styles.title }>
          { title }
        </Text>
        <Text style={ Styles.desc }>
          { desc }
        </Text>
        <Button text={ btnText } onPress={ onPress } width={ RatioHelper.screenWidth / 2 } />
      </Container>
    </View>
  );
};

export default SuccessNotif;
