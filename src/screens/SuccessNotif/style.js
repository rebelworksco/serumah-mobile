import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';
export default {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
 
  img: {
    width: RatioHelper.normalize(120),
    height: RatioHelper.normalize(120),
    margin: RatioHelper.normalize(30),
  },

  title: {
    ...GlobalStyles.textSemiBold(20, Colors.blue.main),
    margin: RatioHelper.normalize(8),
  },
  desc: {
    textAlign: 'center',
    marginTop: RatioHelper.normalize(5),
    marginBottom: RatioHelper.normalize(30),
    color: Colors.blue.navy,
  },
};
