import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
  },

  title: {
    ...GlobalStyles.textSemiBold(24, Colors.blue.main),
  },

  desc: {
    color: Colors.grey.med,
    marginTop: RatioHelper.normalize(10),
    marginBottom: RatioHelper.normalize(15),
  },

  input: {
    marginBottom: RatioHelper.normalize(10),
  },

  innerUpload: {
    alignSelf: 'center',
    backgroundColor: Colors.blue.mainTransparent,
    alignItems: 'center',
    borderRadius: 100,
    width: RatioHelper.normalize(110),
    height: RatioHelper.normalize(110),
    flex: 1,
    justifyContent: 'center',
    marginBottom: RatioHelper.normalize(30),
    marginTop: RatioHelper.normalize(20),
  },

  uploadText: {
    ...GlobalStyles.textSemiBold(12, Colors.blue.main),
  },

  photoIdenText: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.navy),
  },

  uploadIden: {
    flex: 1,
    borderRadius: 8,
    borderStyle: 'dashed',
    borderColor: Colors.grey.text,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: RatioHelper.normalize(20),
    marginTop: RatioHelper.normalize(20),
    height: RatioHelper.normalize(200),
  },

  uploadIdenText: {
    ...GlobalStyles.textSemiBold(14, Colors.grey.text),
  },

  circlePic: {
    width: RatioHelper.normalize(110),
    height: RatioHelper.normalize(110),
    borderRadius: 100,
  },

  idenPic: {
    height: RatioHelper.normalize(198),
    width: '100%',
    borderRadius: 8,
    marginBottom: RatioHelper.normalize(20),
    marginTop: RatioHelper.normalize(20),
    padding: 5,
  },
};
