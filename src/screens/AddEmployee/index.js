import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { Colors, Images, GlobalStyles } from 'consts';
import { HooksHelper } from 'helpers';
import { Button, Container, Input } from 'components';
import Styles from './style';

const AddEmployee = props => {
  const [values, handleChange] = HooksHelper.useForm({
    name: '',
    nickname: '',
    phone: '',
    photo: '',
    identityPhoto: '',
  });

  const imgPickerOptions = {
    title: 'Foto Pekerja',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  
  const onPressContinue = () => {
    console.log('continue');
    Actions.result();
  };

  const onPickImg = option => {
    ImagePicker.showImagePicker(imgPickerOptions, response => {
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: 'data:image/jpeg;base64,' + response.data };
    
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        console.log('source', source);
        handleChange(source, option);
      }
    });
  };

  const renderImg = () => {
    if (values.photo) {
      return;
    }
    return;
  };

  return (
    <View style={ Styles.container }>
      <Container style={ Styles.container }
        header={ { leftButton: 'ios-arrow-back',
        leftButtonColor: Colors.grey.med,
        rightImg: Images.ic_support,
        rightButtonContainerStyle: { justifyContent: 'center', paddingBottom: 7 },
        onPressLeft: () => Actions.pop(),
        rightText2: 'Bantuan',
        rightTextStyle2: GlobalStyles.textSemiBold(16, Colors.blue.main),
        } }>

        <ScrollView showsVerticalScrollIndicator={ false }>
          
          <Text style={ Styles.title }> Tambah pekerja</Text>
          <Text style={ Styles.desc }> Masukkan secara manual informasi pekerja Anda. </Text>

          <View style={ { flex: 1 } } >
            <TouchableOpacity onPress={ () => onPickImg('photo') } style={ Styles.innerUpload }>
              {
                values.photo && values.photo.uri !== '' ? <Image style={ Styles.circlePic } source={ values.photo } /> : <>
                  <Ionicons size={ 40 } name={ 'md-camera' } color={ Colors.blue.main } />
                  <Text style={ Styles.uploadText } > Foto Pekerja </Text>
                </>
              }
            
            </TouchableOpacity>
          </View>

          <Input
            value={ values.name }
            placeholder={ 'Nama Pekerja' }
            onChangeText={ onChangeText => handleChange(onChangeText, 'name') }
            style={ Styles.input }
          />

          <Input
            value={ values.name }
            placeholder={ 'Nama Panggilan' }
            onChangeText={ onChangeText => handleChange(onChangeText, 'nickname') }
            style={ Styles.input }
          />

          <Input
            value={ values.name }
            placeholder={ 'Nomor Telepon' }
            onChangeText={ onChangeText => handleChange(onChangeText, 'phone') }
            style={ Styles.input }
            keyboardType={ 'phone-pad' }
          />

          <Text style={ Styles.photoIdenText }>Foto KTP  <Ionicons size={ 25 } color={ Colors.blue.main } name='md-information-circle' /></Text>
          
          <TouchableOpacity onPress={ () => onPickImg('identityPhoto') } style={ Styles.uploadIden }>
            {
              values.identityPhoto && values.identityPhoto.uri !== '' ? <Image  style={ Styles.idenPic } source={ values.identityPhoto } /> : <>
                <Ionicons size={ 50 } name={ 'md-camera' } color={ Colors.grey.text } />
                <Text style={ Styles.uploadIdenText } > Foto KTP </Text>
              </>
            }
          </TouchableOpacity>

          <Button text={ 'Lanjut' } onPress={ onPressContinue }/>
        </ScrollView>

      </Container>
    </View>
  );
};

export default AddEmployee;
