import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    flex: 1,
  },

  title: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 10,
    },
  ],

  text: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
    {
      marginBottom: 30,
    },
  ],

  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 40,
  },

  input: (isFocused = false) => {
    return {
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1.5,
      borderRadius: 10,
      borderColor: isFocused ? Colors.blue.main : Colors.black.opacityLow,
      backgroundColor: Colors.white.default,
      height: (RatioHelper.screenWidth - 40) / 4 - (RatioHelper.screenWidth - 40) / 16,
      width: (RatioHelper.screenWidth - 40) / 4 - (RatioHelper.screenWidth - 40) / 16,
      textAlign: 'center',
      fontSize: 30,
      fontWeight: 'bold',
    };
  },

  infoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  infoText: GlobalStyles.textRegular(),

  timer: GlobalStyles.textBold(RatioHelper.normalize(14), Colors.blue.main),

  button: {
    width: 130,
  },
  
};
