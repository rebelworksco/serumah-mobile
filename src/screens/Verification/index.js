import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import {
  Container,
  Button,
} from 'components';
import {
  HooksHelper,
  RatioHelper,
} from 'helpers';
// import {
//   actions,
// } from 'store';

import Styles from './style';

const Verification = props => {
  // const userIdProps = useSelector(HooksHelper.useImmutableSelector(state => state.user.get('id')));
  // const emailProps = useSelector(HooksHelper.useImmutableSelector(state => state.user.getIn(['user', 'email'])));
  // const isLoadingProps = useSelector(HooksHelper.useImmutableSelector(state => state.user.get('isLoading')));
  // const errorMessageProps = useSelector(HooksHelper.useImmutableSelector(state => state.error.get('content_message')));

  // const requestOtpProps = HooksHelper.useCustomDispatch(actions.UserAction.requestOtp);
  // const validateOtpProps = HooksHelper.useCustomDispatch(actions.UserAction.validateOtp);

  const [values, handleChange] = HooksHelper.useForm({
    firstDigit: '',
    secondDigit: '',
    thirdDigit: '',
    fourthDigit: '',
   });

  const inputFirstRef = React.createRef();
  const inputSecondRef = React.createRef();
  const inputThirdRef = React.createRef();
  const inputFourthRef = React.createRef();

  const [focusedDigit, setFocusedDigit] = useState(1);

  // useEffect(() => {
  //   if (errorMessageProps) {
  //     setDisabled(true);
  //   }
  // }, [errorMessageProps]);

  // const handleEmpty = () => {
  //   return values.firstDigit === '' || values.secondDigit === '' || values.thirdDigit === '' ||
  //   values.fourthDigit === '' || values.fifthDigit === '' || values.sixthDigit === '';
  // };

  const onKeyPress = (e, refBack = null, refNext = null) => {
    if (e.nativeEvent.key === 'Backspace') {
      if (refBack === inputFirstRef) {
        values.firstDigit = '';
      } else if (refBack === inputSecondRef) {
        values.secondDigit = '';
      } else if (refBack === inputThirdRef) {
        values.thirdDigit = '';
      }
      refBack ? refBack.current.focus() : null;
    } else {
      refNext ? refNext.current.focus() : onPressNext();
    }
  };

  const onPressNext = () => {
    // Keyboard.dismiss();

    // setDisabled(false);

    // validateOtpProps({
    //   id: userIdProps,
    //   otp: values.firstDigit + values.secondDigit + values.thirdDigit + values.fourthDigit,
    // });
    Actions.pin({
      title: 'Buat PIN Baru',
      onPressNext: () => {
        Actions.pin({
          title: 'Konfirmasi PIN Baru',
          onPressNext: () => {
            Actions.worker();
          },
        });
      },
    });
  };

  // const onPressRedo = () => {
  //   Keyboard.dismiss();

  //   setDisabled(false);

  //   setTimeout(() => {
  //     setDisabled(true);
  //   }, 3000);

  //   requestOtpProps({
  //     username: emailProps,
  //   }, true);
  // };

  // const renderLoader = () => {
  // if (isLoadingProps) {
  //   return <Loader/>;
  // }
  //   return null;
  // };

  return (
    <View style={ { flex: 1 } }>
      <Container
        style={ Styles.container }
        header={ {
          onPressLeft: () => Actions.pop(),
          leftButton: 'ios-arrow-round-back',
        } }
      >

      <Text style={ Styles.title }>Verifikasi Nomor Telepon</Text>
      <Text style={ Styles.text }>Masukkan 4 digit kode verifikasi yang barusan kami kirimkan via sms ke nomor Anda.</Text>

        <View style={ Styles.inputContainer }>
          <TextInput
            ref={ inputFirstRef }
            style={ Styles.input(focusedDigit === 1) }
            keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
            onChangeText={ e => handleChange(e, 'firstDigit') }
            value={ values.firstDigit }
            maxLength={ 1 }
            autoFocus={ true }
            caretHidden={ true }
            underlineColorAndroid={ 'transparent' }
            onFocus={ () => { setFocusedDigit(1); } }
            onKeyPress={ e => onKeyPress(e, null, inputSecondRef) }
          />
          <TextInput
            ref={ inputSecondRef }
            style={ Styles.input(focusedDigit === 2) }
            keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
            onChangeText={ e => handleChange(e, 'secondDigit') }
            value={ values.secondDigit }
            maxLength={ 1 }
            caretHidden={ true }
            underlineColorAndroid={ 'transparent' }
            onFocus={ () => { setFocusedDigit(2); } }
            onKeyPress={ e => onKeyPress(e, inputFirstRef, inputThirdRef) }
          />
          <TextInput
            ref={ inputThirdRef }
            style={ Styles.input(focusedDigit === 3) }
            keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
            onChangeText={ e => handleChange(e, 'thirdDigit') }
            value={ values.thirdDigit }
            maxLength={ 1 }
            caretHidden={ true }
            underlineColorAndroid={ 'transparent' }
            onFocus={ () => { setFocusedDigit(3); } }
            onKeyPress={ e => onKeyPress(e, inputSecondRef, inputFourthRef) }
          />
          <TextInput
            ref={ inputFourthRef }
            style={ Styles.input(focusedDigit === 4) }
            keyboardType={ RatioHelper.platformScale('phone-pad', 'default') }
            onChangeText={ e => handleChange(e, 'fourthDigit') }
            value={ values.fourthDigit }
            maxLength={ 1 }
            caretHidden={ true }
            underlineColorAndroid={ 'transparent' }
            onFocus={ () => { setFocusedDigit(4); } }
            onKeyPress={ e => onKeyPress(e, inputThirdRef, null) }
          />
        </View>

        <View style={ Styles.infoContainer }>

          <Text style={ Styles.infoText }>Tidak Menerima Kode?</Text>
          {/* <Text style={ Styles.timer }>00:30</Text> */}
          <Button
            text='Kirim Ulang'
            style={ Styles.button }
          />

        </View>

      </Container>

    </View>
  );
};

export default Verification;
