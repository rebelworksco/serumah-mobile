import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
  },

  title: {
    ...GlobalStyles.textSemiBold(24, Colors.blue.main),
  },

  desc: {
    color: Colors.grey.med,
    marginTop: RatioHelper.normalize(10),
    marginBottom: RatioHelper.normalize(15),
  },

  input: {
    marginBottom: RatioHelper.normalize(10),
  },

  worker: {
    flexDirection: 'row',
    marginTop: RatioHelper.normalize(15),
    marginBottom: RatioHelper.normalize(15),
    paddingTop: RatioHelper.normalize(15),
    paddingBottom: RatioHelper.normalize(15),
    borderBottomWidth: 1,
    borderBottomColor: Colors.grey.borderLight,
  },

  imgContainer: {
    borderRadius: 100,
    backgroundColor: Colors.blue.mainTransparent,
    width: RatioHelper.normalize(56),
    height: RatioHelper.normalize(56),
    justifyContent: 'center',
    marginRight: RatioHelper.normalize(20),
  },

  icon: {
    textAlign: 'center',
  },

  infoContainer: {
    flex: 2,
  },

  selectWorkerBtn: {
    float: 'right',
    padding: RatioHelper.normalize(5),
  },

  formContainer: {
    flex: 1,
  },

  sectionTitle: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.navy),
    marginBottom: RatioHelper.normalize(20),
  },

  textArea: {
    marginBottom: RatioHelper.normalize(10),
    height: RatioHelper.normalize(135),
  },

  info: filled => {
    return {
      color: filled ? Colors.grey.med : Colors.grey.text,
      fontSize: 12,
    };
  },
  
  name: filled => {
    return {
      ...GlobalStyles.textSemiBold(14, filled ? Colors.blue.navy : Colors.grey.text),
      marginTop: RatioHelper.normalize(5),
      marginBottom: RatioHelper.normalize(5),
    };
  },

  workerCards: index => {
    return {
      flexDirection: 'row',
      borderTopWidth: index === 0 ? 0 : 1,
      borderTopColor: Colors.grey.borderLight,
      paddingBottom: RatioHelper.normalize(10),
      paddingTop: RatioHelper.normalize(10),
    };
  },

  bottomCard: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: RatioHelper.normalize(25),
  },

  changeWorker: {
    ...GlobalStyles.textSemiBold(14, Colors.blue.main, 'center'),
  },

  changeWorkerContainer: {
    justifyContent: 'center',
  },

  selectedIconContainer: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'flex-end',
  },

  scrollView: {
    width: RatioHelper.screenWidth - 50,
  },
};
