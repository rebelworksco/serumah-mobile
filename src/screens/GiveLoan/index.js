import React, { useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { Colors, Images } from 'consts';
import { HooksHelper } from 'helpers';
import { Button, Container, Input, BottomCard, Icon } from 'components';
import Styles from './style';
import { ScrollView } from 'react-native-gesture-handler';

const workerList = [
  {
    img: Images.bg_houses,
    role: 'Baby Sitter',
    status: '2 tahun bekerja',
    name: 'Rita Pratiwi Sutrisno',
  },
  {
    img: Images.bg_houses,
    role: 'Supir Pribadi',
    status: '1 tahun bekerja',
    name: 'Bang mobil',
  },
];

const GiveLoan = props => {
  const [worker, setWorker] = useState({
    img: '',
    role: '',
    status: '',
    name: '',
  });

  const [values, setValues] = HooksHelper.useForm({
    totalLoan: '',
    instalment: '',
    reason: '',
  });

  const [isModalOpened, setIsModalOpened] = useState(false);

  const onPressSelectWorker = () => {
    setIsModalOpened(true);
  };

  const onPressGiveLoan = () => {
    console.log('give loan');
  };

  const renderWorker = () => {
    return (
      <View style={ Styles.worker }>
        <View style={ Styles.imgContainer }>
          {
            worker && worker.img ? <Image source={ worker.img } style={ Styles.imgContainer }/> : <Ionicons style={ Styles.icon } size={ 34 } color={ Colors.blue.main } name={ 'ios-add' } />
          }
        </View>
        <View style={ Styles.infoContainer }>
          <Text style={ Styles.info(worker.role) }>{worker.role ? worker.role : 'Peran'}</Text>
          <Text style={ Styles.name(worker.name) }>{ worker.name ? worker.name : 'Nama Pekerja'}</Text>
          <Text style={ Styles.info(worker.status) }>{worker.status ? worker.status : 'Status Gajian'}</Text>
        </View>
        {
          worker.name ? <TouchableOpacity style={ Styles.changeWorkerContainer } onPress={ onPressSelectWorker }><Text style={ Styles.changeWorker }>Ganti</Text></TouchableOpacity> : <Button style={ Styles.selectWorkerBtn } width={ 118 } text={ 'Pilih Pekerja' } onPress={ onPressSelectWorker }/>
        }
      </View>
    );
  };

  const renderForm = () => {
    return (
      <View style={ Styles.formContainer }>
          <Input
            value={ values.totalLoan }
            placeholder={ 'Total Pinjaman' }
            onChangeText={ onChangeText => setValues(onChangeText, 'totalLoan') }
            style={ Styles.input }
            keyboardType={ 'numeric' }
          />

          <Input
            value={ values.instalment }
            placeholder={ 'Cicilan Per Bulan' }
            onChangeText={ onChangeText => setValues(onChangeText, 'instalment') }
            style={ Styles.input }
            keyboardType={ 'numeric' }
          />

          <Text style={ Styles.sectionTitle }>Alasan Peminjaman</Text>

          <Input
            value={ values.reason }
            onChangeText={ onChangeText => setValues(onChangeText, 'reason') }
            style={ Styles.textArea }
            multiline
            numberOfLines={ 5 }
          />
      </View>
    );
  };

  const onPressSelect = selected => () => {
    setWorker(selected);
    setIsModalOpened(false);
  };

  const renderSelectWorker = list => {
    return list.map((x, i) => {
      return (
        <TouchableOpacity onPress={ onPressSelect(x) } style={ Styles.workerCards(i) } key={ i }>
          <Image source={ x.img } style={ Styles.imgContainer } />
          <View>
            <Text style={ Styles.info(true) }> { x.role }</Text>
            <Text style={ Styles.name(true) }> { x.name }</Text>
          </View>
          {
            ((worker.name && worker.name === x.name) || (!worker.name && i === 0)) && (
              <View style={ Styles.selectedIconContainer }>
                <Icon name={ 'checkmark-circle' } size={ 24 } color={ Colors.blue.main } />
              </View>
            )
          }
        </TouchableOpacity>
      );
    });
  };

  const renderBottomSheet = () => {
    return (
      <BottomCard bg isOpen={ isModalOpened } style={ Styles.bottomCard } type={ 'custom' } height={ 243 }>
        <ScrollView style={ Styles.scrollView } bounces={ false } showsVerticalScrollIndicator={ false }>
          <Text style={ Styles.sectionTitle }>Pilih Pekerja</Text>
          { renderSelectWorker(workerList) }
        </ScrollView>
      </BottomCard>
    );
  };

  return (
    <>
      <Container style={ Styles.container }
        header={ {
          leftButton: 'ios-arrow-back',
          leftButtonColor: Colors.grey.med,
          onPressLeft: () => Actions.pop(),
        } }>

          <Text style={ Styles.title }>Beri Pinjaman</Text>
          <Text style={ Styles.desc }> Atur total pinjaman dan alasan pinjaman </Text>
          
          { renderWorker() }
          { renderForm() }
          <Button text={ 'Beri Pinjaman' } onPress={ onPressGiveLoan }/>
         
      </Container>
      { renderBottomSheet() }
      
    </>
  );
};

export default GiveLoan;
