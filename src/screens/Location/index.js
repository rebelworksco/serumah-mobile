import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Geolocation from '@react-native-community/geolocation';
import MapView, { Marker } from 'react-native-maps';

import { Colors, Images } from 'consts';
import { BottomCard, Container } from 'components';
import Styles from './style';

const latLngDelta = {
  latitudeDelta: 0.02,
  longitudeDelta: 0.03,
};

const initialRegion = {
  latitude: -5.259405,
  longitude: 106.781568,
  ...latLngDelta,
};

const Location = props => {
  let mapRef = useRef(null);

  const [workerList, setWorkerList] = useState([
    {
      id: 1,
      name: 'Rita Pararam',
      address: 'Jl. Sultan Iskandar Muda No.6B, RT.7/RW.9, Kby. Lama Sel., Kec. Kby. Lama, Kebayoran Lama, Daerah Khusus Ibukota Jakarta 12240, Indonesia',
      coordinate: { latitude: -6.2441739, longitude: 106.7813403 },
      icon: Images.bg_houses,
    }, {
      id: 2,
      name: 'MR dunkin hahahah',
      address: 'Jl. Dunkin Iskandar Muda No.19, Kby. Lama Utara, Kec. Kby. Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12240',
      coordinate: { latitude: -6.2455603, longitude: 106.7809863 },
      icon: Images.bg_houses,
    },
  ]);

  const [selected, setSelected] = useState({
    id: 1,
    name: 'Rita Pararam',
    address: 'Jl. Sultan Iskandar Muda No.6B, RT.7/RW.9, Kby. Lama Sel., Kec. Kby. Lama, Kebayoran Lama, Daerah Khusus Ibukota Jakarta 12240, Indonesia',
    coordinate: '-6.2441739,106.7813403',
    icon: Images.bg_houses,
  });

  const [currentLoc, setCurrentLoc] = useState(null);

  const [userLoc, setUserLoc] = useState(null);

  useEffect(() => {
    Geolocation.requestAuthorization();
    Geolocation.getCurrentPosition(
      position => {
        setCurrentLoc({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          ...latLngDelta,
        });
        setUserLoc({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          ...latLngDelta,
        });
      },
      error => {
          reqLocation();
      }
      , {
          timeout: 2000,
          maximumAge: 1000,
          enableHighAccuracy: false,
      });
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  const getCurrentLoc = () => {
    Geolocation.getCurrentPosition(
      position => {
        setCurrentLoc({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          ...latLngDelta,
        });
        setUserLoc({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          ...latLngDelta,
        });
      },
      error => {
          console.log('error', error);
      }
      , {
          timeout: 2000,
          maximumAge: 1000,
          enableHighAccuracy: false,
      });
  };

  const reqLocation = () => {
    Geolocation.requestAuthorization();
    getCurrentLoc();
  };

  const selectWorker = worker => () => {
    setSelected(worker);
    setCurrentLoc({
      ...worker.coordinate,
      ...latLngDelta,
    });
  };

  const renderBottomSheet = () => {
    return (
      <BottomCard isOpen={ true } style={ Styles.bottomCard } type={ 'custom' } height={ 243 }>
        <View style={ Styles.imgContainer }>
          {
            workerList.map(x => {
              return <TouchableOpacity key={ x.id } onPress={ selectWorker(x) } ><Image source={ x.icon } style={ Styles.img(x.id === selected.id) }/></TouchableOpacity>;
            })
          }
        </View>
        <Text style={ Styles.name }> { selected.name } </Text>
        <Text style={ Styles.address }> { selected.address } </Text>
      </BottomCard>
    );
  };

  const renderMarker = () => {
    if (workerList) {
      return workerList.map((x, i) => {
        return (
          <Marker
            key={ i }
            coordinate={ x.coordinate }
            title={ x.title }
            description={ x.description }
          >
            <View style={ Styles.markerLabelCon }>
              <Text style={ Styles.markerLabel }>Lokasi Terakhir Pulang</Text>
            </View>
            <Image source={ x.icon } style={ Styles.imgMarker(x.id === selected.id) }/>
          </Marker>
        );
      });
    }
    return null;
  };

  const recenter = () => {
    mapRef.current.animateToRegion(userLoc, 1000);
  };

  return (
    <>
      <Container style={ Styles.container }
        noPaddingHorizontal
        noPaddingTop
        header={ { leftButton: 'ios-arrow-back',
        leftButtonColor: Colors.grey.med,
        onPressLeft: () => Actions.pop(),
        middleText: 'Lokasi Pulang Pekerja',
        middleTextColor: Colors.blue.navy,
        middleWidth: 200,
        } }>

          <MapView
            ref={ mapRef }
            initialRegion={ initialRegion }
            region={ currentLoc && currentLoc }
            showsUserLocation={ true }
            style={ Styles.mapStyle }
          >
            { renderMarker() }
            <TouchableOpacity style={ Styles.recenterBtn } onPress={ recenter }>
              <Image source={ Images.ic_recenter } style={ Styles.recenter } />
            </TouchableOpacity>
          </MapView>

      </Container>
      { renderBottomSheet() }
    </>
  );
};

export default Location;
