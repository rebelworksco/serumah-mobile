import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
    width: RatioHelper.screenWidth,
    height: RatioHelper.screenHeight,
  },
 
  mapStyle: {
    flex: 1,
   
    // position: 'absolute',
    left: 0,
    padding: 0,

  },

  bottomCard: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: RatioHelper.normalize(25),
  },

  img: selected => {
    return {
      borderRadius: 100,
      borderWidth: 4,
      borderColor: selected ? Colors.blue.main : Colors.white.default,
      backgroundColor: Colors.blue.mainTransparent,
      width: RatioHelper.normalize(53),
      height: RatioHelper.normalize(53),
      justifyContent: 'center',
      marginRight: RatioHelper.normalize(20),
      padding: RatioHelper.normalize(5),
    };
  },

  imgContainer: {
    flexDirection: 'row',
  },

  name: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.navy),
    marginTop: RatioHelper.normalize(15),
  },

  address: {
    color: Colors.blue.navy,
    fontSize: 14,
    marginTop: RatioHelper.normalize(13),

  },

  imgMarker: selected => {
    return {
      borderRadius: 100,
      borderWidth: 4,
      borderColor: selected ? Colors.blue.main : Colors.white.default,
      backgroundColor: Colors.blue.mainTransparent,
      width: RatioHelper.normalize(53),
      height: RatioHelper.normalize(53),
      justifyContent: 'center',
      marginRight: RatioHelper.normalize(20),
      padding: RatioHelper.normalize(5),
    };
  },

  markerLabel: {
    ...GlobalStyles.textSemiBold(12, Colors.white.default),
  },
  
  markerLabelCon: {
    borderRadius: 12,
    marginBottom: RatioHelper.normalize(10),
    backgroundColor: Colors.blue.main,
    padding: RatioHelper.normalize(5),
    left: -50,
  },

  recenter: {
    width: RatioHelper.normalize(30),
    height: RatioHelper.normalize(30),
    position: 'absolute',
    alignSelf: 'center',
  },
  
  recenterBtn: {
    backgroundColor: Colors.white.default,
    borderRadius: 100,
    width: RatioHelper.normalize(60),
    height: RatioHelper.normalize(60),
    justifyContent: 'center',
    alignSelf: 'center',
    alignItem: 'center',
    position: 'absolute',
    bottom: RatioHelper.normalize(200),
    right: 10,
    flex: 1,
  },


};
