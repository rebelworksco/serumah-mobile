import React, { useState } from 'react';
import {
  View,
  ScrollView,
} from 'react-native';
import {
  Actions,
} from 'react-native-router-flux';

import {
  Images,
} from 'consts';

import {
  Text,
  Input,
  Button,
  Container,
  BottomCard,
  Link,
  Row,
} from 'components';

import Styles from './style';

const slideItems = [
  {
    id: 0,
    text: 'Lorem Ipsum 1',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  },
  {
    id: 1,
    text: 'Lorem Ipsum 2',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  },
  {
    id: 2,
    text: 'Lorem Ipsum 3',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  },
];

const Login = () => {
  const [activeSlide, setActiveSlide] = useState(0);
  const [scrollOffset, setScrollOffset] = useState(0);
  const [phone, setPhone] = useState('');
  
  const renderSlideItems = () => {
    return (
      slideItems.map(item => (
        <View key={ item.id } style={ Styles.slideItem }>
          <Text type='title' color='white'>{ item.text }</Text>
          <Text color='white'>{ item.description }</Text>
        </View>
      ))
    );
  };

  const renderSlidePointers = () => {
    return (
      slideItems.map(item => (
        <View key={ item.id } style={ Styles.point(item.id === activeSlide) } />
      ))
    );
  };

  const handleSlideScroll = e => {
    // set which slide is displayed after scroll
    const horizontalOffset = e.nativeEvent.contentOffset.x;
    if (scrollOffset < horizontalOffset) {
      setActiveSlide(activeSlide + 1);
    } else if (scrollOffset > horizontalOffset) {
      setActiveSlide(activeSlide - 1);
    }
    setScrollOffset(horizontalOffset);
  };

  const onPressLogin = () => {
    Actions.pin({
      title: 'PIN Anda',
      link: 'Lupa PIN Anda?',
      onPressNext: () => {
        Actions.tnc();
      },
    });
  };

  const renderBottomCard = () => (
    <BottomCard>

      <View>

        <Text type='title'>Masuk</Text>
        <Text>Lorem ipsum dolor sit amet.</Text>

        <Input
          value={ phone }
          placeholder={ 'Nomor Telepon' }
          onChangeText={ onChangePhone => setPhone(onChangePhone) }
          keyboardType={ 'phone-pad' }
          maxLength={ 20 }
        />

        <Button
          text={ 'Masuk' }
          onPress={ onPressLogin }
          disabled={ !phone }
          style={ Styles.button }
        />

        <Row justify='center'>
          <Text mb={ 0 }>Belum punya akun? </Text>
          <Link
            text='Registrasi'
          />
        </Row>

      </View>
      
    </BottomCard>
  );
 
  return (
    <>
      <Container
        style={ Styles.container }
        bg='full'
        bgImage={ Images.bg_houses }
      >

        <Row>

          <ScrollView
            horizontal
            pagingEnabled
            showsHorizontalScrollIndicator={ false }
            style={ Styles.slide }
            onMomentumScrollEnd={ handleSlideScroll }>

            { renderSlideItems() }
          </ScrollView>

          <View style={ Styles.slidePointer }>

            { renderSlidePointers() }
            
          </View>
        
        </Row>

      </Container>

      { renderBottomCard() }
    </>
  );
};

export default Login;
