import {
  Colors,
} from 'consts';

import {
  RatioHelper,
} from 'helpers';

export default {
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: RatioHelper.screenHeight / 6,
  },

  slide: {
    width: RatioHelper.screenWidth - 100,
  },

  slidePointer: {
    width: 60,
    display: 'flex',
    flexDirection: 'row',
  },

  slideItem: {
    width: RatioHelper.screenWidth - 100,
  },

  point: active => {
    return {
      height: 8,
      width: active ? 20 : 8,
      marginHorizontal: 4,
      borderRadius: 4,
      marginTop: 4,
      backgroundColor: active ? Colors.white.default : Colors.white.opacityHalf,
    };
  },

  button: {
    marginBottom: 20,
  },
};
