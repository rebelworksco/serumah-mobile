import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    flex: 1,
    paddingBottom: 0,
    padding: 0,
  },

  title: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 5,
    },
  ],

  text: [
    GlobalStyles.textRegular(undefined, Colors.grey.default),
    {
      lineHeight: 20,
    },
  ],

  roleCardContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },

  roleCard: {
    width: RatioHelper.screenWidth / 2 - 30,
    height: RatioHelper.screenWidth / 2 - 30,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },

  cardImage: {
    height: RatioHelper.screenWidth / 2 - 100,
    marginBottom: 10,
    resizeMode: 'contain',
  },

  cardText: GlobalStyles.textBold(RatioHelper.normalize(14)),

  infoTitle: [GlobalStyles.textBold(RatioHelper.normalize(16)),
  {
    marginVertical: 20,
  }],

  listContainer: {
    marginBottom: 20,
  },
  
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 20,
  },

  itemImage: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginRight: 10,
  },

  itemInfoContainer: {
    width: RatioHelper.screenWidth - 100,
  },

  itemTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(14), Colors.blue.main),
    {
      marginBottom: 5,
    },
  ],

  itemText: GlobalStyles.textRegular(11, Colors.grey.default),

  button: {
    marginBottom: 20,
  },
};
