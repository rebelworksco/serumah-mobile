import React, { useState } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import { useSelector } from 'react-redux';
import {
  Container,
  Card,
  Button,
} from 'components';
import {
  // RatioHelper,
  // HooksHelper,
} from 'helpers';
import {
  // actions,
} from 'store';
import {
  Images,
} from 'consts';

import Styles from './style';

const ChooseRole = props => {
  const [role, setRole] = useState('employer');

  const employerBenefits = [
    {
      id: 0,
      image: Images.ic_house,
      title: 'Lorem Ipsum Employer',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
    },
    {
      id: 1,
      image: Images.ic_house,
      title: 'Lorem Ipsum',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
    },
    {
      id: 2,
      image: Images.ic_house,
      title: 'Lorem Ipsum',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
    },
  ];

  const employeeBenefits = [
    {
      id: 0,
      image: Images.ic_house,
      title: 'Lorem Ipsum Employee',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
    },
    {
      id: 1,
      image: Images.ic_house,
      title: 'Lorem Ipsum',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
    },
    {
      id: 2,
      image: Images.ic_house,
      title: 'Lorem Ipsum',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
    },
  ];

  const onPressContinue = () => {
    Actions.referralCode();
  };

  const renderBenefits = () => {
    const benefits = role === 'employer' ? employerBenefits : employeeBenefits;
    return (
      benefits.map(item => (
        <View key={ item.id } style={ Styles.itemContainer }>

          <Image style={ Styles.itemImage } source={ item.image } />

          <View style= { Styles.itemInfoContainer }>
            <Text style={ Styles.itemTitle } >{ item.title }</Text>
            <Text style={ Styles.itemText }>{ item.text }</Text>
          </View>
          
        </View>
      ))
    );
  };

  return (
    <ScrollView showsVerticalScrollIndicator={ false } style={ Styles.container }>

      <Container
        style={ Styles.container }
        header={ {
          onPressLeft: () => Actions.pop(),
          leftButton: 'ios-arrow-round-back',
        } }
      >

        <Text style={ Styles.title }>Pilih Peran Anda</Text>
        <Text style={ Styles.text }>Pilih peran Anda untuk memulai aplikasi ini.</Text>
        
        <View style={ Styles.roleCardContainer }>

          <TouchableOpacity onPress={ () => setRole('employer') }>
            <Card style={ [Styles.roleCard, role !== 'employer' && { opacity: 0.2 }] }>
              <Image style={ Styles.cardImage } source={ Images.ilu_employer } />
              <Text style={ Styles.cardText }>Pemilik Rumah</Text>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity onPress={ () => setRole('employee') }>
            <Card style={ [Styles.roleCard, role !== 'employee' && { opacity: 0.2 }] }>
              <Image style={ Styles.cardImage } source={ Images.ilu_employee } />
              <Text style={ Styles.cardText }>Pekerja</Text>
            </Card>
          </TouchableOpacity>

        </View>

        <Text style={ Styles.infoTitle }>Keuntungan Pemilik Rumah</Text>

        <View style={ Styles.listContainer }>
          { renderBenefits() }
        </View>

        <Button
          text='Lanjut'
          onPress={ onPressContinue }
          style={ Styles.button }
          />

      </Container>

    </ScrollView>
  );
};

export default ChooseRole;
