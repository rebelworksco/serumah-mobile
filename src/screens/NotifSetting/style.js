import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
  },

  title: {
    ...GlobalStyles.textSemiBold(24, Colors.blue.main),
  },

  desc: {
    color: Colors.grey.med,
    marginTop: RatioHelper.normalize(10),
    marginBottom: RatioHelper.normalize(15),
  },

  section: {
    flexDirection: 'row',
    paddingBottom: RatioHelper.normalize(15),
    paddingTop: RatioHelper.normalize(15),
    borderBottomWidth: 1,
    borderBottomColor: Colors.grey.borderLight,
  },

  sectionInner: {
    flex: 1,
    flexDirection: 'column',
  },

  sectionTitle: {
    paddingBottom: RatioHelper.normalize(5),
    ...GlobalStyles.textSemiBold(14),
  },

  sectionDesc: {
    paddingBottom: RatioHelper.normalize(10),
    color: Colors.grey.med,
  },
};
