import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import ToggleSwitch from 'toggle-switch-react-native';


import { Colors } from 'consts';
import { HooksHelper } from 'helpers';
import { Container } from 'components';
import Styles from './style';

const NotifSetting = props => {
  const [form, handleForm] = HooksHelper.useForm({
    payday: true,
    attendance: true,
    overtime: true,
  });

  const option = [
    {
      title: 'Gajian',
      desc: 'Notifikasi gajian pekerja Anda.',
      val: form.payday,
      label: 'payday',
    }, {
      title: 'Absensi',
      desc: 'Notifikasi absensi pekerja Anda.',
      val: form.attendance,
      label: 'attendance',
    }, {
      title: 'Lembur',
      desc: 'Notifikasi apabila pekerja Anda lembur.',
      val: form.overtime,
      label: 'overtime',
    },
  ];

  const renderContent = () => {
    return option.map(x => {
      return (
        <View key={ x.title } style={ Styles.section }>
          <View style={ Styles.sectionInner }>
            <Text style={ Styles.sectionTitle }> {x.title}</Text>
            <Text style={ Styles.sectionDesc }> {x.desc} </Text>
          </View>
          <ToggleSwitch
            isOn={ x.val }
            onColor={ Colors.blue.main }
            onToggle={ isOn => handleForm(isOn, x.label) }
          />
        </View>
      );
    });
  };

  return (
    <View style={ Styles.container }>
      <Container style={ Styles.container }
        header={ { leftButton: 'ios-arrow-back',
        onPressLeft: () => Actions.pop(),
        leftButtonColor: Colors.grey.med } }>
      
          <Text style={ Styles.title }>Atur Notifikasi</Text>
          <Text style={ Styles.desc }>Atur notifikasi sesuai yang Anda inginkan</Text>
          
          { renderContent() }

      </Container>
    </View>
  );
};

export default NotifSetting;
