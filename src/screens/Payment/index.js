import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
// import { useSelector } from 'react-redux';
import {
  Container,
  Radio,
  Button,
  Icon,
} from 'components';
import {
  // RatioHelper,
  MiscHelper,
} from 'helpers';
import {
  // actions,
} from 'store';
import {
  Images,
  Colors,
} from 'consts';

import Styles from './style';

const Payment = () => {
  const [selected, setSelected] = useState(0);
  const [freeVerification] = useState(2);
  const [useFreeVerification, setUseFreeVerification] = useState(false);
  const [price] = useState(20000);
  const [discount, setDiscount] = useState(null);

  useEffect(() => {
    let currentDiscount = null;
    if (useFreeVerification) {
      currentDiscount = {
        name: 'GRATIS Verifikasi Pekerja',
        price: price,
      };
    } else if (selected === 0) {
      currentDiscount = {
        name: 'GOPAY Cashback 10%',
        price: price / 10,
      };
    }
    setDiscount(currentDiscount);
  }, [useFreeVerification, price, selected]);

  const options = [
    {
      label: 'GOPAY',
      value: 0,
      image: Images.logo_gopay,
    },
    {
      label: 'OVO',
      value: 1,
      image: Images.logo_ovo,
    },
    {
      label: 'DANA',
      value: 2,
      image: Images.logo_dana,
    },
  ];

  const renderRadioButtons = () => (
    options.map(option => (
      <Radio
        key={ option.value }
        card
        selected={ option.value === selected && !useFreeVerification }
        label={ option.label }
        disabled= { useFreeVerification }
        onPress={ () => { setSelected(option.value); } }
        image={ option.image } />
    ))
  );

  const renderFreeVerificationInfo = () => {
    if (freeVerification && !useFreeVerification) {
      return (
        <View style={ Styles.freeVerificationContainer(useFreeVerification) }>

          <View>
            <Text style={ Styles.freeVerificationTitle }>Anda punya { freeVerification } Verifikasi Pekerja!</Text>
            <Text style={ Styles.freeVerificationText }>Gunakan untuk verifikasi pekerja Anda</Text>
          </View>

          <Button
            style={ Styles.freeVerificationButton }
            text='Gunakan'
            onPress={ () => { setUseFreeVerification(true); } }
            />

        </View>
      );
    } else if (freeVerification && useFreeVerification) {
      return (
        <View style={ Styles.freeVerificationContainer(useFreeVerification) }>

          <Icon
            name={ 'checkmark-circle' }
            color={ Colors.green.success }
            size={ 25 } />

          <Text style={ Styles.useFreeVerificationTitle }>GRATIS Verifikasi Pekerja</Text>

          <TouchableOpacity onPress={ () => { setUseFreeVerification(false); } }>
            <Icon
              name={ 'close' }
              color={ Colors.grey.light }
              size={ 40 } />
          </TouchableOpacity>

        </View>
      );
    }
  };

  const renderPriceInfo = () => (
    <View style={ Styles.priceInfoContainer }>

      <View style={ Styles.priceInfoRow } >
        <Text style={ Styles.priceInfoFadeText }>Harga</Text>
        <Text style={ Styles.priceInfoText }>{ MiscHelper.currencyFormat(price, ',') }</Text>
      </View>
      {
        discount &&
        (
          <View style={ Styles.priceInfoRow } >
            <Text style={ Styles.priceInfoFadeText }>{ discount.name }</Text>
            <Text style={ [Styles.priceInfoText, { color: Colors.red.danger }] }>-{ MiscHelper.currencyFormat(discount.price, ',') }</Text>
          </View>
        )
      }
      <View style={ Styles.priceInfoRow } >
        <Text style={ Styles.priceInfoBoldText }>Total</Text>
        <Text style={ Styles.priceInfoBoldText }>{ MiscHelper.currencyFormat(discount ? price - discount.price : price, ',') }</Text>
      </View>

    </View>
  );

  return (
    <ScrollView style={ Styles.container } bounces={ false }>

      <Container
        style={ Styles.container }
        header={ {
          onPressLeft: () => Actions.pop(),
          leftButton: 'ios-arrow-back',
        } }
      >

      <Text style={ Styles.title }>Verifikasi Pekerja</Text>

      <View style={ Styles.profileContainer }>

        <Image style={ Styles.profileImage } source={ { uri: 'https://randomuser.me/api/portraits/women/25.jpg' } } />
        
        <View>
          <Text style={ Styles.profileFadeText } >Baby Sitter</Text>
          <Text style={ Styles.profileBoldText }>Rita Pratiwi Sutrisno</Text>
          <Text style={ Styles.profileFadeText } >24 Tahun</Text>
        </View>

      </View>

      <View style={ Styles.paymentMethodContainer } >

        <Text style={ Styles.paymentMethodTitle }>Pilih Metode Pembayaran</Text>

        { renderRadioButtons() }

      </View>

      { renderFreeVerificationInfo() }

      { renderPriceInfo() }

      <Button
        text='Bayar Sekarang'
        onPress={ () => {
          Actions.successNotif({
            title: 'Pembayaran Berhasil',
            desc: 'Pembayaran telah berhasil. Anda dapat melihat hasil verifikasi pekerja Anda sekarang.',
            btnText: 'Lihat Hasil Verifikasi',
            onPress: () => {
              Actions.popTo('result', {
                verified: true,
              });
            },
          });
        } }
        />

      </Container>
      
    </ScrollView>
  );
};

export default Payment;
