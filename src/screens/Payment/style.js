import {
  RatioHelper,
} from 'helpers';

import {
  Colors,
  GlobalStyles,
} from 'consts';

export default {
  container: {
    flex: 1,
  },

  title: [
    GlobalStyles.textBold(undefined, Colors.blue.main),
    {
      marginBottom: 20,
    },
  ],

  profileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 20,
    borderBottomColor: Colors.grey.light,
    borderBottomWidth: 0.3,
    marginBottom: 20,
  },

  profileImage: {
    height: 60,
    width: 60,
    borderRadius: 60,
    marginRight: 10,
  },

  profileFadeText: [
    GlobalStyles.textRegular(RatioHelper.normalize(12), Colors.grey.default),
    {
      marginBottom: 4,
    },
  ],

  profileBoldText: [
    GlobalStyles.textBold(RatioHelper.normalize(14)),
    {
      marginBottom: 4,
    },
  ],

  paymentMethodContainer: {
    borderBottomColor: Colors.grey.light,
    borderBottomWidth: 0.3,
    paddingBottom: 10,
    marginBottom: 20,
  },

  paymentMethodTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(20)),
    {
      marginBottom: 20,
    },
  ],

  // freeVerificationContainer: {
  //   flexDirection: 'row',
  //   backgroundColor: Colors.blue.light,
  //   paddingHorizontal: 20,
  //   borderRadius: 10,
  //   alignItems: 'center',
  //   justifyContent: 'space-between',
  //   height: 60,
  //   marginBottom: 10,
  // },

  freeVerificationContainer: (used = false) => {
    return {
      flexDirection: 'row',
      backgroundColor: used ? Colors.white.default : Colors.blue.light,
      paddingHorizontal: used ? 18 : 20,
      borderRadius: 10,
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 60,
      marginBottom: 20,
      borderWidth: used ? 2 : null,
      borderColor: used ? Colors.green.success : null,
    };
  },

  freeVerificationTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(14), Colors.blue.main),
    {
      marginBottom: 5,
    },
  ],

  freeVerificationText: [
    GlobalStyles.textRegular(RatioHelper.normalize(12), Colors.blue.main),
  ],

  freeVerificationButton: {
    marginLeft: 10,
    height: 40,
  },

  useFreeVerificationContainer: {
    flexDirection: 'row',
    paddingHorizontal: 18,
    borderRadius: 10,
    alignItems: 'center',
    height: 60,
    borderWidth: 2,
    borderColor: Colors.green.success,
    marginBottom: 10,
  },

  useFreeVerificationTitle: [
    GlobalStyles.textBold(RatioHelper.normalize(14), Colors.blue.main),
    {
      flexGrow: 1,
      marginLeft: 10,
    },
  ],

  priceInfoContainer: {
    marginBottom: 30,
  },

  priceInfoRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },

  priceInfoFadeText: GlobalStyles.textRegular(RatioHelper.normalize(18), Colors.grey.default),

  priceInfoText: GlobalStyles.textRegular(RatioHelper.normalize(18)),

  priceInfoBoldText: GlobalStyles.textBold(RatioHelper.normalize(18)),

};
