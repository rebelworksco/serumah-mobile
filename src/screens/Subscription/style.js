import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
    alignItems: 'center',
  },

  badge: {
    flexDirection: 'row',
    borderColor: Colors.grey.border,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 16,
    width: RatioHelper.normalize(80),
    height: RatioHelper.normalize(35),
  },

  titleContainer: {
    flex: 1,
    width: RatioHelper.screenWidth - 50,
  },

  title: {
    ...GlobalStyles.textSemiBold(24, Colors.blue.main),
    marginBottom: RatioHelper.normalize(10),
  },

  desc: {
    fontSize: RatioHelper.normalize(14),
    color: Colors.grey.med,
    
  },

  cardContainer: {
    flex: 5,
    justifyContent: 'flex-start',
    width: RatioHelper.screenWidth - 50,
  },

  cardText1: {
    textAlign: 'center',
    color: Colors.blue.main,
  },

  cardTextDesc: {
    textAlign: 'center',
    ...GlobalStyles.textSemiBold(14, Colors.blue.main, 'center'),
  },

  firstCard: {
    backgroundColor: Colors.blue.mainTransparent,
    flexDirection: 'row',
    marginBottom: RatioHelper.normalize(15),
    marginTop: RatioHelper.normalize(15),
  },

  firstCardInner: {
    flex: 1,
    flexDirection: 'column',
  },

  card: {
    marginBottom: RatioHelper.normalize(15),
    marginTop: RatioHelper.normalize(15),
  },

  cardInner: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  cardTitle1: {
    ...GlobalStyles.textSemiBold(16, Colors.blue.navy),
    marginBottom: RatioHelper.normalize(6),
    marginTop: RatioHelper.normalize(6),
  },

  cardPrice: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.main),
  },

  cardSlot: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.main),
  },
  
  bottomSection: {
    flex: 4,
    width: RatioHelper.screenWidth - 50,
    borderTopWidth: 1,
    borderTopColor: Colors.grey.borderLight,
    marginTop: RatioHelper.normalize(20),

  },

  bottomSectionInner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: RatioHelper.normalize(20),
  },

  bottomLabel1: {
    color: Colors.grey.med,
    fontSize: RatioHelper.normalize(14),
  },

  bottomPrice1: {
    color: Colors.blue.darkText,
  },

  bottomLabel2: {
    color: Colors.red.mutedPastel,
    fontSize: RatioHelper.normalize(16),
  },

  bottomLabel3: {
    ...GlobalStyles.textSemiBold(16, Colors.blue.navy),
  },

};
