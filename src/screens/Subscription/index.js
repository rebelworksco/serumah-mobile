import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import { RatioHelper } from 'helpers';
import { GlobalStyles, Colors } from 'consts';
import { Button, Container, Card } from 'components';
import Styles from './style';

const Subscription = props => {

  const onPressButton = () => {
    console.log('subscribe');
  };

  const onPressClose = () => {
    console.log('close !');
    Actions.pop();
  };

  return (
    <View style={ { flex: 1 } }>
      <Container style={ Styles.container } header={ { rightButton2: 'ios-close', rightButton2Size: RatioHelper.normalize(50), onPressRight2: onPressClose, rightButton2ContainerStyle: { marginRight: 20 } } }>
        <View style={ Styles.titleContainer }>
          <Text style={ Styles.title }>
            Subscription
          </Text>
          <Text style={ Styles.desc }>
            Beli subscription untuk menambah slot
          </Text>
        </View>

        <View style={ Styles.cardContainer } >
          
          <Card style={ Styles.firstCard }>
            <View style={ Styles.firstCardInner }>
              <Text style={ Styles.cardText1 }>6 bulan Subscription</Text>
              <Text style={ Styles.cardTextDesc }>Diskon 10%</Text>
            </View>
            <View style={ Styles.firstCardInner }>
              <Text style={ Styles.cardText1 }>12 bulan Subscription</Text>
              <Text style={ Styles.cardTextDesc }>Diskon 15%</Text>
            </View>
          </Card>

          <Card style={ Styles.card }>
            <View style={ Styles.cardInner }>
              <Text style={ Styles.cardTitle1 }>Slot Pekerja</Text>
              <Text>
                <Text style={ Styles.cardPrice }>IDR 10,000</Text>
                <Text>/slot</Text>
              </Text>
            </View>

            <View style={ Styles.badge }>
              <Text style={ GlobalStyles.textBold(16, Colors.blue.main) }>
                -
              </Text>
              <Text style={ GlobalStyles.textBold(14) }>1</Text>
              <Text style={ GlobalStyles.textBold(16, Colors.blue.main) }>
                +
              </Text>
            </View>
          </Card>

          <Card style={ Styles.card }>
            <View style={ Styles.cardInner }>
              <Text style={ Styles.cardTitle1 }>Jangka Waktu</Text>
              <Text>
                <Text style={ Styles.cardTitle1 }>Subscription</Text>
                <Text>(bulan)</Text>
              </Text>
            </View>

            <View style={ Styles.badge }>
              <Text style={ GlobalStyles.textBold(16, Colors.blue.main) }>
                -
              </Text>
              <Text style={ GlobalStyles.textBold(14) }>6</Text>
              <Text style={ GlobalStyles.textBold(16, Colors.blue.main) }>
                +
              </Text>
            </View>
          </Card>
        </View>

        <View style={ Styles.bottomSection } >
          <View style={ Styles.bottomSectionInner }>
            <Text style={ Styles.bottomLabel1 }>Harga</Text>
            <Text style={ Styles.bottomPrice1 }>IDR 120,000</Text>
          </View>
          <View style={ Styles.bottomSectionInner }>
            <Text style={ Styles.bottomLabel2 }>Diskon 6 Bulan (10%)</Text>
            <Text style={ Styles.bottomLabel2 }>-IDR 12,000</Text>
          </View>
          <View style={ Styles.bottomSectionInner }>
            <Text style={ Styles.bottomLabel3 }>Total</Text>
            <Text style={ Styles.bottomLabel3 }>IDR 108,000</Text>
          </View>
        </View>
        <Button text={ 'Subscribe' } onPress={ onPressButton } width={ RatioHelper.screenWidth - 50 } />
      </Container>
    </View>
  );
};

export default Subscription;
