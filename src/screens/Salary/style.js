import {
  GlobalStyles,
  Colors,
} from 'consts';
import { RatioHelper } from 'helpers';

export default {
  container: {
    flex: 1,
    zIndex: 20,
    justifyContent: 'flex-start',
  },

  profileContainer: {
    flexDirection: 'row',
    marginBottom: RatioHelper.normalize(8),
  },
  
  innerProfile: {
    flexDirection: 'column',
  },

  imgCon: {
    margin: RatioHelper.normalize(8),
  },

  imgBadge: {
    width: RatioHelper.normalize(25),
    height: RatioHelper.normalize(25),
    position: 'absolute',
    right: -5,
    bottom: -7,
  },

  profileImage: {
    width: RatioHelper.normalize(55),
    height: RatioHelper.normalize(55),
    borderRadius: 100,
  },

  job: {
    color: Colors.white.default,
    fontSize: RatioHelper.normalize(14),
  },

  info: {
    ...GlobalStyles.textSemiBold(18, Colors.white.default),
  },

  infoCon: {
    flex: 1,
    margin: RatioHelper.normalize(8),
    paddingTop: RatioHelper.normalize(6),
  },

  upperCard: {
    flexDirection: 'column',
    padding: 0,
    flex: 3,
  },
  
  innerUpperCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
  },

  cardText1: {
    color: Colors.grey.med,
    marginBottom: RatioHelper.normalize(5),
  },

  cardText2: {
    ...GlobalStyles.textSemiBold(18, Colors.blue.main, 'right'),
    marginBottom: RatioHelper.normalize(5),
  },

  bottomTitle: {
    ...GlobalStyles.textSemiBold(18),
    marginTop: RatioHelper.normalize(30),
    marginBottom: RatioHelper.normalize(15),
  },

  cardContainer: {
    flex: 1,
    flexDirection: 'row',
  },

  innerCardCon: {
    flex: 1,
    flexDirection: 'column',
  },

  bottomCard: {
    // width: (RatioHelper.screenWidth / 2) - 50,
    margin: RatioHelper.normalize(10),
    // flex: 1,
    flexDirection: 'column',
    height: RatioHelper.normalize(92),
  },

  bottomCardNominal: color => {
    return {
      ...GlobalStyles.textSemiBold(18, color),
      flex: 1,
      marginTop: RatioHelper.normalize(8),

    };
  },

  bottomCardIc: {
    width: RatioHelper.normalize(30),
    height: RatioHelper.normalize(30),
  },

  bottomCardInner: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: RatioHelper.normalize(10),
  },

  bottomCardText: {
    color: Colors.grey.med,
    marginLeft: RatioHelper.normalize(10),
    marginTop: RatioHelper.normalize(8),
    fontSize: RatioHelper.normalize(14),
  },

};
