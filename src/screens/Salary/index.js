import React from 'react';
import {
  View,
  Text,
  Image,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import { Images, Colors } from 'consts';
import { RatioHelper } from 'helpers';
import { Button, Container, BottomCard, Card, LineChart } from 'components';
import Styles from './style';

const Salary = props => {
  const { } = props;

  const onPressConfirm = () => {
    console.log('CONFIM CONFIRM CONFIRM!!!');
  };
  
  const renderCard = (icon, text, nominal, color, prefix) => {
    return (
      <Card style={ Styles.bottomCard }>
        <View style={ Styles.bottomCardInner }>
          <Image style={ Styles.bottomCardIc } source={ icon }/>
          <Text style={ Styles.bottomCardText }>
            {text}
          </Text>
        </View>
        <Text style={ Styles.bottomCardNominal(color) }>
          { prefix && prefix}IDR { nominal }
        </Text>
      </Card>
    );
  };

  return (
    <View style={ { flex: 1 } }>
      <Container diamond noPaddingTop style={ Styles.container } bg header={ {  onPressLeft: () => Actions.pop(), leftButton: 'ios-arrow-back', leftButtonColor: Colors.white.default } }>
        <View style={ Styles.diamond } />
        <View style={ Styles.profileContainer }>
          <View style={ Styles.imgCon }>
            <Image style={ Styles.profileImage } source={ Images.bg_houses } />
            <Image style={ Styles.imgBadge } source={ Images.ic_female } />
          </View>
          <View style={ Styles.infoCon }>
            <Text style={ Styles.job }>
              Baby Sitter
            </Text>
            <Text style={ Styles.info }>
              Rita Pratiwi Sutrisno, 24
            </Text>
          </View>
        </View>
        
        <Card style={ Styles.upperCard }>
          <View style={ Styles.innerUpperCard }>
            <View>
              <Text style={ Styles.cardText1 }>
                Total Gaji Bulan ini
              </Text>
              <Text style={ Styles.cardText2 }>
                IDR 6,300,000
              </Text>
            </View>
            <Button text={ 'Konfirmasi' } onPress={ onPressConfirm } width={ RatioHelper.screenWidth / 4 }  />
          </View>
          <LineChart />
        </Card>
        <View style={ { flex: 5 } }>
          <Text style={ Styles.bottomTitle }>
            Rincian Gaji Bulan Desember
          </Text>

          <View style={ Styles.cardContainer }>
            <View style={ Styles.innerCardCon }>
              {renderCard(Images.ic_salary_blue, 'Gaji Pokok', 2500000, Colors.blue.main)}
              {renderCard(Images.ic_overtime, 'Lembur', 2800000, Colors.blue.tosca, '+')}
            </View>
            <View style={ Styles.innerCardCon }>
              {renderCard(Images.ic_salary_purple, 'Tunjangan', 1500000, Colors.purple.default)}
              {renderCard(Images.ic_loan, 'Cicilan Pinjaman', 500000, Colors.yellow.mustard, '-')}
            </View>
          </View>

        </View>
      </Container>
      <BottomCard
        title={ ['Total Gaji Bulan Ini', 'IDR 2,300,000'] }
        type={ 'money' }
        btnText={ 'Konfirmasi Gajian' }
        onPressBtn={ () => Actions.pin({
          title: 'Masukkan PIN Anda',
          onPressNext: () => {
            Actions.successNotif({
              title: 'Konfirmasi Gajian Berhasil',
              desc: 'Konfirmasi gajian Anda berhasil. Notifikasi akan diberikan apabila pekerja Anda telah mengkonfirmasi juga.',
              btnText: 'Kembali ke Halaman Pekerja',
              onPress: () => {
                Actions.worker();
              },
            });
          },
        }) }
      />
    </View>
  );
};

export default Salary;
