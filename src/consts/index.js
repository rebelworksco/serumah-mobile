import Endpoints from './Endpoints';
import Dispatches from './Dispatches';
import Colors from './Colors';
import Images from './Images';
import Fonts from './Fonts';
import GlobalStyles from './GlobalStyles';

export {
  Endpoints,
  Dispatches,
  Colors,
  Images,
  Fonts,
  GlobalStyles,
};
