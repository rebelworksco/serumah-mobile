export default {
	osBold: 'OpenSans-Bold',
	osSemiBold: 'OpenSans-SemiBold',
	osRegular: 'OpenSans-Regular',
	osLight: 'OpenSans-Light',
	osItalic: 'OpenSans-Italic',
};
