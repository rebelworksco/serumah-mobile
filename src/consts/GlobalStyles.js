import {
  RatioHelper,
} from 'helpers';

import Colors from './Colors';

export default {
  textBold: (fontSize = RatioHelper.normalize(24), color = Colors.black.default, textAlign = 'left') => {
    return {
      fontSize: fontSize,
      // fontFamily: Fonts.osBold,
      fontWeight: 'bold',
      color: color,
      textAlign: textAlign,
      letterSpacing: 0.3,
    };
  },

  textSemiBold: (fontSize = RatioHelper.normalize(20), color = Colors.black.default, textAlign = 'left') => {
    return {
      fontSize: fontSize,
      // fontFamily: Fonts.osSemiBold,
      color: color,
      textAlign: textAlign,
      letterSpacing: 0.3,
      fontWeight: '600',
    };
  },

  textRegular: (fontSize = RatioHelper.normalize(14), color = Colors.black.opacity, textAlign = 'left') => {
    return {
      fontSize: fontSize,
			// fontFamily: Fonts.osRegular,
			color: color,
			textAlign: textAlign,
			letterSpacing: 0.3,
    };
  },

  shadow: {
		shadowOpacity: 0.2,
		shadowRadius: 10,
		shadowOffset: {
			height: 3,
			width: 0,
		},
		elevation: 1,
		borderWidth: RatioHelper.platformScale(0, 0.2),
		borderColor: Colors.grey.borderOpacity,
		backgroundColor: Colors.white.default,
	},

  diamond: (top = 0 - RatioHelper.screenHeight / 8, left = 0 - RatioHelper.screenHeight / 5) => {
    return {
      top: top,
      left: left,
      position: 'absolute',
      opacity: 0.04,
      backgroundColor: Colors.white.default,
      height: RatioHelper.screenHeight / 2,
      width: RatioHelper.screenHeight / 2,
      transform: [{ rotate: '45deg' }],
      borderRadius: 60,
    };
  },

  fullScreen: {
    position: 'absolute',
    height: RatioHelper.screenHeight,
    width: RatioHelper.screenWidth,
  },
};
