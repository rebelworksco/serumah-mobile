import env from '../../env';
const baseUrl = env.baseUrl;

export default {
	REGISTER: { baseUrl: baseUrl, path: 'register', method: 'POST' },
	LOGIN: { baseUrl: baseUrl, path: 'get-token', method: 'POST' },
	GET_PROFILE: { baseUrl: baseUrl, path: 'profiles', method: 'GET' },
};
