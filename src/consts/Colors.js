export default {
  white: {
    default: 'rgb(255, 255, 255)',
    opacity: 'rgba(255, 255, 255, 0.8)',
    opacityHalf: 'rgba(255, 255, 255, 0.5)',
    opacityLow: 'rgba(255, 255, 255, 0.1)',
  },
  black: {
    default: 'rgb(0, 0, 0)',
    opacity: 'rgba(0, 0, 0, 0.8)',
    opacityHalf: 'rgba(0, 0, 0, 0.5)',
    opacityLow: 'rgba(0, 0, 0, 0.1)',
  },
  grey: {
    default: 'rgb(128, 128, 128)',
    light: 'rgb(208,208,208)',
    lightest: 'rgb(250,250,250)',
    shadow: 'rgba(0, 0, 0, 0.08)',
    text: 'rgb(208, 208, 208)',
    border: 'rgb(234, 234, 234)',
    borderLight: 'rgb(244, 244, 244)',
    med: 'rgb(128, 138, 158)',
  },
  red: {
    default: 'rgb(255, 0, 0)',
    danger: 'rgb(210,118,118)',
    light: 'rgb(250,241,240)',
    mutedPastel: 'rgb(210, 118, 118)',
  },
  blue: {
    default: 'rgb(0, 0, 255)',
    main: 'rgb(62, 114, 221)',
    darkText: 'rgb(0, 44, 104)',
    light: 'rgb(235,240,251)',
    mainTransparent: 'rgba(62, 114, 221, 0.1)',
    tosca: 'rgb(69, 202, 172)',
    veryTransparent: 'rgba(62, 114, 221, 0.05)',
    veryTransparent2: 'rgba(62, 114, 221, 0.01)',
  },
  yellow: {
    default: 'rgb(255, 255, 0)',
    main: 'rgb(252,254,39)',
    mustard: 'rgb(245, 166, 35)',
    navy: 'rgb(6, 31, 82)',
    bright: 'rgb(252, 255, 36)',
  },
  green: {
    default: 'rgb(0, 128, 0)',
    success: 'rgb(69,202,172)',
  },
  orange: {
    default: 'rgb(255, 165, 0)',
  },
  pink: {
    default: 'rgb(255, 192, 203)',
  },
  purple: {
    default: 'rgb(155, 119, 186)',
  },
};
