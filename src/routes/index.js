import React from 'react';
import {
  Router,
  Scene,
  Stack,
  Tabs,
} from 'react-native-router-flux';

import {
  Splash,
  Login,
  Pin,
  TnC,
  ChooseRole,
  ReferralCode,
  Subscription,
  Salary,
  AddEmployee,
  FreeVerification,
  Register,
  Verification,
  WorkAgreement,
  NotifSetting,
  Profile,
  Notification,
  Worker,
  Result,
  Payment,
  History,
  Review,
  Attendance,
  Loan,
  SuccessNotif,
  GiveLoan,
  Location,
} from '../screens';

import { TabBar } from 'components';

const AppRouter = () => {
  return (
    <Router>
      <Stack hideNavBar key='root'>
        <Scene key='splash' component={ Splash } panHandlers={ null } />
        <Scene key='login' component={ Login } panHandlers={ null } />
        <Scene key='pin' component={ Pin } panHandlers={ null } />
        <Scene key='tnc' component={ TnC } />
        <Scene key='chooseRole' component={ ChooseRole } />
        <Scene key='referralCode' component={ ReferralCode } />
        <Scene key='freeVerification' component={ FreeVerification } />
        <Scene key='register' component={ Register } />
        <Scene key='verification' component={ Verification } />
        <Stack
          headerMode='none'
          tabs={ true }
        >
          <Scene key='attendance' component={ Attendance } tabBarComponent={ TabBar } />
          <Scene key='worker' component={ Worker } tabBarComponent={ TabBar } />
          <Scene key='loan' component={ Loan } tabBarComponent={ TabBar } />
          <Scene key='profile' initial component={ Profile } tabBarComponent={ TabBar }/>
        </Stack>
        <Scene key='notification' hideNavBar component={ Notification } panHandlers={ null } />
        <Scene key='location' hideNavBar component={ Location } panHandlers={ null } />
        <Scene key='addEmployee' hideNavBar component={ AddEmployee } hideTabBar />
        <Scene key='result' hideNavBar component={ Result } />
        <Scene key='payment' hideNavBar component={ Payment }  />
        <Scene key='giveLoan' component={ GiveLoan } hideNavBar panHandlers={ null } />
        <Scene key='salary' component={ Salary } hideNavBar panHandlers={ null } />
        <Scene key='successNotif' component={ SuccessNotif } hideNavBar panHandlers={ null } />
        <Scene key='subscription' component={ Subscription } hideNavBar panHandlers={ null } />
        <Scene key='workAgreement' component={ WorkAgreement } hideNavBar panHandlers={ null } />
        <Scene key='notifSetting' component={ NotifSetting } hideNavBar panHandlers={ null }/>
        <Scene key='history' hideNavBar component={ History }  />
        <Scene key='review' hideNavBar component={ Review } />
      </Stack>
    </Router>
  );
};
export default AppRouter
;
